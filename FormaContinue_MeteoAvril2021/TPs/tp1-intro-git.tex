\documentclass[a4paper]{scrartcl}

\usepackage{header}

\newcommand{\annee}{2021}

\date{\annee}
\author{Sylvain Bouveret, Grégory Mounié et (majoritairement) Matthieu Moy}
\title{Utilisation de Git -- Introduction à l'outil}

\sloppy

\begin{document}
\maketitle

Ce document peut être téléchargé depuis l'adresse suivante~:
\begin{center}
  \url{\gitlabpages/\jobname.pdf}
\end{center}

\section{Introduction}

\subsection{Git et la gestion de versions}

Git est un gestionnaire de versions, c'est-à-dire un logiciel qui
permet de conserver l'historique des fichiers sources d'un projet, et
d'utiliser cet historique pour fusionner automatiquement plusieurs
révisions (ou « versions »). Chaque membre de l'équipe travaille sur
sa version du projet, et peut envoyer les versions suffisamment
stables à ses coéquipiers via un dépôt partagé (commande \texttt{git
  push}) qui pourront les récupérer et les intégrer aux leurs quand
ils le souhaitent (commande \texttt{git pull}).

Il existe beaucoup d'autres gestionnaires de versions. Les pages
\url{http://ensiwiki.ensimag.fr/index.php/Gestionnaire_de_Versions} ou
\url{https://en.wikipedia.org/wiki/Comparison_of_version-control_software}
vous donnent un aperçu de l'existant.

\subsection{Organisation pendant la séance machine}

Pour la séance machine, choisissez deux PC adjacents par équipe (on
peut utiliser son ordinateur portable à la place d'un PC de l'école,
pour peu que l'on ait accès à la machine contenant le dépôt
partagé). Chaque personne travaille sur son compte.

Le dépôt partagé  doit être accessible en permanence  donc hébergé sur
un  serveur.  Ce  dépôt est  simplement un  répertoire qui  contiendra
l'ensemble de l'historique  du projet.  On ne  travaillera jamais dans
le dépôt directement, mais on  utilisera Git pour envoyer et récupérer
des révisions.  Tous  les membres de l'équipe auront accès  à ce dépôt
en lecture et en écriture. Dans  la suite des explications, on suppose
que le dépôt est hébergé sur \texttt{gitlab.ensimag.fr}.

L'équipe  est  constituée  d'\textit{Alice}   qui  travaille  sur  son
portable \texttt{laptop1} et \textit{Bob}  qui travaille également sur
son  portable  \texttt{laptop2}.   Si \textit{Alice}  ou  \textit{Bob}
travaille sur un PC de  l'école, il faut remplacer \texttt{laptop1} ou
\texttt{laptop2} par  le nom de la  machine (e.g.  \texttt{ensipc42}).
Les  explications sont  écrites pour  2 utilisateurs  pour simplifier,
mais il peut y avoir un nombre quelconque de coéquipiers.

\tikzstyle{depot}=[draw=black,shape=ellipse]
\begin{center}
\begin{tikzpicture}
  \draw node[depot] (partagee) {
    \begin{tabular}{c}
Dépôt Partagé\\
\texttt{gitlab.ensimag.fr}
\end{tabular}
};
  \draw (partagee) ++(-3, -2.5) node[depot] (alice) {
    \begin{tabular}{c}
Répertoire de\\
travail d'\textit{Alice}\\
\texttt{alice@laptop1}
\end{tabular}
};
  \draw (partagee) ++(+3, -2.5) node[depot] (bob) {
    \begin{tabular}{c}
Répertoire de\\
travail de \textit{Bob}\\
\texttt{bob@laptop2}
\end{tabular}
};

\draw[<->,>=latex,thick] (partagee) -- (alice);
\draw[<->,>=latex,thick] (partagee) -- (bob);
\end{tikzpicture}
\end{center}

\section{Configuration locale de Git}

Si vous travaillez sur votre machine personnelle, vérifiez que Git est
installé (la commande \texttt{git}, sans argument, doit vous donner un
message d'aide). Si ce n'est pas le cas, installez-le (sous Ubuntu,
« \texttt{apt-get install git gitk} » ou « \texttt{apt-get install git-core gitk} »
devrait faire l'affaire, ou bien rendez-vous sur \url{http://git-scm.com/}).

On commence par configurer l'outil Git. Sur la machine sur laquelle on
souhaite travailler (donc sur vos portables dans notre exemple)~:
\begin{minted}{console}
both@laptops$ git config --edit --global
\end{minted}
Ou bien~:
\begin{minted}{console}
both@laptops$ emacs ~/.gitconfig          # ou son éditeur préféré à la place d'Emacs !
\end{minted}
Le contenu du fichier \texttt{.gitconfig} (à créer s'il n'existe pas)
doit ressembler à ceci~:

\begin{minted}{ini}
[core]
# Editeur de texte au choix, n'en garder qu'un :
        editor = nano
        editor = atom --wait
        editor = code --wait
        editor = gedit -s
        editor = emacs
        editor = emacsclient -c
        editor = vim
        editor = gvim -f
[user]
        name = Prénom Nom
        email = Prenom.Nom@ensimag.grenoble-inp.fr
[push]
        default = simple   # défaut depuis Git 2.x
# Section ci-dessous pas nécessaires avec un Git récent
[color]
        ui = auto
\end{minted}

La ligne \mintinline{text}{editor} de la section \mintinline{text}{[core]} définit votre
éditeur de texte préféré (par exemple, \mintinline{text}{emacs}, \mintinline{text}{vim},
\mintinline{text}{atom},\dots{}).  Attention, comme vous pouvez le constater
ci-dessus, certains éditeurs comme Atom, Visual Studio Code ou gedit
demandent des options particulières pour être compatible avec git.
Sans ces options, vous seriez confronté à l'erreur suivante lors d'un
\mintinline{text}{commit} git si l'éditeur était déjà lancé~:

\begin{minted}{console}
Aborting commit due to empty commit message.
\end{minted}

La ligne \mintinline{text}{editor} n'est pas obligatoire ; si elle n'est pas présente, la
variable d'environnement \mintinline{text}{VISUAL} sera utilisée ; si cette
dernière n'existe pas, ce sera la variable d'environnement \mintinline{text}{EDITOR}.

La section \mintinline{text}{[user]} est obligatoire, elle donne les informations
qui seront enregistrées par Git lors d'un \mintinline{text}{commit}. Il est
conseillé d'utiliser ici votre vrai nom (pas juste votre login) et votre
adresse e-mail officielle, et d'utiliser la même configuration
sur toutes les machines sur lesquelles vous travaillez.

La ligne \mintinline{text}{editor} de la section \mintinline{text}{[core]} définit votre éditeur
de texte préféré (par exemple, \mintinline{text}{emacs}, \mintinline{text}{vim},%
\mintinline{text}{gvim-f},\dots{} mais évitez \mintinline{text}{gedit} qui vous posera problème
ici)\footnote{Si un \mintinline{text}{gedit} est déjà lancé, la commande \mintinline{text}{git
    commit} va se connecter au \mintinline{text}{gedit} déjà lancé pour lui
  demander d'ouvrir le fichier, et le processus lancé par \mintinline{text}{git} va
  terminer immédiatement. Git va croire que le message de commit est
  vide, et abandonner le commit. Il semblerait que \mintinline{text}{gedit -s -w}
  règle le problème, mais cette commande est
  disponible seulement avec les versions $\geq$ 3.1.2 de \mintinline{text}{gedit},
  donc pas sur CentOS 6, mais peut-être sur vos portables.}. Cette
dernière ligne n'est pas obligatoire ; si elle n'est pas présente, la
variable d'environnement \mintinline{text}{VISUAL} sera utilisée ; si cette
dernière n'existe pas, ce sera la variable d'environnement \mintinline{text}{EDITOR}.

La section \mintinline{text}{[color]} est là pour rendre l'interface de Git plus jolie.

La section \mintinline{text}{[push]} permet d'avoir le même comportement avec
Git 2.x et Git 1.x (utiliser \mintinline{text}{current} au lieu de
\mintinline{text}{simple} avec les très vieilles versions de Git).

\section{Mise en place}

Le contenu de cette section est réalisé une bonne fois pour toutes, au
début du projet. Si certains membres de l'équipe ne comprennent pas
les détails, ce n'est pas très grave, nous verrons ce que tout le
monde doit savoir dans la section~\ref{sec=utilisation}.


\subsection{Création du dépôt partagé}
\label{sec=creation-depot-partagee}

On va maintenant créer le dépôt partagé. Seule \textit{Alice} fait
cette manipulation, sur son compte sur \texttt{gitlab.ensimag.fr}. Le
dépôt partagé doit être créé sur un serveur pour être accessible en
permanence ; \texttt{gitlab.ensimag.fr} est celui sur lesquels sont
hébergés les dépôts Git à l'Ensimag.

Il faut dans un premier temps vous connecter à votre compte~:

\begin{minted}{console}
alice@laptop1$ firefox gitlab.ensimag.fr
\end{minted}

Si votre mot de passe n'est pas reconnu, rendez-vous sur la page
\url{https://copass-client.grenet.fr/} et modifiez ou re-validez votre mot
de passe, puis réessayez.

\subsubsection{Création d'un groupe (de développeur)}

Nous allons commencer par créer un groupe pour les deux membres de
l'équipe. Cette étape n'est pas nécessaire, mais elle permet de gérer
efficacement les droits d'accès pour un ensemble de projets.

En utilisant le menu/Bouton « + » au sommet de la page, Alice crée un
groupe (\emph{New group}) nommé avec les logins séparés par un
\texttt{souligné/underscore}, par exemple \verb!alice_bob!. Ce
groupe est créé avec une visibilité \emph{Private} (le défaut).

En utilisant le bouton « Groups » au sommet de la page, Alice
sélectionne son groupe nouvellement créé et ajoute Bob aux membres du
groupe (\emph{Members} dans le menu gauche, puis « Bob » dans
\emph{Add new member}). Elle lui donnera le même rôle que le sien,
celui de \emph{Owner}.

\subsubsection{Création du dépôt (de code)}

En utilisant le menu/Bouton « + » au sommet de la page, Alice crée un
projet (\emph{New project}) nommé \texttt{projet1}. Ce projet est créé
avec le groupe \verb!alice_bob!. Si elle était sur la page du groupe,
elle peut directement indiquer que ce projet appartient au
groupe. Sinon, elle peut remplacer son login (le groupe par défaut
lors de la création d'un projet) par \verb!alice_bob!.

Bob faisant partie du groupe, il a directement l'accès au projet.

Nous avons terminé la création du dépôt sur
\texttt{gitlab.ensimag.fr}. Vous pouvez maintenant \textbf{revenir sur
  votre machine de travail habituelle} (PC de l'Ensimag ou votre
machine personnelle).

La figure \ref{fig:git} résume les échanges entre dépôt local et distant ainsi que les différents états 
\footnote{\url{https://git-scm.com/book/en/v2/Getting-Started-Git-Basics}\\
Git has three main states that your files can reside in: committed, modified, and staged:
\begin{itemize}
	\item Committed means that the data is safely stored in your local database.
	\item Modified means that you have changed the file but have not committed it to your database yet.
	\item Staged means that you have marked a modified file in its current version to go into your next commit snapshot.
\end{itemize}
}
dans lesquels vos fichiers vont se retrouver  
au fur et mesure des manipulations du TP.

\begin{figure}[H]
  \centering
  \input{figs/git-sequence.tikz}
  \caption{Répertoire de travail (\emph{working dir.}), index (\emph{stag. area}) et dépôts (\emph{repo.}) local/distant git}
  \label{fig:git}
\end{figure}

\subsection{Configuration de ssh}
\label{sec=configuration-ssh}

Git et Gitlab peuvent utiliser les protocoles \texttt{https}, ou \texttt{ssh} pour dialoguer.
Le protocole \texttt{ssh} permet d'éviter d'avoir à taper son mot de
passe à chaque transfert Git tout en ayant un excellent niveau de
sécurité.

Pour cela vous aurez besoin d'avoir votre propre paire de clefs ssh (vous pouvez
lire à bon escient \href{https://git-scm.com/book/fr/v2/Git-sur-le-serveur-G%C3%A9n%C3%A9ration-des-cl%C3%A9s-publiques-SSH}{le chapitre 4.3 du livre de référence git} à ce propos).

Si vous ne l'avez pas déjà fait, il vous faudra ainsi créer une paire de clefs
publique et privée, en tapant la commande suivante :
\begin{minted}{console}
$ ssh-keygen
\end{minted}
% $

Cette commande vous demande de taper une \textit{passphrase}, qui servira
à protéger votre paire de clefs. Une phrase, ou quelques mots
que vous n'oublierez jamais, sont une bonne \textit{passphrase}. Une
bonne pratique est de créer une paire différente par ordinateur ou
par compte informatique.

Maintenant vous devriez avoir deux fichiers \texttt{id\_rsa.pub}
(partie publique) and \texttt{id\_rsa} (partie privée) dans votre
répertoire \texttt{\textasciitilde/.ssh}. \emph{Ne transmettez
  \texttt{id\_rsa} à personne!}

Lorsque cette paire est crée, copiez la clef publique, c'est-à-dire le
contenu de \texttt{id\_rsa.pub}, dans votre compte
\url{https://gitlab.ensimag.fr}. Dans \emph{User Settings} (Menu en
haut à droite, troisième entrée) vous trouverez une section \emph{SSH
  Keys} (Menu à gauche). Copiez-collez la clef publique dans le
formulaire. Choisissez un nom pour cette clef et cliquez \texttt{Add
  key}.

\paragraph{Utilisation d'un agent SSH} La configuration de
l'authentification Gitlab est d'ores et déjà opérationnelle, et vous
pourrez interagir avec les dépôts sur lesquels vous avez les droits,
en tapant votre \textit{passphrase} à chaque fois. Néanmoins, si vous
interagissez souvent avec les dépôts, il peut être fastidieux de taper
le mot de passe à chaque fois. Pour vous simplifier la vie, vous pouvez
utiliser un agent SSH, en suivant les étapes suivantes.

D'abord, vérifiez que vous avez déjà un agent ssh (\texttt{ssh-agent}) en
train de tourner.

\begin{minted}{console}
both@laptops$ env | grep SSH_AGENT_PID
SSH_AGENT_PID=27487
\end{minted}
%$

Sinon, démarrez un nouvel agent SSH dans un terminal. Dans ce cas,
il faudra lancer toutes vos commandes git depuis ce terminal, ou
dans des processus issus de ce terminal.

\begin{minted}{console}
both@laptops$ eval $(ssh-agent)
Agent pid 27487
\end{minted}

Donnez votre \textit{passphrase} à l'agent~:
\begin{minted}{console}
both@laptops$ ssh-add
Enter passphrase for /home/both/.ssh/id_rsa:
\end{minted}
%$

Désormais, toutes les commandes comme \mintinline{sh}{git push}
ou \mintinline{sh}{git pull} se feront directement, sans vous redemander
votre \textit{passphrase} à chaque fois.


\subsection{Création des répertoires de travail}

On va maintenant créer le premier répertoire de travail. Pour
l'instant, il n'y a aucun fichier dans notre dépôt, donc la première
chose à faire sera d'y ajouter les fichiers sur lesquels on veut
travailler. Dans notre exemple, c'est \textit{Alice} qui va s'en occuper.

Pour créer un répertoire de travail dans le répertoire
\verb|~/projet1| (qui n'existe pas encore), \textit{Alice} entre donc les
commandes~:

{\small
\begin{minted}{console}
alice@laptop1$ git clone ssh://git@gitlab.ensimag.fr/alice_bob/projet1.git projet1
\end{minted}
}

Pour l'instant, ce répertoire est vide, ou presque~: il contient un
répertoire caché \texttt{.git/} qui contient les méta-données utiles à
Git (c'est là que sera stocké l'historique du projet).

Pour cette séance machine, un répertoire \texttt{sandbox/} a été prévu
pour vous, pour pouvoir vous entraîner sans casser un vrai projet.
\emph{Alice} télécharge le dépôt depuis
\url{https://ensiwiki.ensimag.fr/index.php/Fichier:Sandbox.tar.gz}
puis le décompresse et l'ajoute au dépôt local git~:

\begin{minted}{console}
alice@laptop1$ cd ~/projet1/
alice@laptop1$ tar xzvf ~/chemin/vers/le/repertoire/sandbox.tar.gz
alice@laptop1$ git add sandbox/
alice@laptop1$ git commit -m "ajout du repertoire sandbox/"
\end{minted}

La commande « \texttt{git add sandbox/} » dit à Git de « traquer »
tous les fichiers du répertoire \texttt{sandbox/}, c'est-à-dire qu'il
va enregistrer le contenu de ces fichiers, et suivre leur historique
ensuite. La commande \texttt{git commit} enregistre effectivement le
contenu de ces fichiers dans le dépôt local.

\textit{Alice} peut maintenant envoyer le squelette qui vient d'être importé
vers le dépôt partagé~:

\begin{minted}{console}
alice@laptop1$ git push
\end{minted}

% Avec les vieux Git seulement, normalement ce n'est plus d'actualité.
Si ça ne marche pas, essayez \mintinline{sh}{git push --all}.

Tout est prêt pour commencer à travailler. \textit{Bob} peut à son tour
récupérer sa copie de travail~:
{\small
\begin{minted}{console}
bob@laptop2$ cd
bob@laptop2$ git clone ssh://git@gitlab.ensimag.fr/alice_bob/projet1.git projet1
bob@laptop2$ cd projet1
bob@laptop2$ ls
\end{minted}
}


Si tout s'est bien passé, la commande \texttt{ls} ci-dessus devrait
faire apparaître le répertoire \texttt{sandbox/}.

\section{Utilisation de Git pour le développement}
\label{sec=utilisation}

Pour commencer, on va travailler dans le répertoire \texttt{sandbox}, qui
contient deux fichiers pour s'entraîner~:

\begin{minted}{console}
both@laptops$ cd sandbox
both@laptops$ emacs hello.c
\end{minted}

Il y a deux problèmes identifiés par des commentaires dans le fichier \texttt{hello.c}.
\textit{Alice} résout l'un des problèmes, et \textit{Bob} choisit l'autre. Par ailleurs, chacun ajoute son nom
en haut du fichier, et enregistre le résultat.

\subsection{Création de nouvelles révision}

Avec la commande \mintinline{bash}{git status}, un utilisateur peut comparer le repertoire de travail et le dépôt. On voit apparaître~:
\begin{minted}{console}
both@laptops$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

      modified:   hello.c

\end{minted}
Ce qui nous intéresse ici est la ligne « modified: hello.c »  qui signifie que
vous avez modifié \texttt{hello.c}, et que ces modifications n'ont
pas été enregistrées dans le dépôt local.
La distinction entre « Changes not staged for commit » et « Changes to be
committed » n'est pas importante pour l'instant.
On peut vérifier plus précisément
ce qu'on vient de faire~:
\begin{minted}{console}
both@laptops$ git diff HEAD
\end{minted}
Comme {\em Alice} et {\em Bob} ont fait des modifications différentes, le diff
affiché sera différent, mais ressemblera dans les deux cas à~:
\begin{minted}{diff}
diff --git a/sandbox/hello.c b/sandbox/hello.c
index a47665a..7f67d33 100644
--- a/sandbox/hello.c
+++ b/sandbox/hello.c
@@ -1,5 +1,5 @@
 /* Chacun ajoute son nom ici */
-/* Auteurs : ... et ... */
+/* Auteurs : Alice et ... */
 
 #include <stdio.h>
\end{minted}
Les lignes commençant par '-' correspondent à ce qui a été enlevé, et
les lignes commençant par '+' à ce qui a été ajouté par rapport au
précédent commit. Si vous avez suivi les consignes ci-dessus à propos
du fichier \texttt{.gitconfig}, vous devriez avoir les lignes supprimées
en rouge et les ajoutées en vert.

Maintenant, {\em Alice} et {\em Bob} font~:
\begin{minted}{console}
both@laptops$ git commit -a   # Enregistrement de l'état courant de l'arbre de travail dans le dépôt local.
\end{minted}
L'éditeur est lancé et demande d'entrer un message de 'log'. Ajouter
des lignes et d'autres renseignements sur les modifications apportées à
\texttt{hello.c} (on voit en bas la liste des fichiers modifiés). Un bon
message de log commence par une ligne décrivant rapidement le
changement, suivi d'une ligne vide, suivi d'un court texte expliquant
pourquoi la modification est bonne.

On voit ensuite apparaître~:
\begin{minted}{console}
[master 2483c22] Ajout de mon nom
 1 files changed, 2 insertions(+), 12 deletions(-)
\end{minted}
Ceci signifie qu'un nouveau « commit » (qu'on appelle aussi parfois
« revision » ou « version ») du projet a été enregistrée dans le dépôt.
Ce commit est identifié par une chaîne hexadécimale (« 2483c22 » dans
notre cas).

On peut visualiser ce qui s'est passé avec les commandes
\begin{minted}{console}
both@laptops$ gitk --all  # Visualiser tout l'historique graphiquement
\end{minted}
et
\begin{minted}{console}
both@laptops$ git gui blame hello.c    # voir l'historique de chaque ligne du fichier hello.c
\end{minted}

On va maintenant mettre ce « commit » à disposition des
autres utilisateurs.

\subsection{Fusion de révisions (merge)}

{\em Bob} SEULEMENT fait~:
\begin{minted}{console}
bob@laptop2$ git push     # Envoyer les commits locaux dans le dépôt partagé
\end{minted}
Pour voir où on en est, les deux équipes peuvent lancer la commande~:
\begin{minted}{console}
both@laptops$ gitk --all   # afficher l historique sous forme graphique
\end{minted}
ou bien~:
\begin{minted}{console}
both@laptops$ git log     # afficher l'historique sous forme textuelle.
\end{minted}
À PRESENT, {\em Alice} peut tenter d'envoyer ses modifications et on voit apparaître~:
\begin{minted}{console}
alice@laptop1$ git push
To ssh://git@gitlab.ensimag.fr/alice_bob/projet1.git
 ! [rejected]        master -> master (non-fast forward)
error: failed to push some refs to
'ssh://git@gitlab.ensimag.fr/alice_bob/projet1.git'
To prevent you from losing history, non-fast-forward updates were rejected
Merge the remote changes (e.g. 'git pull') before pushing again.  See the
'Note about fast-forwards' section of 'git push --help' for details.
\end{minted}

L'expression « non-fast-forward » (qu'on pourrait traduire par « absence
d'avance rapide ») veut dire qu'il y a des modifications dans le dépôt
vers lequel on veut envoyer nos modifications et que nous n'avons
pas encore récupérées. Il faut donc fusionner les modifications avant
de continuer.

L'utilisateur {\em Alice} fait donc~:
\begin{minted}{console}
alice@laptop1$ git pull
\end{minted}
Après quelques messages sur l'avancement de l'opération, on voit
apparaître~:
\begin{minted}{console}
Auto-merging sandbox/hello.c
CONFLICT (content): Merge conflict in sandbox/hello.c
Automatic merge failed; fix conflicts and then commit the result.
\end{minted}
Ce qui vient de se passer est que {\em Bob} et {\em Alice} ont fait
des modifications au même endroit du même fichier dans les commits
qu'ils ont faits chacun de leur côté (en ajoutant leurs noms sur la
même ligne), et Git ne sait pas quelle version choisir pendant la
fusion~: c'est un conflit, et nous allons devoir le résoudre
manuellement. Allez voir \texttt{hello.c}.

La bonne nouvelle, c'est que les modifications faites par \textit{Alice} et Bob
sur des endroits différents du fichier ont été fusionnées. Quand une
équipe est bien organisée et évite de modifier les mêmes endroits en
même temps, ce cas est le plus courant~: les développeurs font les
modifications, et le gestionnaire de versions fait les fusions
automatiquement.

En haut du fichier, on trouve~:
\begin{minted}{diff}
<<<<<<< HEAD
/* Auteurs : Alice et ... */
=======
/* Auteurs : ... et Bob */
>>>>>>> 2483c228b1108e74c8ca4f7ca52575902526d42a
\end{minted}

Les lignes entre \mintinline{diff}{<<<<<<<} et \mintinline{diff}{=======} contiennent la
version de votre commit (qui s'appelle HEAD). Les lignes entre
\mintinline{diff}{=======} et \mintinline{diff}{>>>>>>>} contiennent la version que nous
venons de récupérer par « pull » (nous avions dit qu'il était identifié
par la chaîne 2483c22, en fait, l'identifiant complet est plus long,
nous le voyons ici).

Il faut alors « choisir » dans \texttt{hello.c} la version qui convient
(ou même la modifier). Ici, on va fusionner à la main (\textit{i.e.} avec un
éditeur de texte) et remplacer l'ensemble par ceci~:

\begin{minted}{c}
/* Auteurs : Alice et Bob */
\end{minted}

Si {\em Alice} fait à nouveau \mintinline{bash}{git status}, on voit apparaître~:
\begin{minted}{console}
alice@laptop1$ git status
On branch master
Your branch and 'origin/master' have diverged,
and have 1 and 1 different commit(s) each, respectively.

Unmerged paths:
  (use "git add/rm <file>..." as appropriate to mark resolution)

      both modified:      hello.c

no changes added to commit (use "git add" and/or "git commit -a")
\end{minted}
Si on n'est pas sûr de soi après la résolution des conflits, on peut
lancer la commande~:
\begin{minted}{console}
alice@laptop1$ git diff    # git diff sans argument, alors qu'on avait l'habitude d'appeler 'git diff HEAD'
\end{minted}
Après un conflit, Git affichera quelque chose comme~:
\begin{minted}{diff}
diff --cc hello.c
index 5513e89,614e4b9..0000000
--- a/hello.c
+++ b/hello.c
@@@ -1,5 -1,5 +1,5 @@@
  /* Chacun ajoute son nom ici */
- /* Auteurs : Alice et ... */
 -/* Auteurs : ... et Bob */
++/* Auteurs : Alice et Bob */
  
  #include <stdio.h>

\end{minted}
(les '+' et les '-' sont répartis sur deux colonnes, ce qui correspond
aux changements par rapport aux deux « commits » qu'on est en train de
fusionner. Si vous ne comprenez pas ceci, ce n'est pas très grave !)

Après avoir résolu manuellement les conflits à l'intérieur du fichier,
on marque ces conflits comme résolus, explicitement, avec%
\mintinline{sh}{git add}~:

\begin{minted}{console}
alice@laptop1$ git add hello.c
alice@laptop1$ git status
On branch master
Your branch and 'origin/master' have diverged,
and have 1 and 1 different commit(s) each, respectively.

Changes to be committed:

      modified:   hello.c
\end{minted}

On note que \texttt{hello.c} n'est plus considéré « both modified »
(c'est-à-dire contient des conflits non-résolus) par Git, mais simplement
comme « modified ».

Quand il n'y a plus de fichier en conflit, il faut faire un commit
(comme « git pull » nous l'avait demandé)~:
\begin{minted}{console}
alice@laptop1$ git commit
\end{minted}
(Dans ce cas, il est conseillé, même pour un débutant, de ne pas
utiliser l'option \mintinline{sh}{-a}, mais c'est un détail)

Un éditeur s'ouvre, et propose un message de commit du type « \texttt{Merge
branch 'master' of ...} », on peut le laisser tel quel, sauver et
quitter l'éditeur.

NB~: s'il n'y avait pas eu de conflit, ce qui est le cas le plus
courant, « git pull » aurait fait tout cela~: télécharger le nouveau
commit, faire la fusion automatique, et créer si besoin un nouveau
commit correspondant à la fusion.

On peut maintenant regarder plus en détails ce qu'il s'est passé~:
\begin{minted}{console}
alice@laptop1$ gitk
\end{minted}
Pour \textit{Alice}, on voit apparaître les deux « commit » fait par {\em Bob} et
{\em Alice} en parallèle, puis le « merge commit » que nous venons de créer
avec « git pull ». Pour {\em Bob}, rien n'a changé.

La fusion étant faite, {\em Alice} peut mettre à disposition son travail
(le premier commit, manuel, et le commit de fusion) avec~:
\begin{minted}{console}
alice@laptop1$ git push
\end{minted}
et {\em Bob} peut récupérer le tout avec~:
\begin{minted}{console}
bob@laptop2$ git pull
\end{minted}
(cette fois-ci, aucun conflit, tout se passe très rapidement et en une
commande)

Les deux utilisateurs peuvent comparer ce qu'ils ont avec~:
\begin{minted}{console}
both@laptops$ gitk
\end{minted}
Ils ont complètement synchronisé leurs répertoires. On peut également
faire~:
\begin{minted}{console}
both@laptops$ git pull
both@laptops$ git push
\end{minted}
Mais ces commandes se contenteront de répondre \texttt{Already up-to-date.}
et \texttt{Everything up-to-date}.

\subsection{Ajout de fichiers}

À présent, {\em Alice} crée un nouveau fichier, \texttt{toto.c},
avec un contenu quelconque.

{\em Alice} fait~:
\begin{minted}{console}
alice@laptop1$ git status
\end{minted}
On voit apparaître~:
\begin{minted}{console}
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)

      toto.c
nothing added to commit but untracked files present (use "git add" to track)
\end{minted}
Notre fichier \texttt{toto.c} est considéré comme « Untracked » (non suivi
par Git). Si on veut que \texttt{toto.c} soit ajouté au dépôt, il faut
l'enregistrer (\texttt{git commit} ne suffit pas)~: \texttt{git add toto.c}

{\em Alice} fait à présent~:
\begin{minted}{console}
alice@laptop1$ git status
\end{minted}
On voit apparaître~:
\begin{minted}{console}
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

      new file:   toto.c
\end{minted}

{\em Alice} fait à présent (-m permet de donner directement le message
de log)~:
\begin{minted}{console}
alice@laptop1$ git commit -m "ajout de toto.c"
\end{minted}
On voit apparaître~:
\begin{minted}{console}
[master b1d56e6] Ajout de toto.c
 1 files changed, 4 insertions(+), 0 deletions(-)
 create mode 100644 toto.c
\end{minted}
\texttt{toto.c} a été enregistré dans le dépôt. {\em Alice} peut publier ce
changement~:
\begin{minted}{console}
alice@laptop1$ git push
\end{minted}
{\em Bob} fait à présent~:
\begin{minted}{console}
bob@laptop2$ git pull
\end{minted}
Après quelques messages informatifs, on voit apparaître~:
\begin{minted}{console}
Fast forward
 toto.c |    4 ++++
 1 files changed, 4 insertions(+), 0 deletions(-)
 create mode 100644 toto.c
\end{minted}
Le fichier \texttt{toto.c} est maintenant présent chez {\em Bob}.

\subsection{Fichiers ignorés par Git}

{\em Bob} crée à présent un nouveau fichier \texttt{temp-file.txt}, puis
fait un \mintinline{console}{git status}. On voit maintenant apparaître~:
\begin{minted}{console}
bob@laptop2$ git status
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)

      temp-file.txt

nothing added to commit but untracked files present (use "git add" to track)
\end{minted}
Si {\em Bob} souhaite que le fichier \texttt{temp-file.txt} ne soit pas enregistré
dans le dépôt (soit « ignoré » par Git), il doit placer son nom dans un
fichier \texttt{.gitignore} dans le répertoire contenant \texttt{temp-file.txt}.
Concrètement, {\em Bob} tape la commande
\begin{minted}{console}
bob@laptop2$ emacs .gitignore
\end{minted}
et ajoute une ligne
\begin{verbatim}
temp-file.txt
\end{verbatim}
puis sauve et quitte.

Dans le répertoire \texttt{sandbox/} qui vous est fourni, il existe
déjà un fichier \texttt{.gitignore} qui peut vous servir de base pour
vos projets.

Si {\em Bob} souhaite créer un nouveau \texttt{.gitignore} (par
exemple, à la racine du projet pour que les règles s'appliquent sur
tout le projet), pour que tous les utilisateurs du dépôt
bénéficient du même fichier \texttt{.gitignore}, {\em Bob} fait~:
\begin{minted}{console}
bob@laptop2$ git add .gitignore
\end{minted}
{\em Bob} fait à nouveau un \mintinline{console}{git status}, et on voit apparaître~:
\begin{minted}{console}
bob@laptop2$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

      new file:   .gitignore

\end{minted}
Quelques remarques~:

\begin{itemize}
\item Le fichier \texttt{temp-file.txt} n'apparaît plus. C'était le but
  de la manœuvre. Une bonne pratique est de faire en sorte que « git
  status » ne montre jamais de « Untracked files »~: soit un fichier
  doit être ajouté dans le dépôt, soit il doit être explicitement
  ignoré. Cela évite d'oublier de faire un « git add ».
\item En général, on met dans les \texttt{.gitignore} les fichiers
  générés (*.o, fichiers exécutables, \dots{}), ce qui est en partie fait
  pour vous dans le \texttt{.gitignore} du répertoire
  \texttt{sandbox/} (qu'il faudra adapter pour faire le
  \texttt{.gitignore} de votre projet). Les « wildcards » usuels
  (\texttt{*.o}, \texttt{*.ad?}, \dots{}) sont acceptés pour ignorer
  plusieurs fichiers.
\item Le fichier \texttt{.gitignore} vient d'être ajouté (ou bien il est
  modifié s'il était déjà présent). Il faut à nouveau faire un commit
  et un push pour que cette modification soit disponible pour tout le
  monde.
\end{itemize}

\section{Pour conclure\dots{}}

Bien sûr, Git est bien plus que ce que nous venons
de voir, et nous encourageons les plus curieux à se plonger dans le
manuel utilisateur et les pages de man de Git pour en apprendre plus.
Au niveau débutant, voici ce qu'on peut retenir~:

\subsection*{Les commandes}
\begin{description}
  \item[git commit] enregistre l'état courant de tous les fichiers qui
    ont été au préalable ajoutés par \texttt{git add},
 \item[git commit -a] enregistre l'état courant du répertoire de travail,
 \item[git push] publie les commits,
 \item[git pull] récupère les commits publiés,
 \item[git add, git rm et git mv] permettent de dire à Git quels
   fichiers il doit surveiller (``traquer'' ou ``versionner'' dans le
   jargon),
 \item[git status, git diff HEAD] pour voir où on en est.
\end{description}

\section*{Conseils pratiques}
\begin{itemize}
\item Ne \emph{jamais} s'échanger des fichiers sans passer par Git
  (email, scp, clé USB), sauf si vous savez \emph{vraiment} ce que
  vous faites.
  % \item Toujours utiliser \mintinline{sh}{git commit} avec l'option \mintinline{sh}{-a}.
\item Faire souvent \mintinline{sh}{git status}, pour observer l'état du dépôt local.
\item Faire un \mintinline{sh}{git push} souvent, sauf si on veut garder ses
  modifications privées. Il peut être nécessaire de faire un %
  \mintinline{sh}{git pull} avant un \mintinline{sh}{git push} si des nouvelles révisions sont
  disponibles dans le dépôt partagé.
\item Faire des \mintinline{sh}{git pull} régulièrement pour rester synchronisés
  avec vos collègues. Il faut faire un \mintinline{sh}{git commit -a} avant de
  pouvoir faire un \mintinline{sh}{git pull} (ce qui permet de ne pas mélanger
  modifications manuelles et fusions automatiques).
\item Ne faites jamais un « \mintinline{sh}{git add} » sur un fichier binaire
  généré~: si vous les faites, attendez-vous à des conflits à chaque
  modification des sources ! Git est fait pour gérer des fichiers
  sources, pas des binaires.
\end{itemize}
(quand vous ne serez plus débutants\footnote{cf. par exemple
  \url{http://ensiwiki.ensimag.fr/index.php/Maintenir_un_historique_propre_avec_Git}},
vous verrez que la vie n'est pas
si simple, et que la puissance de Git vient de \mintinline{sh}{git commit} sans
\mintinline{sh}{-a}, des \mintinline{sh}{git commit} sans \mintinline{sh}{git push}, \dots{} mais chaque
chose en son temps !)

\subsection*{Quand rien ne va plus\dots{}}

En cas de problème avec l'utilisation de Git~:

\begin{itemize}
\item Consulter la page
  \url{http://ensiwiki.ensimag.fr/index.php/FAQ_Git} sur EnsiWiki.
  Cette page a été écrite pour le projet GL, mais la plupart des
  explications s'appliquent directement pour vous,
\item Demander de l'aide aux enseignants,
\item Demander de l'aide sur la mailing-list de Git.
\end{itemize}

Dans tous les cas, lire la documentation est également une bonne
idée~: \url{http://git-scm.com/documentation} ! Par exemple, le livre
numérique de Scott Chacon « Pro Git », simple d'accès et traduit en
français~: \url{http://git-scm.com/book/fr/v2}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "francais"
%%% End:
