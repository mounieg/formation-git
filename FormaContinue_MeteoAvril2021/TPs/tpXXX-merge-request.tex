\documentclass[a4paper,10pt]{article}

\usepackage{header}

\date{2021}
\author{Sylvain Bouveret, Grégory Mounié}
\title{Git -- Merge request flow}

\sloppy

\begin{document}
\maketitle

Ce document peut être téléchargé depuis l'adresse suivante :
\begin{center}
  \url{\gitlabpages/\jobname.pdf}
\end{center}

\section{Introduction}

Dans les collections d'entrepots git (Github, Gitlab, ...), l'un des
schémas courants de contribution consiste à cloner un entrepot chez
soi, à y appliquer ses contributions puis à faire une \og merge
request\fg{}. Cette contribution étant rarement parfaite, nous allons
faire aussi pratiquer le cycle de discussion avec le responsable de
l'intégration.

\todo[inline]{Reprendre le scénario de nettoyage avec le dumb projet ? faire
  un nettoyage à distance et montrer que c'est dangereux ? montrer \og
  pull --reset\fg{} ?}


\end{document}

La clarté de l'historique est un préalable à la production de logiciel
de qualité. Dans cette session nous allons~:
\begin{itemize}
\item nettoyer d'abord un petit historique~;
\item rechercher un commit par bissection dans un historique de grande
  taille.
\end{itemize}

Pour cette session, les différents exercices peuvent être réalisés
seuls.

\section{Nettoyage d'un historique}

Le nettoyage de l'historique change les commits. Cela signifie
qu'\textbf{il faut réaliser ce nettoyage avant de publier les commits}
et qu'ils ne soient récupérés dans un autre dépot. En effet, sinon les deux
historiques seront incohérents et git ne pourra plus gérer les opérations
de fusion.

Dans ce TP, nous nettoyerons l'historique d'un dépot déjà publié, ce
qui ne faut jamais faire en pratique.

\subsection{Mise en place}

Cloner le dépot suivant contenant un historique perfectible:
\begin{minted}{console}
$ git clone git@gitlab.ensimag.fr:git/dumb-project.git
\end{minted}

Observer l'historique, par exemple avec~:
\begin{minted}{console}
$ git log --graph -c --pretty=full
\end{minted}

\subsection{Reconstruire l'historique}

Vous pouvez observer dans cet historique un certain nombre de
problèmes~: coquilles dans les messages de commit, commits qui sont
visiblement mal séparés ou redondants, etc.

Tout ce que vous avez à faire est d'utiliser la commande
\mintinline{bash}{git rebase -i} pour reconstruire l'historique à votre
convenance. Procédez par petits pas plutôt que de tout faire d'un
coup. Git vous permet de faire plusieurs reconstructions d'historique
à la suite. Pour remonter au commit d'origine, vous pouvez utiliser~:
\begin{minted}{console}
$ git rebase -i --root
\end{minted}

Cela affichera votre éditeur préféré, celui configuré par défaut, avec le contenu suivant:
\begin{minted}{ini}
pick 27f44e5 Initial revision
pick 8900bfb Add diff, test it, and test add better.
pick 2612865 Rename diff to sub
pick 48a2fa4 Rename diff to sub in tests too
pick df7eae8 add fnuctino f

# Rebasage de df7eae8 sur f223221 (5 commandes)
#
# Commandes :
#  p, pick <commit> = utiliser le commit
#  r, reword <commit> = utiliser le commit, mais reformuler son message
#  e, edit <commit> = utiliser le commit, mais s'arrêter pour le modifier
#  s, squash <commit> = utiliser le commit, mais le fusionner avec le précédent
#  f, fixup <commit> = comme "squash", mais en éliminant son message
#  x, exec <commit> = lancer la commande (reste de la ligne) dans un shell
#  b, break = s'arrêter ici (on peut continuer ensuite avec 'git rebase --continue')
#  d, drop <commit> = supprimer le commit
#  l, label <label> = étiqueter la HEAD courante avec un nom
#  t, reset <label> = réinitialiser HEAD à label
#  m, merge [-C <commit> | -c <commit>] <label> [# <uniligne>]
#          créer un commit de fusion utilisant le message de fusion original
#          (ou l'uniligne, si aucun commit de fusion n'a été spécifié).
#          Utilisez -c <commit> pour reformuler le message de validation.
#
# Vous pouvez réordonner ces lignes ; elles sont exécutées de haut en bas.
#
# Si vous éliminez une ligne ici, LE COMMIT CORRESPONDANT SERA PERDU.
#
# Cependant, si vous effacez tout, le rebasage sera annulé.
#
\end{minted}
Sinon, à la place de l'option \mintinline{bash}{--root}, vous pouvez
également utiliser un numéro de commit particulier.

Nous vous proposons de faire les modifications suivantes:
\begin{enumerate}
\item découper le deuxième commit en deux ou trois morceaux
\item fusionner le troisième et quatrième commit avec l'option de
  votre choix
\item reformuler le message du dernier commit
\end{enumerate}

Pour séparer un commit en plusieurs morceaux, le plus simple est
d'arrêter le rebase sur le commit à séparer pour une édition (edit),
de le défaire (avec reset), puis d'enregistrer les nouveaux commits
avant de continuer.
\begin{minted}{console}
$ git rebase -i --root
# mettre le commit à séparer à "edit"
# et demander aussi les autres modifications
$ git reset HEAD^
$ git add ...
$ git commit ...
$ git add ...
$ git commit ...
$ git rebase --continue
\end{minted}

\section{Recherche par bissection}

Afin d'illustrer le principe de la recherche par bissection, nous
allons récupérer un dépôt de grande taille~: celui de Git
lui-même. Récupérez le dépôt du projet en le clonant~:
\begin{minted}{console}
$ git clone https://github.com/git/git.git
\end{minted}

L'une des fonctionnalités qui a été ajoutée à Git est la possibilité
de synchroniser un dépôt avec Mediawiki. Mais quand donc cette
fonctionnalité a-t-elle été introduite ? Nous allons faire une
recherche dans l'historique.

Tout d'abord, vérifiez que cette fonctionnalité est bien présente dans
la version actuelle du projet. Le plus simple pour cela est de tester
l'existence du répertoire \lstinline{contrib/mw-to-git}.
\begin{minted}{sh}
[ -d contrib/mw-to-git ] && echo "Le répertoire existe" || echo "Le répertoire n'existe pas"
\end{minted}

Nous allons débuter la recherche par bissection~:
\begin{minted}{console}
$ git bisect start
\end{minted}

Git bisect est un outil plutôt utilisé pour remonter à l'origine de
problèmes dans les programmes (plutôt que pour savoir quand telle ou
telle fonctionnalité a été introduite). Le raisonnement à adopter est
donc le suivant~: la fonctionnalité Mediawiki est nocive
(\textit{bad})~; nous cherchons à remonter au premier commit dans
lequel cette fonctionnalité nocive est apparue.

Nous partons de la tête de la branche master. Comme nous avons pu le
vérifier préalablement, le répertoire \mintinline{bash}{contrib/mw-to-git}
existe bien dans l'état actuel. C'est donc qu'il faut remonter dans
l'historique pour trouver le premier commit dans lequel ce répertoire
a été versionné. Pour cela, nous disons à git que ce commit est
mauvais~:
\begin{minted}{console}
$ git bisect bad
\end{minted}

Ensuite, il faut remonter (manuellement pour cette fois-ci) à un
commit antérieur. Remontons à la version 1.0.0 de git, et testons si
la fonctionnalité est déjà présente ou non. Normalement, elle ne
devrait pas l'être, donc nous marquons ce commit comme \texttt{good}~:
\begin{minted}{console}
$ git checkout v1.0.0
$ [ -d contrib/mw-to-git ] && echo "Le répertoire existe" || echo "Le répertoire n'existe pas"
Le répertoire n'existe pas
$ git bisect good
\end{minted}

La suite est semi-automatique. Après avoir réfléchi un petit moment,
git va vous proposer un commit intermédiaire. À vous de déterminer si
le répertoire est présent ou pas. S'il est présent, il faut marquer ce
commit comme \texttt{bad}. Sinon, il faut le marquer comme
\texttt{good}.

Après quelques itérations, vous devriez tomber sur le commit ayant
permis l'apparition de cette fonctionnalité. Qui en est responsable~?
Est-ce un bon exemple de message de commit~?

Terminez l'exercice en revenant à la position initiale~:
\begin{minted}{console}
$ git bisect reset
\end{minted}

\section{Pour conclure\dots{}}

Vous avez appris dans ce TP à maîtriser l'historique de vos
dépôts. Vous avez vu comment on peut utiliser la bissection pour
rechercher à quel moment une fonctionnalité a été introduite. Vous
avez également vu comment réécrire l'histoire (\emph{sur un dépôt
  local uniquement}) en utilisant \texttt{rebase}. Attention, en
rebasant, vous modifiez l'historique de votre dépôt, donc vous pouvez
potentiellement perdre des informations sur les états
intermédiaires\footnote{En fait, en général rien n'est vraiment perdu
  dans git, tout peut être retrouvé lorsque l'on connaît les bons
  numéros de commits ou alors les noms des références permettant de
  s'y retrouver. Mais ça peut être plus compliqué...}.


\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
