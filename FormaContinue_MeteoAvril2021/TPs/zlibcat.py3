#!/usr/bin/python3

import sys
import zlib

sys.stdout.buffer.write(zlib.decompress(sys.stdin.buffer.read()))
