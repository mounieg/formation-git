\documentclass[a4paper]{scrartcl}

\usepackage{header}

\newcommand{\annee}{2021}

\date{\annee}
\author{Sylvain Bouveret, Grégory Mounié}
\title{Git -- Gestion des branches et des sources}

\sloppy

\begin{document}
\maketitle

Ce document peut être téléchargé depuis l'adresse suivante :
\begin{center}
  \url{\gitlabpages/\jobname.pdf}
\end{center}

\section{Introduction}

\begin{figure}[hbtp]
  \centering
  \includegraphics[width=10cm]{../Slides/images/vincent_driessen_model.png}
  \caption{Le modèle de branches de git-flow de Vincent Driessen \url{http://nvie.com/posts/a-successful-git-branching-model/}}
  \label{fig:gitflow}
\end{figure}

Le but premier de ce TP est de vous familiariser avec l'utilisation
des branches. Nous allons donc créer des branches, sauter d'une
branche à l'autre et faire de nombreuses fusions (merge). L'autre
point du TP sera de vous amener à manipuler plusieurs entrepôts
distants simultanément. Un serveur «public» diffusera la branche
«master» tandis qu'un serveur de «développement» stockera l'ensemble des
autres branches.

\section{Git et les branches}

\subsection{Mise en place}

Ce TP sera lui aussi réalisé en équipe de deux utilisateurs (Alice et
Bob dans le reste du sujet). Les explications sont écrites pour 2
utilisateurs pour simplifier, mais il peut y avoir un nombre
quelconque de coéquipiers.


La première étape, sur votre PC, est de construire d'abord un dépôt
minimaliste. Ce dépôt sera copié, plus tard, sur
\mintinline{sh}{depots.ensimag.fr}.

\textit{Alice} crée sur sa machine le dépôt local initial.
\begin{minted}{console}
alice@laptop1$ mkdir alice-et-bob
alice@laptop1$ cd alice-et-bob
alice@laptop1$ git init .
alice@laptop1$ mon_editeur_prefere fichier.txt # mettre quelques lignes
alice@laptop1$ git add fichier.txt
alice@laptop1$ git commit
\end{minted}

À chaque étape, pensez à afficher le graphe de commit pour en suivre
l'évolution, soit en mode texte avec~:
\begin{minted}{console}
$ git log --all --graph --oneline
\end{minted}
soit en mode graphique avec~:
\begin{minted}{console}
$ gitk --all
\end{minted}

Suivant les modifications que vous ferez à \texttt{fichier.txt}, il
faudra peut-être aussi gérer quelques conflits.

\subsection{Créer deux dépôts vides sur \texttt{gitlab.ensimag.fr}}

\textit{Bob} va, lui, créer deux dépôts vides sur
\texttt{gitlab.ensimag.fr}.

Référez-vous au TP1 pour créer le groupe de développeurs, nommé en
fonction de vos logins, comme, par exemple \verb!alice_bob!, s'il
n'existe pas déjà, et deux projets vides dans ce groupe.

Les deux projets se nommeront:
\begin{enumerate}
\item \texttt{projet2-public}
\item \texttt{projet2-dev}  
\end{enumerate}

\subsection{Ajouter deux sources distantes (remote) et y
  pousser le code de départ}

\textit{Alice} ajoute deux sources distantes (remote) à son dépôt et
pousse sa version initiale dans les deux dépôts.

\begin{minted}{console}
alice@laptop1$ cd alice-et-bob
alice@laptop1$ git remote add origin ssh://git@gitlab.ensimag.fr/alice_bob/projet2-public.git
alice@laptop1$ git remote add dev ssh://git@gitlab.ensimag.fr/alice_bob/projet2-dev.git
alice@laptop1$ git remote -v
alice@laptop1$ git push origin master
alice@laptop1$ git push dev master
\end{minted}

Maintenant, \textit{Bob} peut cloner un des deux dépôts et ajouter
l'autre source, lui aussi.

\begin{minted}{console}
bob@laptop2$ git clone ssh://git@gitlab.ensimag.fr/alice_bob/projet2-public.git
bob@laptop2$ cd projet2-public
bob@laptop2$ git remote -v
bob@laptop2$ git remote add dev ssh://git@gitlab.ensimag.fr/alice_bob/projet2-dev.git
bob@laptop2$ git remote -v
\end{minted}

\subsection{Ajouter des tags sur la version «master»}

\textit{Alice} ajoute le tag \texttt{v0.0} à la version initiale de la
branche «master». Elle devra écrire un message associé au tag
(option -a \texttt{--annotate}). Cela permet d'associer au tag une date
de création, l'identité du créateur, et éventuellement sa signature
GnuPG.

\begin{minted}{console}
alice@laptop1$ git tag -a v0.0
\end{minted}

Mais ce tag n'existe encore que localement. \textit{Alice} pousse le tag
dans les deux dépôts. Elle peut le faire en poussant uniquement le
tag, ou en poussant tous les tags.
\begin{minted}{console}
alice@laptop2$ git push origin v0.0
alice@laptop2$ git push --tags dev
\end{minted}

\textit{Bob} récupère alors le tag de manière implicite lorsqu'il fait un
git pull. Cela ne fonctionne de manière implicite que pour les tags
concernant des objets présents ou tirés dans le dépôt de \textit{Bob}.

\begin{minted}{console}
bob@laptop2$ git pull
\end{minted}

\textit{Bob} crée un second commit en modifiant \texttt{fichier.txt} et
ajoute le tag annoté \texttt{v1.0}. Il pousse sa modification et le
tag dans les deux dépôts.

\begin{minted}{console}
bob@laptop2$ mon_editeur_prefere fichier.txt
bob@laptop2$ git add -p fichier.txt
bob@laptop2$ git commit
bob@laptop2$ git tag -a v1.0
bob@laptop2$ git push --tags dev
bob@laptop2$ git push --tags
bob@laptop2$ git push
bob@laptop2$ git push dev
\end{minted}

Le dépôt public «origin» de \textit{Bob} est son dépôt par défaut.

\textit{Alice} peut maintenant le récupérer. Pour ce faire, elle va
indiquer «origin» comme dépôt par défaut de sa branche «master».
\begin{minted}{console}
alice@laptop1$ git branch --set-upstream-to=origin/master master
alice@laptop1$ git pull
\end{minted}
Elle a noté que l'étiquette «remote/dev/master» est toujours sur le
premier commit.

\subsection{Ajouter une branche «develop» dans dev}

Pour faciliter la maintenance et le développement, {\em Alice} ajoute
une branche «develop».
\begin{minted}{shell-session}
alice@laptop1$ git checkout -b develop
alice@laptop1$ git branch
* develop
  master
\end{minted}

puis elle ajoute un commit sur cette branche et la pousse dans
«dev». Par défaut, Git pousse la branche courante. Elle en profite pour
indiquer à Git que c'est «dev» qui sera le dépôt par défaut où pousser
cette branche
\begin{minted}{console}
alice@laptop1$ mon_editeur_prefere fichier.txt
alice@laptop1$ git add -p
alice@laptop1$ git commit
alice@laptop1$ git push --set-upstream dev
\end{minted}

{\em Bob} récupère la branche \texttt{develop} et bascule sa
position courante (HEAD) dessus.
\begin{minted}{console}
bob@laptop2$ git pull dev
\end{minted}

Un «pull» c'est deux opérations: un «fetch» qui récupère les nouvelles
informations du dépôt, ici, la branche «develop» et son commit, et un «merge».

Git vous indique que comme la branche «master» n'est pas configurée
pour suivre «dev» (elle est configurée pour suivre «origin»), il faut
préciser la branche pour le «merge».
\begin{minted}{console}
bob@laptop2$ git checkout develop
\end{minted}

Si tout va bien, Git indique que la branche est configurée pour suivre
l'évolution de la branche de même nom sur «dev».


\subsection{Faire une branche de fonctionnalité et la fusionner}

\textit{Bob} veut implanter une nouvelle fonctionnalité. Il vérifie qu'il
est bien sur la branche «develop». Il ajoute une branche
(«topic1»), code la fonctionnalité et la publie dans
«dev».

\begin{minted}{console}
bob@laptop2$ git status
On branch develop
Your branch is up to date with 'dev/develop'.
...
bob@laptop2$ git checkout -b topic1
bob@laptop2$ mon_editeur_prefere fichier.txt
bob@laptop2$ git add -p
bob@laptop2$ git commit
bob@laptop2$ git push --set-upstream dev topic1
\end{minted}

\textit{Alice} veut implanter, elle aussi, une nouvelle
fonctionnalité. Elle le fait dans la branche «topic2» qui part de
«develop». Elle enregistre, elle aussi, son commit et le publie dans
«dev».

\begin{minted}{console}
alice@laptop1$ git status
On branch develop
Your branch is up to date with 'dev/develop'.
...
alice@laptop1$ git checkout -b topic2
alice@laptop1$ mon_editeur_prefere fichier.txt
alice@laptop1$ git add -p
alice@laptop1$ git commit
alice@laptop1$ git push --set-upstream dev topic2
\end{minted}

Après l'avoir testé rigoureusement \textit{Bob} veut fusionner «topic1»
dans «develop».

\begin{minted}{console}
bob@laptop2$ git checkout develop
bob@laptop2$ git merge topic1
bob@laptop2$ git push
bob@laptop2$ gitk --all
\end{minted}

Regardez attentivement le graphe de commit. Git n'a pas créé de
commit pour ce merge. Il a juste déplacé la tête. Le terme Git
désignant ce type de merge sans commit est «fast-forwarding».

\textit{Alice} a une idée pour améliorer cette contribution. Elle
récupère le travail de \textit{Bob} (en utilisant juste le fetch), ajoute
un commit dans la branche «topic1» mais elle veut garder une trace de
la fusion. Elle utilise l'option \texttt{--no-ff} lors du merge. Elle
devra aussi mettre à jour sa branche «develop» avant.

\begin{minted}{console}
alice@laptop1$ git fetch dev
alice@laptop1$ git checkout topic1
alice@laptop1$ mon_editeur_prefere fichier.txt
alice@laptop1$ git add -p
alice@laptop1$ git commit
alice@laptop1$ git checkout develop
alice@laptop1$ git pull
alice@laptop1$ git merge --no-ff topic1
alice@laptop1$ git push
alice@laptop1$ gitk --all
\end{minted}

Cette fois-ci, Git a demandé un message de
merge et a créé un commit supplémentaire.

La forme finale du graphe de commits dépend de l'usage, ou non,
de l'option \texttt{--no-ff}. Dans le reste du sujet, \textit{Alice} et
\textit{Bob} utiliserons cette option pour tracer les dates, et les
responsables des «merge», dans les branches «master» et «develop».

\subsection{Repositionner topic2 avec rebase}

\textit{Bob} reprend le travail \textit{Alice} sur le «topic2». Il rebase la
branche sur la nouvelle version de «develop». Il aura peut-être à
gérer des conflits. Il faudra aussi faire un pull (pour le
merge) après le rebase avant de pouvoir faire le push.

\begin{minted}{console}
bob@laptop2$ git fetch dev
bob@laptop2$ git checkout topic2
bob@laptop2$ git rebase develop
bob@laptop2$ git pull
bob@laptop2$ git push
bob@laptop2$ gitk --all
\end{minted}

\subsection{La cachette}

\textit{Alice} récupère les modifications de «topic2» et continue les
modifications de \texttt{fichier.txt}. Elle note alors un horrible bug
et ce bug existe aussi dans la version «master».

Comme elle est au milieu de sa modification, elle ne veut pas faire un
commit qui ne compilerait pas. Elle ne veut pas non plus perdre son
travail.

Elle va donc sauvegarder le travail en cours dans la cachette (stash),
pour repartir du dernier commit et elle le reprendra après la
correction du bug.

\begin{minted}{console}
alice@laptop1$ git checkout topic2
alice@laptop1$ git pull
alice@laptop1$ mon_editeur_prefere fichier.txt # modification incomplète
alice@laptop1$ git status
alice@laptop1$ git stash
alice@laptop1$ git status
alice@laptop1$ git stash list
\end{minted}

\subsection{Faire une correction de bug}

Pour faire faire la correction \textit{Alice} reprend le travail depuis
la version v1.0 de «master». Elle crée alors une nouvelle branche,
«hotfix1», pour y écrire la correction.

\begin{minted}{console}
alice@laptop1$ git checkout v1.0 # attention tête détachée !
alice@laptop1$ git checkout -b hotfix1
\end{minted}

Elle modifie \texttt{fichier.txt} et crée un commit dans
«hotfix1».
\begin{minted}{console}
alice@laptop1$ mon_editeur_prefere fichier.txt
alice@laptop1$ git add -p
alice@laptop1$ git commit
alice@laptop1$ gitk --all
\end{minted}

Elle fusionne ensuite ce commit avec «master» et «develop», en prenant
soin de bien créer un nouveau commit dans les deux cas. Le nouveau
commit de «master» sera tagué \texttt{v1.1} et publié sur
«origin». Elle pourrait avoir à gérer quelques conflits.

\begin{minted}{console}
alice@laptop1$ git checkout master
alice@laptop1$ git merge --no-ff hotfix1
alice@laptop1$ git tag -a v1.1
alice@laptop1$ git push --tags
alice@laptop1$ git push
alice@laptop1$ git checkout develop
alice@laptop1$ git merge --no-ff hotfix1
alice@laptop1$ git push
alice@laptop1$ gitk --all
\end{minted}

\subsection{Sortir de la cachette}

\textit{Alice} retourne dans la branche «topic2». Elle repositionne la
branche par rapport à «develop» et réinsère les changements sur
lesquels elle travaillait. Elle aura peut-être quelques conflits à
résoudre. Il faudra aussi fusionner la version distante et locale de
«topic2».

\begin{minted}{console}
alice@laptop1$ git checkout topic2
alice@laptop1$ git rebase develop
alice@laptop1$ git pull
alice@laptop1$ git stash list
alice@laptop1$ git stash pop
\end{minted}

Ensuite \textit{Alice} finit ses modifications, enregistre un commit et
fusionne la branche dans «develop» en prenant soin de bien créer un
nouveau commit.

\begin{minted}{console}
alice@laptop1$ mon_editeur_prefere fichier.txt
alice@laptop1$ git add -p
alice@laptop1$ git commit
alice@laptop1$ git checkout develop
alice@laptop1$ git merge --no-ff topic2
alice@laptop1$ git push
alice@laptop1$ gitk --all
\end{minted}

\textit{Alice} remarque que la branche distante de «topic2» est en retard.
Pour pousser l'ensemble des distantes branches en retard
(comme «topic2») \textit{Alice} effectue
\begin{minted}{console}
alice@laptop1$ git push --all
\end{minted}

\subsection{Nouvelle version majeure}

\textit{Bob} va publier la nouvelle version. Il récupère les
modifications et crée une nouvelle branche \texttt{release2} partant
de \texttt{develop}. Dans cette branche il réalise un nouveau commit
corrigeant quelques problèmes. Il fusionne ensuite cette branche dans
«master» et «develop» en créant un nouveau commit dans les deux
cas. La nouvelle version du «master» est publiée avec le tag v2.0.

\begin{minted}{console}
bob@laptop2$ git fetch dev
bob@laptop2$ git checkout develop
bob@laptop2$ git pull
bob@laptop2$ git checkout -b release2
bob@laptop2$ mon_editeur_prefere fichier.txt
bob@laptop2$ git add -p
bob@laptop2$ git commit
bob@laptop2$ git checkout master
bob@laptop2$ git pull
bob@laptop2$ git merge --no-ff release2
bob@laptop2$ git tag -a v2.0
bob@laptop2$ git push --tags
bob@laptop2$ git push
bob@laptop2$ git checkout develop
bob@laptop2$ git merge --no-ff release2
bob@laptop2$ git push
bob@laptop2$ gitk --all
\end{minted}

\section{En conclusion}

Le but de cette session était de vous montrer que les branches sont
très faciles d'emploi et qu'il n'y a aucune raison de ne pas en abuser
car la fusion n'est pas un problème. L'utilisation de deux dépôts
différents simultanément n'est pas très compliquée non plus.

Il existe encore de nombreuses autres commandes liées à cette gestion
des branches, notamment "cherry-pick" et l'effacement de tags ou de
branches à distance. Mais maintenant vous devriez avoir toutes les
clefs pour les comprendre.

Pour aller plus loin, ou revenir sur certains concepts:
\begin{itemize}
\item \url{https://git-scm.com/book/} Pro Git, un livre couvrant les
  concepts et les usages courants
\item \url{http://git-scm.com} contient une très riche documentation:
  vidéos, références, des tutoriels, des livres libres (Pro Git, Git
  Magic, Git Internals) ou pas.
\end{itemize}

\end{document}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
