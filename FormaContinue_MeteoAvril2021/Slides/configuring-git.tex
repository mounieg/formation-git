\usepackage{moy-bouveret-slides}
\usepackage{tikz}
\usetikzlibrary{shapes}
\usepackage{moy-bouveret-tikz}
\usepackage{comment}

\title[Configuring Git]{Configuring Git}

%\subtitle{...}

\author{Sylvain Bouveret, Grégory Mounié, Matthieu Moy}

\institute[first.last@imag.fr]{[first].[last]@imag.fr\\
\url{https://git.pages.ensimag.fr/formation-git/slides/\jobname.pdf}}

\date{2021}

\begin{document}

\begin{frame}[plain]
  \titlepage

  \begin{center}
    \includegraphics[width=2cm]{images/CC-BY-SA}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Goals of the presentation}

  \begin{itemize}
  \item Explain how to configure git
  \item Explain where and how configuration files are stored
  \item Explain how to tell git to ignore files
  \end{itemize}
\end{frame}

\section{Configuration files}

\begin{frame}[fragile]
  \frametitle{Git Configuration: Which Files}
  
  \begin{itemize}
  \item 3 places
    \begin{itemize}
    \item System-wide: \mintinline{sh}{/etc/gitconfig}
    \item User-wide (``global''): \mintinline{sh}{~/.gitconfig} or
      \mintinline{sh}{~/.config/git/config}
    \item Per-repository: \mintinline{sh}{$project/.git/config}%$
    \end{itemize}
  \item Precedence: per-repo overrides user-wide overrides
    system-wide.
  \item Not versionned by default, not propagated by %
    \mintinline{sh}{git clone}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Git Configuration: Syntax}

  \begin{itemize}
  \item Simple syntax, key/value:
\begin{minted}{ini}
[section1]
	# This is a comment
        key1 = value1 # comment as well
        key2 = value2
[section2 "subsection"]
        key3 = value3
\end{minted}
  \item Semantics:
    \begin{itemize}
    \item \mintinline{ini}{section1.key1} takes value \mintinline{ini}{value1}
    \item \mintinline{ini}{section1.key2} takes value \mintinline{ini}{value2}
    \item \mintinline{ini}{section2.subsection.key3} takes value \mintinline{ini}{value3}
    \end{itemize}
  \item ``section'' and ``key'' are case-insensitive.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Querying/Modifying Config Files}

\begin{minted}{console}
# Modify per-repo .git/config:
$ git config user.name 'Matthieu Moy'
$ cat .git/config
...
[user]
        name = Matthieu Moy
# Modify user-wide ~/.gitconfig:
$ git config --global user.name 'Matthieu Moy'
# Get the value of a variable:
$ git config user.name
Matthieu Moy
\end{minted}
\end{frame}

\begin{frame}
  \frametitle{Some Useful Config Variables}
  \begin{itemize}
  \item User-wide:
    \begin{description}
    \item[user.name, user.email] Who you are (used in \texttt{git
        commit})
    \item[core.editor] Text editor to use for \texttt{commit},
      \texttt{rebase -i}, ...
    \end{description}
  \item Per-repo:
    \begin{description}
    \item[remote.origin.url] Where to fetch/push
    \end{description}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Aliases}
\begin{minted}{sh}
# Definition
$ cat .git/config
...
[alias]
        lg = log --graph --oneline

# Use
$ git lg
*   a5da80c Merge branch 'master' into HEAD
|\  
| * 048e8c1 bar
* | 5034527 boz
|/  
* 1e0e4a5 foo
\end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Documentation about Configuration}
  \begin{itemize}
  \item \mintinline{sh}{man git-config} : documents all configuration
    variables (>~350)
  \item Example:
\begin{minted}{sh}
user.email
    Your email address to be recorded in any
    newly created commits. Can be overridden
    by the GIT_AUTHOR_EMAIL, GIT_COMMITTER_EMAIL,
    and EMAIL environment variables.
    See git-commit-tree(1).
\end{minted}
  \end{itemize}
\end{frame}

\section{(Git)Ignore files}

\begin{frame}
  \frametitle{Ignore Files: Why?}
  \begin{itemize}
  \item Git needs to know which files to track (\mintinline{sh}{git add},
    \mintinline{sh}{git rm})
  \item You don't want to forget a \mintinline{sh}{git add}
  \item $\Rightarrow$ \mintinline{sh}{git status} shows %
    \mintinline{sh}{Untracked files} as a reminder. Two options:
    \begin{itemize}
    \item \mintinline{sh}{git add} them
    \item ask Git to ignore: add a rule to \mintinline{sh}{.gitignore}
    \end{itemize}
  \item Only impacts \mintinline{sh}{git status} and \mintinline{sh}{git add}.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Ignore Untracked Files: How?}
  
  \begin{itemize}
  \item \mintinline{sh}{.gitignore} file contain one rule per line:
\begin{minted}{ini}
# This is a comment

# Ignore all files ending with ~:
*~
# Ignore all files named 'core':
core
# Ignore file named foo.pdf in this directory:
/foo.pdf
# Ignore files in any auto directory:
auto/
# Ignore html file in subdir of any Doc directory:
Doc/**/*.html
\end{minted}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Ignore Files: Where?}
  \begin{itemize}
  \item User-wide: \mintinline{sh}|~/.config/git/ignore|:
    \begin{itemize}
    \item Example: your editor's file like \mintinline{sh}|*~| or \mintinline{sh}|.*.swp|
    \item Don't disturb your co-workers with your personal preferences
    \item Set once and for all
    \end{itemize}
  \item Per-repo, not versionned: \mintinline{sh}|.git/info/exclude|
    \begin{itemize}
    \item Not very useful ;-)
    \end{itemize}
  \item Tracked within the project (\mintinline{sh}{git add} it):
    \mintinline{sh}|.gitignore| in any directory, applies to this directory and
    subdirectories.
    \begin{itemize}
    \item Generated files (especially binary)
    \item Example: \mintinline{sh}|*.o| and \mintinline{sh}|*.so| for a C project
    \item Share with people working on the same project
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{About Generated Files}
  \begin{itemize}
  \item Versionning (\mintinline{sh}{git add}-ing) generated files is bad
    \pause
  \item Versionning generated \alert{binary} files is \alert{very} bad
    \pause
  \item Why?
    \begin{itemize}
    \item breaks \mintinline{sh}{make} (timestamp = \mintinline{sh}{git checkout}
      time)
    \item breaks merge
    \item eats disk space (inefficient delta-compression)
    \end{itemize}
  \end{itemize}
\end{frame}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "american"
%%% End:
