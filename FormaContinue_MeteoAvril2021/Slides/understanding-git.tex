%\documentclass{beamer}

\usepackage{moy-bouveret-slides}
\usepackage{tikz}
\usetikzlibrary{shapes}
\usepackage{moy-bouveret-tikz}
\usepackage{comment}

\title{Understanding Git}

%\subtitle{...}

\author{Sylvain Bouveret, Grégory Mounié, Matthieu Moy}

\institute[first.last@imag.fr]{[first].[last]@imag.fr\\
\url{https://git.pages.ensimag.fr/formation-git/slides/\jobname.pdf}}

\date{2021}

\begin{document}
%\begin{comment}
\begin{frame}[plain]
  \titlepage

  \begin{center}
    \includegraphics[width=2cm]{images/CC-BY-SA}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Goals of the presentation}

  \begin{itemize}
  \item Presenting the data model behind Git
  \item Showing how Git stores data and history
  \item Understanding how to navigate between the commits
    of a repository
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{columns}
    \column{.45\textwidth}
    \begin{center}
      \includegraphics[height=0.7\textheight]{fig/git}
    \end{center}
    \column{.5\textwidth}
    \pause
    \mode<beamer>{
      \begin{tikzpicture}
        \node[rectangle callout, callout relative pointer={(-0.5, 0)},callout pointer width=0.5cm,rounded corners,fill=fg, text width=0.8\textwidth, inner sep=10pt] {
          \color{bg}\footnotesize
          If that doesn't fix it, git.txt contains the phone number
          of a friend of mine who understands git. Just wait through a few
          minutes of ``It's really pretty simple, just think of branches
          as...'' and eventually you'll learn the commands that will fix
          everything.
        };
      \end{tikzpicture}
    }
    \mode<handout>{
      If that doesn't fix it, git.txt contains the phone number
      of a friend of mine who understands git. Just wait through a few
      minutes of ``It's really pretty simple, just think of branches
      as...'' and eventually you'll learn the commands that will fix
      everything.
    }
  \end{columns}
\end{frame}

\begin{frame}
\frametitle{Why do I need to learn about Git's
    internal?}
  \begin{itemize}
  \item Beauty of Git: \alert{very} simple data model\\
    (The tool is clever, the repository format is simple\&stupid)
  \item Understand the model, and the 150+ commands will become
    \alert{simple}!
  \end{itemize}
\end{frame}

\section{Objects, sha1}

\begin{frame}[label=objects] \frametitle{Content of a Git repository: Git objects}

  \tikz[baseline] \draw ++(0,.7ex) node[gitblob] {} node[right=0.3cm, fill=fg!20!bg] {blob}; Any sequence of bytes, represents file content\\
  \tikz[baseline] \draw ++(0,.7ex) node[gittree] {} node[right=0.3cm, fill=fg!20!bg] {tree}; Associates object to pathnames, represents a directory\\
  \only<2->{\tikz[baseline] \draw ++(0,.7ex) node[gitcommit] {} node[right=0.3cm, fill=fg!20!bg] {commit}; Metadata +
    pointer to tree + pointer to parents}

  \begin{center}
    \scalebox{.7}{
    \begin{tikzpicture}
      \draw           node[gitblob](blob1) {};
      \draw ++(3,  0) node[gitblob](blob2) {};
      \draw ++(1.5,2) node[gittree](tree1) {};
      \draw ++(4.5,2) node[gitblob](blob3) {};
      \draw ++(3,4)   node[gittree](tree2) {};
      \draw[commit arc] (tree1) to node[above,sloped]{\small file1.txt} (blob1);
      \draw[commit arc] (tree1) to node[above,sloped]{\small file2.txt} (blob2);
      \draw[commit arc] (tree2) to node[above,sloped]{\small dir1.txt} (tree1);
      \draw[commit arc] (tree2) to node[above,sloped]{\small file3.txt} (blob3);

      \uncover<2->{
      \draw(tree2) ++(0,1.5) node[gitcommit](commit1) {};
      \draw[commit arc] (commit1) to node[right]{tree} (tree2);
      }

      \uncover<3->{
        \draw(commit1) ++(4,0) node[gitcommit](commit2) {};
        \draw[commit arc] (commit2) to node[above]{parent} (commit1);
      }

      \uncover<4->{
        \draw (commit2) ++(0,-1.5)   node[gittree](treecommit2) {};
        \draw[commit arc] (commit2) to node[right]{tree} (treecommit2);
        \draw[commit arc] (treecommit2) to node[above,sloped,near start]{\small dir1.txt}  (tree1);
        \draw (treecommit2) ++(1,-2) node[gitblob](blob3commit2) {};
        \draw[commit arc] (treecommit2) to node[above,sloped]{\small file3.txt} (blob3commit2);
      } 

      \uncover<5->{
        \draw(commit2) ++(4,0) node[gitcommit](commit3) {};
        \draw[commit arc] (commit3) to node[below]{parent} (commit2);
      }
        
      \uncover<6->{
        \draw(commit2) ++(4,1) node[gitcommit](commit4) {};
        \draw[commit arc] (commit4) to node[above,sloped]{parent} (commit2);
      }

      \uncover<7->{
        \draw(commit3) ++(4,0) node[gitcommit](mergecommit) {};
        \draw[commit arc] (mergecommit) to node[below,sloped]{parent} (commit3);
        \draw[commit arc] (mergecommit) to node[above,sloped]{parent} (commit4);
      
        \draw (commit3) ++(0,-1.5) node {...};
        \draw (mergecommit) ++(0,-1.5) node {...};
      }
    \end{tikzpicture}
  }
  \end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{Git objects: On-disk format}
  \footnotesize
\begin{minted}[escapeinside=||]{console}
$ git log
|commit \alert{7a7fb77be431c284f1b6d036ab9aebf646060271}|
Author: Matthieu Moy <Matthieu.Moy@imag.fr>
Date:   Wed Jul 2 20:13:49 2014 +0200

    Initial commit
$ find .git/objects/
.git/objects/
.git/objects/fc
.git/objects/fc/264b697de62952c9ff763b54b5b11930c9cfec
.git/objects/7a
|.git/objects/\alert{7a/7fb77be431c284f1b6d036ab9aebf646060271}|
.git/objects/50
.git/objects/50/a345788a8df75e0f869103a8b49cecdf95a416
.git/objects/26
.git/objects/26/27a0555f9b58632be848fee8a4602a1d61a05f
\end{minted}
\end{frame}

\begin{frame}[fragile]
\frametitle{Git objects: On-disk format}
  \footnotesize
\begin{minted}{shell-session}
$ echo foo > README.txt; git add README.txt 
$ git commit -m "add README.txt"
\end{minted}
\vspace{-2.7em}
\begin{minted}[escapeinside=||]{shell-session}
|{[}master \alert{5454e3b}{]} add README.txt|
 1 file changed, 1 insertion(+)
 create mode 100644 README.txt
$ find .git/objects/
.git/objects/
.git/objects/fc
.git/objects/fc/264b697de62952c9ff763b54b5b11930c9cfec
.git/objects/7a
.git/objects/7a/7fb77be431c284f1b6d036ab9aebf646060271
.git/objects/25
.git/objects/25/7cc5642cb1a054f08cc83f2d943e56fd3ebe99
.git/objects/54
|.git/objects/\alert{54/54e3b}51e81d8d9b7e807f1fc21e618880c1ac9|
...
\end{minted}
%$ 
\end{frame}

\begin{frame}
\frametitle{Git objects: On-disk format}
  \begin{itemize}
  \item By default, 1 object = 1 file
  \item Name of the file = object unique identifier
    content
  \item Content-addressed database:
    \begin{itemize}
    \item Identifier computed as a hash of its content
    \item Content accessible from the identifier
    \end{itemize}
  \item Consequences:
    \begin{itemize}
    \item Objects are immutable
    \item Objects with the same content have the same identity\\
      (deduplication for free)
    \item Previously, no known collision in SHA1, now moving to SHA-256
    \item Acyclic (DAG = Directed Acyclic Graph)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{On-disk format: Pack files}
  \footnotesize
  \begin{minted}[escapeinside=||]{console}
$ du -sh .git/objects/
68K     .git/objects/
$ git gc
...
$ du -sh .git/objects/
24K     .git/objects/
$ find .git/objects/
.git/objects/
.git/objects/pack
.git/objects/pack/pack-f9cbdc53005a4b500934625d...a3.idx
.git/objects/pack/pack-f9cbdc53005a4b500934625d...a3.pack
.git/objects/info
.git/objects/info/packs
$
\end{minted}
\vspace{-0.7cm}
  \begin{center}
    \large
    $\leadsto$ More efficient format, no conceptual change\\
    (objects are still there)
  \end{center}
  %$
\end{frame}

\begin{frame}[fragile]
\frametitle{Exploring the object database}
  \begin{itemize}
  \item \texttt{\bfseries{} git cat-file -p} : pretty-print the content of an object
  \end{itemize}
  \vfill
  \footnotesize
  \begin{minted}[escapeinside=||]{console}
$ git log --oneline
|{\color{red!50!black}\bfseries{}5454e3b} add README.txt|
|{\color{blue!50!black}\bfseries{}7a7fb77} Initial commit|

$ git cat-file -p 5454e3b
|tree {\color{green!50!black}\bfseries{}59802e9b115bc606b88df4e2a83958423661d8c4}|
|parent {\color{blue!50!black}\bfseries{}7a7fb77}be431c284f1b6d036ab9aebf646060271|
author Matthieu Moy <Matthieu.Moy@imag.fr> 1404388746 +0200
committer Matthieu Moy <Matthieu.Moy@imag.fr> 1404388746 +0200

add README.txt
\end{minted}
\end{frame}

\begin{frame}[fragile]
\frametitle{Exploring the object database}
  \begin{itemize}
  \item \texttt{\bfseries{} git cat-file -p} : pretty-print the content of an object
  \end{itemize}
  \vfill
  \footnotesize
  \begin{minted}[escapeinside=<>]{console}
<\$ git cat-file -p {\color{green!50!black}\bfseries{}59802e9b115bc606b88df4e2a83958423661d8c4}>
<100644 blob {\color{violet!50!black}\bfseries{}257cc5642cb1a054f08cc83f2d943e56fd3ebe99}  README.txt>
040000 tree 2627a0555f9b58632be848fee8a4602a1d61a05f  sandbox

<\$ git cat-file -p {\color{violet!50!black}\bfseries{}257cc5642cb1a054f08cc83f2d943e56fd3ebe99}>
foo

<\$ printf 'blob 4\textbackslash{}0foo\textbackslash{}n' | sha1sum>
<{\color{violet!50!black}\bfseries{}257cc5642cb1a054f08cc83f2d943e56fd3ebe99}  ->
  \end{minted}
% $
\end{frame}

\begin{frame}[fragile]
\frametitle{Merge commits in the object
    database}
  \footnotesize
  \begin{minted}{shell-session}
$ git checkout -b branch HEAD^
Switched to a new branch 'branch'
$ echo foo > file.txt; git add file.txt  
$ git commit -m "add file.txt"
[branch f44e9ab] add file.txt
 1 file changed, 1 insertion(+)
 create mode 100644 file.txt
$ git merge master
Merge made by the 'recursive' strategy.
 README.txt | 1 +
 1 file changed, 1 insertion(+)
 create mode 100644 README.txt
  \end{minted}
%$
\end{frame}

\begin{frame}[fragile]
\frametitle{Merge commits in the object
    database}
  \scriptsize
  \begin{minted}[escapeinside=<>]{shell-session}
$ git checkout -b branch HEAD^
$ echo foo > file.txt; git add file.txt  
$ git commit -m "add file.txt"
$ git merge master
$ git log --oneline --graph
<*   {\color{green!50!black}\bfseries{}1a7f9ae}> (HEAD, branch) Merge branch 'master' into branch>
<|\textbackslash>
<| * {\color{blue!50!black}\bfseries{}5454e3b} (master) add README.txt>
<* | {\color{red!50!black}\bfseries{}f44e9ab} add file.txt>
<|/>  
* 7a7fb77 Initial commit
<\$ git cat-file -p {\color{green!50!black}\bfseries{}1a7f9ae}>
tree 896dbd61ffc617b89eb2380cdcaffcd7c7b3e183
<parent {\color{red!50!black}\bfseries{}f44e9ab}ff8918f08e91c2a8fefe328dd9006e242>
<parent {\color{blue!50!black}\bfseries{}5454e3b}51e81d8d9b7e807f1fc21e618880c1ac9>
author Matthieu Moy <Matthieu.Moy@imag.fr> 1404390461 +0200
committer Matthieu Moy <Matthieu.Moy@imag.fr> 1404390461 +0200

Merge branch 'master' into branch
  \end{minted}
%$
\end{frame}

\begin{frame}
\frametitle{Snapshot-oriented storage}
  \begin{itemize}
  \item A commit represents \alert{exactly} the state of the project
  \item A tree represents \alert{only} the state of the project (where
    we are, not how we got there)
  \item Renames are not tracked, but re-detected on demand
  \item Diffs are computed on demand (e.g. \texttt{git diff HEAD HEAD\^{}})
  \item Physical storage still efficient
  \end{itemize}
\end{frame}

\section{References}

\begin{frame}[fragile]
\frametitle{Branches, tags: references}
  \begin{itemize}
  \item In Java:
    \vspace{-0.2cm}
    \begin{minted}{java}
String s; // Reference named s
s = new String("foo"); // Object pointed to by s
String s2 = s; // Two refs for the same object
\end{minted}
\item In Git: likewise!
\footnotesize
    \begin{minted}[escapeinside=||]{shell-session}
$ git log --oneline    
|{\color{blue!50!black}\bfseries{}5454e3b} add README.txt|
7a7fb77 Initial commit
$ cat .git/HEAD       
ref: refs/heads/master
$ cat .git/refs/heads/master 
|{\color{blue!50!black}\bfseries{}5454e3b}51e81d8d9b7e807f1fc21e618880c1ac9|
$ git symbolic-ref HEAD
refs/heads/master
$ git rev-parse refs/heads/master
|{\color{blue!50!black}\bfseries{}5454e3b}51e81d8d9b7e807f1fc21e618880c1ac9|
    \end{minted}
%$
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{References (refs) and objects}

  \begin{center}
    \scalebox{.7}{
    \begin{tikzpicture}
      \draw           node[gitblob](blob1) {};
      \draw ++(3,  0) node[gitblob](blob2) {};
      \draw ++(1.5,2) node[gittree](tree1) {};
      \draw ++(4.5,2) node[gitblob](blob3) {};
      \draw ++(3,4)   node[gittree](tree2) {};
      \draw[commit arc] (tree1) to node[above,sloped]{\small file1.txt} (blob1);
      \draw[commit arc] (tree1) to node[above,sloped]{\small file2.txt} (blob2);
      \draw[commit arc] (tree2) to node[above,sloped]{\small dir1.txt} (tree1);
      \draw[commit arc] (tree2) to node[above,sloped]{\small file3.txt} (blob3);

      \uncover<1->{
      \draw(tree2) ++(0,1.5) node[gitcommit](commit1) {};
      \draw[commit arc] (commit1) to node[right]{tree} (tree2);
      }

      \uncover<2->{
        \draw (commit1) ++(0,2) node[gitref] (ref1) {master};
      }
      \uncover<2>{
        \draw[commit arc] (ref1.east) -- (commit1);
      }

      \uncover<3->{
        \draw(commit1) ++(4,0) node[gitcommit](commit2) {};
        \draw[commit arc] (commit2) to node[above]{parent} (commit1);
      }

      \uncover<3>{
        \draw[commit arc,->] (ref1.east) .. controls (ref1.east -| commit2) .. (commit2);
      }

      \uncover<3->{
        \draw (commit2) ++(0,-1.5)   node[gittree](treecommit2) {};
        \draw[commit arc] (commit2) to node[right]{tree} (treecommit2);
        \draw[commit arc] (treecommit2) to node[above,sloped,near start]{\small dir1.txt}  (tree1);
        \draw (treecommit2) ++(1,-2) node[gitblob](blob3commit2) {};
        \draw[commit arc] (treecommit2) to node[above,sloped]{\small file3.txt} (blob3commit2);
      } 

      \uncover<4->{
        \draw(commit2) ++(4,0) node[gitcommit](commit3) {};
        \draw[commit arc] (commit3) to node[below]{parent} (commit2);
      }
        
      \uncover<4>{
        \draw[commit arc,->] (ref1.east) .. controls (ref1.east -| commit3) .. (commit3);
      }

      \uncover<5->{
        \draw(commit2) ++(4,1) node[gitcommit](commit4) {};
        \draw[commit arc] (commit4) to node[above,sloped]{parent} (commit2);
      }

      \uncover<5->{
        \draw(commit3) ++(4,0) node[gitcommit](mergecommit) {};
        \draw[commit arc] (mergecommit) to node[below,sloped]{parent} (commit3);
        \draw[commit arc] (mergecommit) to node[above,sloped]{parent} (commit4);
      
        \draw (commit3) ++(0,-1.5) node {...};
        \draw (mergecommit) ++(0,-1.5) node {...};
      }

      \uncover<5->{
        \draw[commit arc,->] (ref1.east) .. controls (ref1.east -| mergecommit) .. (mergecommit);
      }

      \uncover<6>{
        \draw (ref1) ++(-1,0) node[gitref] (head) {HEAD};
        \draw[commit arc] (head) -- (ref1);
      }
    \end{tikzpicture}
  }
  \end{center}
\end{frame}

\begin{frame}
\frametitle{Sounds Familiar?}
  \begin{center}
    \includegraphics[width=\textwidth]{images/gitk-branch}

    {\Huge $\approx$}
    
    \scalebox{.7}{
    \begin{tikzpicture}
      \uncover<1->{
      \draw node[gitcommit](commit1) {};
      }

      {
      }

      {
        \draw(commit1) ++(4,0) node[gitcommit](commit2) {};
        \draw[commit arc] (commit2) to node[above]{parent} (commit1);
      }

      {
        \draw(commit2) ++(4,0) node[gitcommit](commit3) {};
        \draw[commit arc] (commit3) to node[below]{parent} (commit2);
      }
        
      {
        \draw(commit2) ++(4,1) node[gitcommit](commit4) {};
        \draw[commit arc] (commit4) to node[above,sloped]{parent} (commit2);
      }

      {
        \draw(commit3) ++(4,0) node[gitcommit](mergecommit) {};
        \draw[commit arc] (mergecommit) to node[below,sloped]{parent} (commit3);
        \draw[commit arc] (mergecommit) to node[above,sloped]{parent} (commit4);
      }
      
      \draw (commit4) ++(0,1.5) node[gitref](branch) {branch};
      \draw[commit arc] (branch.east) -- (commit4);

      \draw (mergecommit) ++(0,3) node[gitref] (ref1) {master};
      \draw (ref1) ++(-1,0) node[gitref] (head) {HEAD};
      \draw[commit arc] (head) -- (ref1);
      \draw[commit arc,->] (ref1.east) .. controls (ref1.east -| mergecommit) .. (mergecommit);
    \end{tikzpicture}
  }
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Branches, HEAD, tags}
  \begin{itemize}
  \item A branch is a ref to a commit
  \item A lightweight tag is a ref (usually to a commit) (like a
    branch, but doesn't move)
  \item Annotated tags are objects containing a ref + a (signed) message
  \item HEAD is ``where we currently are''
    \begin{itemize}
    \item If HEAD points to a branch, the next commit will move the branch
    \item If HEAD points directly to a commit (detached HEAD), the
      next commit creates a commit not in any branch (warning!)
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Branches and tags in practice}

\begin{frame}
  \frametitle{Branches and Tags in Practice}
  \begin{itemize}
  \item Create a local branch and check it out:\\
    \mintinline{sh}{git checkout -b <branch-name>}
  \item List local branches:\\
    \mintinline{sh}{git branch}
  \item List all branches (including remote-tracking):\\
    \mintinline{sh}{git branch -a}
  \item Create a tag:\\
    \mintinline{sh}{git tag <tag-name>}
  \item Switch to a branch, a tag, or a commit:\\
    \mintinline{sh}{git checkout [branch-name |tag-name | commit]}
  \end{itemize}
\end{frame}


%\end{comment}

% \section{More Documentation}

% \begin{frame}
% \frametitle{More Documentation}
%   \begin{itemize}
%   \item \url{http://ensiwiki.ensimag.fr/index.php/Maintenir_un_historique_propre_avec_Git}
%   \item \url{http://ensiwiki.ensimag.fr/index.php/Ecrire_de_bons_messages_de_commit_avec_Git}
%   \end{itemize}
% \end{frame}

% \section{Exercises}

% \begin{frame}[fragile]
%   \frametitle{Exercises}

%   \begin{itemize}
%   \item Visit \url{https://github.com/moy/dumb-project.git}
%   \item Fork it from the web interface (or just \texttt{git clone})
%   \item Clone it on your machine
%   \item Repair the dirty history!
%   \end{itemize}
% \end{frame}

% \begin{frame}[fragile]
%   \frametitle{If you liked this training ...}
%   \begin{center}
%     \Large ``Principes et utilisations de l'outil Git'', 1 day
%     training. Next iteration: April 25th 2016. Tell your friends ;-).
    
%     {\tiny \url{http://formation-continue.grenoble-inp.fr/genie-logiciel-programmation/principes-et-utilisations-de-l-outil-git-681441.kjsp#page-presentation}}
%   \end{center}
% \end{frame}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "american"
%%% End:
