%\documentclass{beamer}

\usepackage{moy-bouveret-slides}
\usepackage{tikz}
\usetikzlibrary{shapes}
\usepackage{moy-bouveret-tikz}
\usepackage{comment}
\usepackage{minted}

\title[Git Workflow]{Git Workflows}

%\subtitle{...}
\author{Sylvain Bouveret, Grégory Mounié, Matthieu Moy}

\institute[first.last@imag.fr]{[first].[last]@imag.fr\\
\url{https://git.pages.ensimag.fr/formation-git/slides/\jobname.pdf}}

\date{2021}


\begin{document}
%\begin{comment}
\begin{frame}[plain]
  \titlepage

  \begin{center}
    \includegraphics[width=2cm]{images/CC-BY-SA}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Goals of the presentation}

  \begin{itemize}
  \item Global history: multiple workflow
  \item Global history: branching, rebasing, stashing 
  \end{itemize}
\end{frame}

\section{Workflows}

\subsection{Branching models}

\begin{frame}
  \frametitle{Branches}

  \begin{itemize}
  \item Branching exists in most VCS
  \item \textbf{Goal:} keep track separately of various parallel works
    on an on-going software development.
  \end{itemize}

  \pause

  \begin{center}
    But in git: \alert{Branching is a killing feature}
  \end{center}

  \begin{itemize}
  \item Lightweight
  \item Efficient
  \item $\to$ easy to split the work among developers and still let
    them work easily together toward the common goal.
  \end{itemize}

  \pause
  
  But it is not a magic bullet: work splitting should be handled with care!
\end{frame}

\begin{frame}
  \frametitle{Example of bad splitting: per developer branches}

  \textbf{Pro}
  \begin{itemize}
  \item simple to define and set-up \cmark
  \end{itemize}

  \textbf{Cons}
  \begin{itemize}
  \item missing information: to find something, you need to know who
    did it \xmark
  \item redundant information: committer and branch name are strongly related \xmark
  \item confusing and error prone code sharing, blending new feature
    development and bug correction \xmark
  \item developers use old version of common code \xmark
  \item As it does not enforce convergence of code $\Rightarrow$ the
    multiple incompatible development are increasingly difficult to
    merge \xmark
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Better splitting: per topic branches}

  \begin{columns}
    \begin{column}{.48\textwidth}
      \includegraphics[width=\textwidth]{images/vincent_driessen_model}
    \end{column}
    \begin{column}{.48\textwidth}
      \begin{itemize}
      \item {\em \textcolor{blue}{master}} branch contain release versions
      \item {\em \textcolor{orange}{develop}} branch contain ongoing development
      \item {\em \textcolor{green!50!fg}{release}} branch to prepare new release in master
      \item {\em \textcolor{red!70!fg}{hotfix}} branch for bug correction of release version
      \item {\em \textcolor{purple}{feature}} branch to develop new features
      \end{itemize}
      {\scriptsize [Vincent Driessen, \url{http://nvie.com/posts/a-successful-git-branching-model/}]}
    \end{column}
  \end{columns}
\end{frame}

\subsection{Merging models}

\begin{frame}
  \frametitle{Code sharing models}

  Distributed VCS such as Git are ... distributed.

  The code is thus modified at several places at the same time. There are
  many different ways to share the code modifications between
  repositories.
\end{frame}


\begin{frame}
  \frametitle{Code sharing: centralized workflow}
  \begin{columns}
    \begin{column}{.60\textwidth}\footnotesize
      \begin{tikzpicture}
        [repo/.style={rectangle,very thick,tips,
          rounded corners=1mm,text width=1.7cm,text centered,draw}]
        \node[repo,fill=blue!20!white] (central) at (0, 3) { central repository };
        \node[repo] (dev1) at (-2.5, 0) { developer repository };
        \node[repo] (dev2) at (0, 0)  { developer repository };
        \node[repo] (dev3) at (2.5, 0)  { developer repository };
        \draw[<->,very thick] (dev1) -- (central);
        \draw[<->,very thick] (dev2) -- (central);
        \draw[<->,very thick] (dev3) -- (central);
        \uncover<2->{\draw[<->,very thick] (dev1.south) to [out=270, in=270] (dev2.south);}
      \end{tikzpicture}
    \end{column}
    \begin{column}{.39\textwidth}
      Similar to SVN way of life, just fully operational with
      branches, merge and off-line work. Quite good for small teams.
      
      \uncover<2->{\vspace{1cm} Additional transfers using git are \alert{easy and safe !}}
    \end{column}
  \end{columns}

  \begin{block}<2->{The tricky part}
    The most important point is {\em to use git} for transfer
    patches. Any transfer outside git can not be taken into account in
    futur merge.
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Centralized workflow}
  
\begin{minted}{c}
do {
   while (nothing_interesting())
      work();
   while (uncommited_changes()) {
      while (!happy) { // git diff --staged ?
         while (!enough) git add -p;
         while (too_much) git reset -p;
      }
      git commit; // no -a
      if (nothing_interesting())
         git stash;
   }
   while (!happy)
      git rebase -i;
} while (!done);
git push; // send code to central repository
\end{minted}
\end{frame}


\begin{frame}
  \frametitle{Code sharing: Github workflow}
  \vspace{-3mm}
  \begin{columns}[c]
    \begin{column}{.45\textwidth}\footnotesize
      \begin{tikzpicture}
        [repo/.style={rectangle,very thick,tips,
          rounded corners=1mm,text width=2cm,text centered,draw}]
        \node[repo,fill=blue!20!white] (central) at (0, 3) { blessed repository };
        \node[repo,fill=red!20!white] (centralpriv) at (0, 0) { integration repository };

        \node[repo] (dev1priv) at (3, 0) { developer private repository };
        \node[repo] (dev1pub) at (3, 3)  { developer public repository };
        \uncover<1,5->{
          \draw[->,very thick] (central) to node[below,sloped,pos=0.35]{pull} (dev1priv);
        }
        \uncover<2,5->{
          \draw[->,very thick] (dev1priv) to node[above,sloped]{push} (dev1pub);
        }
        \uncover<3,5->{
          \draw[->,very thick] (dev1pub) to node[above,sloped,pos=0.25]{pull} (centralpriv);
        }
        \uncover<4,5->{
          \draw[->,very thick] (centralpriv) to node[above,sloped]{push} (central);
        }
      \end{tikzpicture}
    \end{column}
    \begin{column}{.50\textwidth}
      \begin{itemize}
      \item Released and official branches are stored in the blessed
      repository.
      \item Contributors forks and works privately
      \item Contributors publish their work and ask for merge
      \item Integrators merges then publish the contributions
      \end{itemize}
    \end{column}
  \end{columns}
  \begin{block}<5->{Public mailing list as public repository}

    Git patches can be send by email
    ({\tt git format-patch -1}), published in mailing-list
    (eg. bug-gnu-emacs@gnu.org), then integrated ({\tt git am})
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Triangular Workflow with pull-requests}

  \begin{itemize}
  \item Developers pull from upstream, and push to a ``to be merged'' location
  \item Someone else reviews the code and merges it upstream
  \end{itemize}
  \begin{center}
    \begin{tikzpicture}
      \draw (0,0)  node[gitrepo](upstream) {Upstream};
      \draw (4,0)  node[gitrepo](publish) {A's public repo};
      \draw (4,-3) node[gitrepo](local) {A's private repo};
      \draw[big arrow] (upstream) to node[below, sloped]{clone, pull} (local.north west);
      \draw[big arrow] (local) to node[above, sloped]{push} (publish);
      \draw[big arrow] (publish) to node[above, sloped]{merge} (upstream);

      \draw (-4,0)  node[gitrepo](publish) {B's public repo};
      \draw (-4,-3) node[gitrepo](local) {B's private repo};
      \draw[big arrow] (upstream) to node[below, sloped]{clone, pull} (local.north east);
      \draw[big arrow] (local) to node[above, sloped]{push} (publish);
      \draw[big arrow] (publish) to node[above, sloped]{merge} (upstream);
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Pull-requests in Practice}
  \begin{description}
  \item[Contributor] create a branch, commit, push
  \item[Contributor] click ``Create pull request'' (GitHub, GitLab,
    BitBucket, ...), or \texttt{git request-pull}
  \item[Maintainer] receives an email
  \item[Maintainer] review, comment, ask changes
  \item[Maintainer] merge the pull-request
  \end{description}

  The code review is the major point for code quality insurance !
\end{frame}

\subsection{Code review in Triangular Workflows}


\begin{frame}
  \frametitle{Code Review}
  
  \begin{itemize}
  \item What we'd like:
    \begin{enumerate}
    \item A writes code, commits, pushes
    \item B does a review
    \item B merges to upstream
    \end{enumerate}
  \item What usually happens:
    \begin{enumerate}
    \item A writes code, commits, pushes
    \item B does a review
    \item B requests some changes
    \item ... then ?
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Iterating Code Reviews}

  \begin{itemize}
  \item At least 2 ways to deal with changes between reviews:
    \begin{enumerate}
    \item Add more commits to the pull request and push them on top
    \item Rewrite commits (\texttt{rebase -i}, \dots) and overwrite
      the old pull request
      \begin{itemize}
      \item The resulting history is clean
      \item Much easier for reviewers joining the review effort at
        iteration 2
      \item e.g. On Git's mailing-list, 10 iterations is not uncommon.
      \end{itemize}
    \end{enumerate}
  \end{itemize}
\end{frame}

% \subsection{Continuous Integration}

% \begin{frame}[fragile]
%   \frametitle{Continuous Integration: example with GitHub and
%     Travis-CI}
%   \framesubtitle{https://github.com/moy/travis-demo}
%   \begin{itemize}
%   \item Configuration:
% \begin{verbatim}
% language: python
% python:
%   - "2.7"
%   - "3.4"
% install:
%   - pip install pep8
% script:
%   - pep8 main.py
%   - ./test.py
% \end{verbatim}
%   \item Use: work as usual ;-).
%   \end{itemize}
% \end{frame}


\begin{frame}
  \frametitle{Code sharing: Linux kernel workflow}
  \vspace{3mm}
  \begin{columns}[c]
    \begin{column}{.45\textwidth}\footnotesize
      \begin{tikzpicture}
        [repo/.style={rectangle,very thick,tips,
          rounded corners=1mm,text width=2cm,text centered,draw}]
        \node[repo,fill=red!20!white] (dictator) at (0, 4) {dictator repository};
        \node[repo,fill=blue!20!white] (public) at (3, 4) {public repository};

        \node[repo,fill=blue!20!white] (lieutenant) at (0, 2) {lieutenant public\\repository};

        \node[repo] (d1) at (0, 0) {developer public repository};
        \node[repo] (d2) at (3, 0)  {developer public repository};
        \draw[->,very thick] (dictator) -- (public);
        \draw[->,very thick] (lieutenant) -- (dictator);
        \draw[<->,very thick] (d1) -- (lieutenant);
        \draw[<->,very thick] (d2) -- (lieutenant);
        \draw[->,very thick] (public) -- (d2);
        \draw[->,very thick] (public.south) to [out=270,in=30](d1);
      \end{tikzpicture}
    \end{column}
    \begin{column}{.50\textwidth}
      \begin{itemize}
      \item Code review and basic filtering of contributions is done
        by the lieutenants
      \item Final decision is done by the benevolent dictator
      \item Lieutenant repositories are the testbeds of new ideas that
        mature in it before upstream submission
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Code sharing: remote management}

  Remote allows to work with several sources and sink
\begin{minted}{console}
$ git remote add lieutenant git://.../public.git
$ git remote
origin
lieutenant
$ git fetch lieutenant
...
$ git branch -r
 origin/HEAD -> origin/master
 origin/master
 lieutenant/master
$ git checkout -b lieutenant lieutenant/master
\end{minted}

\end{frame}

\section{Branches and tags in practice}

\begin{frame}
  \frametitle{Branches and Tags in Practice}
  \begin{itemize}
  \item Create a local branch and check it out:\\
    \texttt{git checkout -b \textit{branch-name}}
  \item Switch to a branch:\\
    \texttt{git checkout \textit{branch-name}}
  \item List local branches:\\
    \texttt{git branch}
  \item List all branches (including remote-tracking):\\
    \texttt{git branch -a}
  \item Create a tag:\\
    \texttt{git tag \textit{tag-name}}
  \end{itemize}
\end{frame}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "american"
%%% End:
