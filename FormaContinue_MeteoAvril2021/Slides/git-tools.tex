\usepackage{moy-bouveret-slides}
\usepackage{tikz}
\usetikzlibrary{shapes}
%\usepackage{moy-tikz}
\usepackage{comment}

\title{Git tools}

%\subtitle{...}

\author{Sylvain Bouveret, Grégory Mounié, Matthieu Moy}

\institute[first.last@imag.fr]{[first].[last]@imag.fr\\
\url{https://git.pages.ensimag.fr/formation-git/slides/\jobname.pdf}}


\date{2021}

\begin{document}

\begin{frame}[plain]
  \titlepage

  \begin{center}
    \includegraphics[width=2cm]{images/CC-BY-SA}
  \end{center}
\end{frame}

\section{Git integrated graphical interface}

\begin{frame}
  \frametitle{Git interfaces included}

  Several basic tools are included with git. For portability reason
  (Tcl/Tk) they are ugly, yet convenient. They are good starting point
  to learn what are the basic operations.

  \begin{description}
  \item[Git gui] commit edition
  \item[Gitk] History navigation
  \item[Gitweb] web server on the repository
  \end{description}
\end{frame}

\subsection{git gui}

\begin{frame}
  \frametitle{Git gui: commit edition}

  \begin{columns}
    \begin{column}{.4\textwidth}
      \includegraphics[width=\textwidth]{images/git-gui}
    \end{column}
    \begin{column}{.6\textwidth}
      A Tcl/Tk graphical user interface dedicated to making commit with:
      \begin{description}
      \item[Top left] list of modified and untracked files not in the index
      \item[bottom left] list of files in the index
      \item[Up right] Patches associated with the selected file in one of the two previous area
      \item[bottom] Message editor and index/sign/commit buttons
      \end{description}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Git gui: fine grain patch selection}

  \begin{columns}
    \begin{column}{.7\textwidth}
      \includegraphics[width=\textwidth]{images/git-gui-patch}
    \end{column}
    \begin{column}{.3\textwidth}
      Each individual patch may be added or removed from the index in
      one right-click
    \end{column}
  \end{columns}

\end{frame}


\begin{frame}
  \frametitle{Git gui: commit message}

  \begin{columns}
    \begin{column}{.6\textwidth}
      \includegraphics[width=\textwidth]{images/git-gui-commit}
    \end{column}
    \begin{column}{.4\textwidth}
      Create the commit message with a very simple editor (yet still
      with dictionary check).

      Amend previous commit in one button.

      Commit may be signed with the private key of the developer.

      Push in one click
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}
  \frametitle{git gui blame}

  It also helps to visit quickly who write the code, when and why
  (commit message)

  \includegraphics[width=\textwidth]{images/git-gui-blame}
\end{frame}

\subsection{gitk}

\begin{frame}[fragile]
  \frametitle{Gitk: visualize changes in repository}

  \begin{columns}
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{images/gitk}
    \end{column}
    \begin{column}{.5\textwidth}
      A Tcl/Tk graphical user interface dedicated to visualize changes
      in the repository:
      \begin{description}
      \item[Top] commit graph with branches; author; date
      \item[bottom left] message of the selected commit
      \item[bottom right] File list related to the selected commit
      \end{description}
    \end{column}
  \end{columns}
  \begin{alertblock}{Focus history}
    By default \lstinline{gitk} shows the current branch history.
    \lstinline{gitk --all} shows all branches.
    \lstinline{gitk --since="2 weeks ago"}, the last two week changes.
  \end{alertblock}
\end{frame}

\subsection{gitweb}

\begin{frame}
  \frametitle{Gitweb: visualize changes on the web}

  \begin{columns}
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{images/gitweb}
    \end{column}
    \begin{column}{.5\textwidth}
      A web graphical user interface dedicated to visualize the
      repository content:
      \begin{description}
      \item[Top] Description
      \item[Content] shortlog; tags; heads; remotes
      \end{description}
      \begin{alert}{To run it with lighttpd}
        \lstinline{git instaweb}
      \end{alert}
    \end{column}
  \end{columns}
\end{frame}


\section{Others interfaces}


\begin{frame}
  \frametitle{Many specialized interfaces}
  \begin{columns}
    \begin{column}[t]{.3\textwidth}
      \includegraphics[width=\textwidth]{images/gource}
    \end{column}
    \begin{column}[t]{.7\textwidth}
      \lstinline{Gource} animates the contribution among the time, around two points: files and contributors.
  
      Many other interfaces, different needs:
      \begin{itemize}
      \item gitg (history exploration),
      \item git-cola (git gui on steroid),
      \item gitstats,
      \item magit (emacs mode),
      \item egit (git integration in eclipse),
      \item ...
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\section{Git integration}

% prendre les photos du tutorial de eclipse.org ?
\begin{frame}
  \frametitle{Eclipse and git: Egit}
  \includegraphics[height=0.4\textheight]{images/eclipse-team-menu}      
  \includegraphics[height=0.4\textheight]{images/egit-tuto-diff}
  
  \lstinline{Egit} is the git plugin of Eclipse. Most
  functionalities are in right button "Team" menu (Add to Index,
  Commit, History...).

  (Pictures from \url{http://wiki.eclipse.org/EGit/User_Guide})
\end{frame}


\begin{frame}{Git and VSCode}
  \begin{itemize}
  \item VSCode embarks a Source Control Manager, that can work with git repos.
    See {\scriptsize\url{https://code.visualstudio.com/docs/editor/versioncontrol}}.
    \begin{itemize}
    \item Automatic file modifications highlighting (git diff)
    \item Commit / Push / Pull support
    \item Support for branches
    \item ...
    \end{itemize}
  \item To have a visual representation of the git tree, you need an extension. For instance:
    Git Graph ({\scriptsize\url{https://github.com/mhutchie/vscode-git-graph}})
  \end{itemize}
\end{frame}


\begin{frame}{Git and Pycharm}
  \begin{itemize}
  \item Native Source Control Manager in Pycharm
    \begin{itemize}
    \item Automatic file modifications highlighting (git diff)
    \item Commit / Push / Pull support
    \item Support for branches
    \item ...
    \end{itemize}
  \end{itemize}

  \begin{center}
    \includegraphics[width=0.6\textwidth]{images/pycharm.png}
  \end{center}
\end{frame}

\section{Back to git forges}

\begin{frame}{Git forges}
  \begin{itemize}
  \item As we have seen earlier, git forges (\textit{e.g.} Github and
    Gitlab) are more than just git hosting facilities
  \item We will present three useful features:
    \begin{itemize}
    \item Pull / merge requests
    \item Continuous Integration / Deployment
    \item Pages
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Pull request (Github); Merge request (Gitlab)}

\begin{frame}
  \frametitle{Merge request}

  \begin{columns}
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{images/merge_request_clip}
    \end{column}
    \begin{column}{.5\textwidth}
      Github and Gitlab propose high level interface to organize and
      track issues and patch proposals (pull/merge request)
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{How to submit a merge request}
  \includegraphics[height=0.4\textheight]{images/propose_merge_request_clip}

  The user creates a \textbf{fork} (private copy) of the project. He
  develops inside the copy, creates commits, and then, he proposes the new
  commits in a \textbf{merge request}.
\end{frame}

\begin{frame}
  \frametitle{How to check or revise a merge request}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{images/reception_merge_request}
      \includegraphics[width=\textwidth]{images/webide_merge_request}     
    \end{column}
    \begin{column}{.5\textwidth}
      The developer of the original project get a notification of the
      merge request. He may accept and merge it (1 click), comment
      the proposed commit, or reject it and close it.

      The webide interface allows fast and easy review of simple
      small code.
    \end{column}
  \end{columns}
\end{frame}

\subsection{Continuous Integration / Deployment}

\begin{frame}{About CI/CD}
  \begin{itemize}
  \item Gitlab CI/CD aims at automatizing builds / tests / deployments of applications
  \item Based on \alert{Gitlab Runners} (executors)
    \begin{itemize}
    \item You can install your own runners (see the Gitlab Runners
      project \url{https://docs.gitlab.com/runner/})
    \item There also exist (limited) public runners
    \item We will not discuss the installation of runners, but assume
      some are already properly configured
    \item (NB: you will have to activate them in the project configuration
      on Gitlab)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Defining jobs}
  \begin{itemize}
  \item Gitlab CI/CD jobs are defined using a manifest file called
    \mintinline{sh}{.gilab-ci.yml} to be placed at the root of the
    repo.
  \item In this file, you specify:
    \begin{itemize}
    \item Jobs to execute (scripts)
    \item Stages that define the pipeline (build, test, deploy...)
    \item The docker image on which the jobs will be executed
    \item Job artifacts
    \item ...
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{A small example}
  Let us configure a Gitlab CI/CD pipeline to run tests (and compute the code coverage)

  The \mintinline{sh}{.gitlab-ci.yml} file:
  
  \begin{minted}{yaml}
run tests:
  stage: test
  image: python:3
  only:
    - master
  before_script:
    - pip install pytest pytest-cov
    - pip install numpy
  script:
    - pytest --cov=src/ sorting_test.py -vvv

stages:
  - test
  \end{minted}
\end{frame}

\begin{frame}{A small example (cont'd)}
  After a push to master, the pipeline is automatically triggered and we can
  see the result:

  \begin{center}
    \includegraphics[width=0.6\textwidth]{images/gitlab-ci1.png}
  \end{center}
\end{frame}

\begin{frame}{A small example (cont'd)}
  We can even see the detailed output of the different jobs:
  
  \begin{center}
    \includegraphics[width=\textwidth]{images/gitlab-ci2.png}
  \end{center}
\end{frame}

\begin{frame}[fragile]{A small example (cont'd)}
  And, cherry on the cake, we can also display a small badge showing
  the result of the code coverage.

  For that, we need to tell Gitlab to parse the output of the script. Go to:

  Settings $\triangleright$ CI / CD $\triangleright$ General pipelines $\triangleright$
  Test coverage parsing

  And specify a regular expression corresponding to what Gitlab is supposed to read. For Python Coverage:

  \mintinline{sh}{^TOTAL.+?(\d+\%)$}

  % $
  
  \begin{center}
    \includegraphics[width=\textwidth]{images/gitlab-ci3.png}
  \end{center}
\end{frame}

\subsection{Gitlab Pages}

\begin{frame}{About Gitlab Pages}
  \begin{itemize}
  \item A wonderful feature of Gitlab is that it can serve
    automatically generated static web pages
  \item Pages are generated by a Gitlab CI/CD job
  \item Useful for documentation, information about the build process,
    hosting material for a lecture...
  \end{itemize}

  \pause

  Configuring page generation is easy:

  \begin{enumerate}
  \item Define a job called ``pages''
  \item Define the associated generation script that eventually copies all
    the needed files to a directory called ``public''
  \item Define that ``public'' is an artifact of the job
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{Configuration of page generation}
  For instance, if we want to use Gitlab to show the complete
  HTML report of our code coverage:
  
  \begin{minted}{yaml}
pages:
  stage: deploy
  image: python:3
  only:
    - master
  before_script:
    - pip install pytest pytest-cov
    - pip install numpy
  script:
    - pytest --cov=src/ sorting_test.py -vvv
    - coverage html
    - mv htmlcov public
  artifacts:
    paths:
      - public
  \end{minted}
\end{frame}


\section*{Conclusion}

\begin{frame}{Conclusion}
  \begin{itemize}
  \item The CLI is just one possible interface to git
  \item Many other tools can simplify your life as a developer:
    \begin{itemize}
    \item Git integrated interfaces (git gui, gitk...)
    \item Other specialized interfaces (gitcola, gitg, gource...)
    \item Git plugins to IDE (Eclipse, Netbeans, VScode, PyCharm...)
    \item + Several killer features in Gitlab and Github
    \end{itemize}
  \end{itemize}

  \pause

  Use the ones you are more comfortable with! 
\end{frame}


\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "american"
%%% End:
