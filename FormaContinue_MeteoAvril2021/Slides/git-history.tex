%\documentclass{beamer}

\usepackage{moy-bouveret-slides}
\usepackage{tikz}
\usetikzlibrary{shapes}
\usepackage{moy-bouveret-tikz}
\usepackage{comment}

\title{Git: How to Clean (and Rewrite) History}

%\subtitle{...}

\author{Sylvain Bouveret, Grégory Mounié, Matthieu Moy}

\institute[first.last@imag.fr]{[first].[last]@imag.fr\\
\url{https://git.pages.ensimag.fr/formation-git/slides/\jobname.pdf}}

\date{2021}

\begin{document}
%\begin{comment}
\begin{frame}[plain]
  \titlepage

  \begin{center}
    \includegraphics[width=2cm]{images/CC-BY-SA}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Goals of the presentation}

  \begin{itemize}
  \item Explaining why having a clean history is important
  \item Showing how to use the index
  \item Introducing the mechanisms that git provides to deal with
    history
  \end{itemize}
\end{frame}

\section{Clean History: Why?}

\begin{frame}
  \begin{center}
    \includegraphics[width=.8\textwidth]{fig/git_commit}\\
    {\scriptsize Source: \url{https://xkcd.com/1296/}}
  \end{center}
\end{frame}

\begin{frame}
\frametitle{Git blame: Who did that?}
  \begin{center}
    \mintinline{sh}{git gui blame <file>}

    ~\\

    \includegraphics[width=.95\textwidth]{images/git-gui-blame}
  \end{center}
\end{frame}

\begin{frame}[fragile]
\frametitle{Bisect: Find regressions}

\vspace{-0.4cm}
\begin{minted}[escapeinside=||]{console}
$ git bisect start
$ git bisect bad
$ git bisect good v1.9.0
Bisecting: 607 revisions left to test after this (roughly 9 steps)
[8fe3ee67adcd2ee9372c7044fa311ce55eb285b4] Merge branch 'jx/i18n'
$ git bisect good
Bisecting: 299 revisions left to test after this (roughly 8 steps)
[aa4bffa23599e0c2e611be7012ecb5f596ef88b5] Merge branch 'jc/cod[...]
$ git bisect good
Bisecting: 150 revisions left to test after this (roughly 7 steps)
[96b29bde9194f96cb711a00876700ea8dd9c0727] Merge branch 'sh/ena[...]
$ git bisect bad
Bisecting: 72 revisions left to test after this (roughly 6 steps)
[09e13ad5b0f0689418a723289dca7b3c72d538c4] Merge branch 'as/pre[...]
...
$ git bisect good
|\alert{\ttbold{}60ed26438c909fd273528e67 is the first bad commit}|
commit 60ed26438c909fd273528e67b399ee6ca4028e1e
\end{minted}
%$
\end{frame}

\begin{frame}<handout:1,5>
\frametitle{Bisect: Binary search}
  \begin{center}
    \texttt{git bisect visualize}

    \only<all:1>{\includegraphics[width=.8\textwidth]{images/git-bisect-1}}
    \only<all:2>{\includegraphics[width=.8\textwidth]{images/git-bisect-2}}
    \only<all:3>{\includegraphics[width=.8\textwidth]{images/git-bisect-3}}
    \only<all:4>{\includegraphics[width=.8\textwidth]{images/git-bisect-4}}
    \only<all:5>{\includegraphics[width=.8\textwidth]{images/git-bisect-5}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Then what?}

  \mintinline{sh}{git blame} and \mintinline{sh}{git bisect} point you to a
  commit, then ...
  \begin{itemize}
  \item Dream:
    \begin{itemize}
    \item Commit is a 50-lines long patch
    \item Commit message explains the intent of the programmer
    \end{itemize}
  \item Nightmare 1:
    \begin{itemize}
    \item Commit mixes a large reindentation, a bugfix and a real feature
    \item Message says ``I reindented, fixed a bug and added a feature''
    \end{itemize}
  \item Nightmare 2:
    \begin{itemize}
    \item Commit is a trivial fix for the previous commit
    \item Message says ``Oops, previous commit was stupid''
    \end{itemize}
  \item Nightmare 3:
    \begin{itemize}
    \item Bisect not even applicable because most commits aren't
      compilable.
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Then what?}
  \begin{center}
    \Large
    \only<1>{Which one do you prefer?}

    \uncover<2->{Clean history is\only<3>{\alert{ as}} important\only<3>{\alert{ as comments}}
      for software maintainability}
    \vfill
  \end{center}
\end{frame}

\begin{frame}
\frametitle{Two Approaches To Deal With History}
  \begin{center}
    \huge
    {\large Approach 1}\\
    ``Mistakes are part of history.''

    ~\\

    {\large Approach 2}\\
    ``History is a set of lies agreed upon.''\footnote{Napoleon Bonaparte}
  \end{center}
\end{frame}

\begin{frame}
\frametitle{Approach 1: Mistakes are part of history}
  \begin{itemize}
  \item $\approx$ the only option with Subversion/CVS/...
  \item History reflects the chronological order of events
  \item Pros:
    \begin{itemize}
    \item Easy: just work and commit from time to time
    \item Traceability
    \end{itemize}
  \item But ...
    \begin{itemize}
    \item Is the actual order of event what you want to remember?
    \item When you write a draft of a document, and then a final
      version, does the final version reflect the mistakes you did in
      the draft?
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Approach 2: History is a set of lies agreed upon}
  \begin{itemize}
  \item Popular approach with modern VCS (Git, Mercurial\dots{})
  \item History tries to show the best logical path from one point
    to another
  \item Pros:
    \begin{itemize}
    \item See above: blame, bisect, ...
    \item Code review
    \item Claim that you are a better programmer than you really are!
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Another View About Version Control}

  \begin{itemize}
  \item 2 roles of version control:
    \begin{itemize}
    \item For beginners: \alert{help} the code reach upstream.
    \item For advanced users: \alert{prevent} bad code from reaching upstream.
    \end{itemize}
  \item Several opportunities to reject bad code:
    \begin{itemize}
    \item Before/during commit
    \item Before push
    \item Before merge
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{What is a clean history}
  \begin{itemize}
  \item Each commit introduce \alert{small} group of \alert{related}
    changes ($\approx$ 100 lines changed max, no minimum!)
  \item Each commit is compilable and passes all tests (``bisectable
    history'')
  \item ``Good'' commit messages
  \end{itemize}
\end{frame}

\section{Writing good commit messages}

\begin{frame}[fragile]
\frametitle{Reminder: good comments}
  \begin{itemize}
  \item Bad: \only<2->{\alert{What? The code already tells}}
\begin{minted}{java}
int i; // Declare i of type int
for (i = 0; i < 10; i++) { ... }
f(i)
\end{minted}
  \item Possibly good: \only<2->{\alert{Why? Usually the relevant question}}
\begin{minted}{java}
int i; // We need to declare i outside the for
       // loop because we'll use it after.
for (i = 0; i < 10; i++) { ... }
f(i)
\end{minted}
\end{itemize}
\begin{overlayarea}{\textwidth}{0.3\textheight}
  \only<3->{
    \begin{center}
      Common rule: if your code isn't clear enough,\\
      rewrite it to make it clearer\\
      instead of adding comments.
    \end{center}
  }
\end{overlayarea}
\end{frame}

\begin{frame}[fragile]
\frametitle{Good commit messages}

  \begin{itemize}
  \item Recommended format:
\begin{minted}{text}
One-line description (< 50 characters)

Explain here why your change is good.
\end{minted}
  \item Write your commit messages like an email: subject and body
  \item Imagine your commit message is an email sent to the
    maintainer, trying to convince him to merge your code\footnote{Not
      just imagination, see \mintinline{sh}{git send-email}}
  \item Don't use \mintinline{sh}{git commit -m}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Good commit messages: examples}
  \framesubtitle{From Git's source code}
  \scriptsize

  {\tiny\url{https://github.com/git/git/commit/b1b49ff8d42a21ade6a72b40a147fd3eaff3db8d}}
  \begin{minted}[escapeinside=||]{text}
|\textbf{daemon: plug memory leak}|

Call child_process_clear() when a child ends to release the memory
allocated for its environment.  This is necessary because unlike all
other users of start_command() we don't call finish_command(), which
would have taken care of that for us.

This leak was introduced by f063d38 (daemon: use cld->env_array
when re-spawning).

Signed-off-by: Rene Scharfe <l.s.r@web.de>
Signed-off-by: Junio C Hamano <gitster@pobox.com>
  \end{minted}
\end{frame}

\begin{frame}[fragile]
\frametitle{Good commit messages: counter-example}
  \framesubtitle{GNU-style changelogs}
  \scriptsize

  {\tiny\url{http://git.savannah.gnu.org/cgit/emacs.git/commit/?id=19e09cfab61436cb4590303871a31ee07624f5ab}}
  \begin{minted}[escapeinside=||]{text}
|\textbf{Ensure redisplay after evaluation}|

* lisp/progmodes/elisp-mode.el (elisp--eval-last-sexp): Revert
last change.
* lisp/frame.el (redisplay--variables): Populate the
redisplay--variables list.
* src/xdisp.c (maybe_set_redisplay): New function.
(syms_of_xdisp) <redisplay--variables>: New variable.
* src/window.h (maybe_set_redisplay): Declare prototype.
* src/data.c (set_internal): Call maybe_set_redisplay. (Bug#21835)
\end{minted}
\small Not much the patch didn't already say ... (do you understand the
problem the commit is trying to solve?)\footnote{NB: This style is historical. Emacs development moved from one VCS to another. The first commit date readeable in the git history (at that time in RCS) is 18 April 1985}
\end{frame}

\section{Partial commits, the index}

\begin{frame}{Git Data transport commands}
  \begin{center}
    \scalebox{0.6}{
      \input{tikz/git-data-transport}
    }\\
    {\scriptsize (inspired by \url{http://osteele.com})}
  \end{center}
\end{frame}

\begin{frame}
\frametitle{The index, or ``Staging Area''}
  \begin{itemize}
  \item ``the index'' is where the next commit is prepared
  \item Contains the list of files \alert{and their content}
  \item \mintinline{sh}{git commit} transforms the index into a commit
  \item \mintinline{sh}{git commit -a} stages all changes in the worktree in
    the index before committing. You'll find it sloppy soon.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Dealing with the index}
  \begin{itemize}
  \item Commit only 2 files:
    \begin{minted}{console}
$ git add file1.txt
$ git add file2.txt
$ git commit
    \end{minted}
  \item Commit only some patch hunks:
    \begin{minted}{console}
$ git add -p
(answer yes or no for each hunk)
$ git commit
    \end{minted}
%$
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{\texttt{git add -p}: example}
  \footnotesize
\begin{semiverbatim}
$ {\ttbold{}git add -p}
@@ -1,7 +1,7 @@
 int main() {
{\color{red!70!black}-       int i;}
{\color{green!70!black}+       int i = 0;}
        printf("Hello, ");
        i++;
{\color{blue!70!black}Stage this hunk [y,n,q,a,d,/,K,g,e,?]?} {\ttbold{}y}\pause
@@ -5,6 +5,6 @@

{\color{red!70!black}-       printf("i is \%s\textbackslash{}n", i);}
{\color{green!70!black}+       printf("i is \%d\textbackslash{}n", i);}
 }
{\color{blue!70!black}Stage this hunk [y,n,q,a,d,/,K,g,e,?]?} {\ttbold{}n}\pause
$ git commit -m "Initialize i properly"
[master c4ba68b] Initialize i properly
 1 file changed, 1 insertion(+), 1 deletion(-)
  \end{semiverbatim}
\end{frame}

\begin{frame}
  \frametitle{\texttt{git add -p}: dangers}
  \begin{itemize}
  \item Commits created with \mintinline{sh}{git add -p} do not correspond to
    what you have on disk
  \item You probably never tested this commit ...
  \item Solutions:
    \begin{itemize}
    \item \mintinline{sh}{git stash -k}: stash what's not in the index
    \item \mintinline{sh}{git rebase --exec}: see later
    \item (and code review)
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Clean local history}

\begin{frame}
  \frametitle{Example}

  Implement \mintinline{sh}{git clone -c var=value} : 9 preparation patches, 1
  real (trivial) patch at the end!
  \begin{center}
    \url{https://github.com/git/git/commits/84054f79de35015fc92f73ec4780102dd820e452}
  \end{center}

  Did the author actually write this in this order?
\end{frame}

\subsection{Avoiding merge commits: \mintinline{sh}{rebase} Vs \mintinline{sh}{merge}}

\begin{frame}
  \frametitle{Merging With Upstream}

  \begin{center}
    \textbf{Question:} upstream (where my code should eventually end up) has
    new code, how do I get it in my repo?
  \end{center}
  \begin{itemize}
  \item Approach 1: merge (default with \mintinline{sh}{git pull})
    \scalebox{.5}{
    \begin{tikzpicture}[scale=1]
      \draw node[gitcommit](root) {};
      \coordinate (current) at (root);

      \uncover<2->{
        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream1) {};
        \draw[commit arc] (upstream1) -- (root);
      }

      \uncover<4->{
        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream2) {};
        \draw[commit arc] (upstream2) -- (upstream1);
      }

      \uncover<6->{
        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream3) {};
        \draw[commit arc] (upstream3) -- (upstream2);

        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream4) {};
        \draw[commit arc] (upstream4) -- (upstream3);
      }

      \uncover<3->{
        \draw (upstream1) ++(2,-1) node[gitcommit](topic1) {} node[commit text,above=0.1cm] {\large A};
        \draw[commit arc] (topic1) -- (upstream1);
      }

      \uncover<5->{
        \draw (upstream2) ++(2,-1) node[gitcommit](topic2) {} node[commit text,above=0.1cm] {\large Merge1};
        \draw[commit arc] (topic2) -- (upstream2);
        \draw[commit arc] (topic2) -- (topic1);
      }

      \uncover<7->{
        \draw (topic2) ++(3,0) node[gitcommit](topic3) {} node[commit text,above=0.1cm] {\large B};
        \draw[commit arc] (topic3) -- (topic2);
        \draw (topic3) ++(3,0) node[gitcommit](topic4) {} node[commit text,above=0.1cm] {\large C};
        \draw[commit arc] (topic4) -- (topic3);
      }

      \uncover<8->{
        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream5) {} node[commit text,above=0.1cm] {\large Merge2};
        \draw[commit arc] (upstream5) -- (upstream4);
        \draw[commit arc] (upstream5) -- (topic4);
      }
    \end{tikzpicture}
    }
    \uncover<9->{
  \item Drawbacks:
    \begin{itemize}
    \item Merge1 is not relevant, distracts reviewers (unlike Merge2).
    \end{itemize}
  }
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Merging With Upstream}

  \begin{center}
    Question: upstream (where my code should eventually end up) has
    new code, how do I get it in my repo?
  \end{center}
  \begin{itemize}
  \item Approach 2: no merge
    \scalebox{.5}{
        \begin{tikzpicture}[scale=1]
      \draw node[gitcommit](root) {};
      \coordinate (current) at (root);

      \uncover<2->{
        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream1) {};
        \draw[commit arc] (upstream1) -- (root);
      }

      \uncover<4->{
        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream2) {};
        \draw[commit arc] (upstream2) -- (upstream1);
      }

      \uncover<4->{
        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream3) {};
        \draw[commit arc] (upstream3) -- (upstream2);

        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream4) {};
        \draw[commit arc] (upstream4) -- (upstream3);
      }

      \uncover<3->{
        \draw (upstream1) ++(2,-1) node[gitcommit](topic1) {} node[commit text,above=0.1cm] {\large A};
        \draw[commit arc] (topic1) -- (upstream1);
      }

      \uncover<5->{
        \draw (topic1) ++(5,0) node[gitcommit](topic3) {} node[commit text,above=0.1cm] {\large B};
        \draw[commit arc] (topic3) -- (topic1);
        \draw (topic3) ++(3,0) node[gitcommit](topic4) {} node[commit text,above=0.1cm] {\large C};
        \draw[commit arc] (topic4) -- (topic3);
      }

      \uncover<6->{
        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream5) {} node[commit text,above=0.1cm] {\large Merge2};
        \draw[commit arc] (upstream5) -- (upstream4);
        \draw[commit arc] (upstream5) -- (topic4);
      }
    \end{tikzpicture}
    }
    \uncover<6->{
  \item Drawbacks:
    \begin{itemize}
    \item In case of conflict, they have to be resolved by the
      developer merging into upstream (possibly after code review)
    \item Not always applicable (e.g. ``I need this new upstream
      feature to continue working'')
    \end{itemize}
  }
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Merging With Upstream}

  \begin{center}
    Question: upstream (where my code should eventually end up) has
    new code, how do I get it in my repo?
  \end{center}
  \begin{itemize}
  \item Approach 3: rebase (\mintinline{sh}{git rebase} or \mintinline{sh}{git pull --rebase})
    \scalebox{.5}{
    \begin{tikzpicture}[scale=1]
      \draw node[gitcommit](root) {};
      \coordinate (current) at (root);
      \coordinate (master) at (root.east);

      \uncover<2->{
        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream1) {};
        \draw[commit arc] (upstream1) -- (root);
        \only<2->{
          \coordinate (master) at (upstream1.east);
        }
      }

      \uncover<4->{
        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream2) {};
        \draw[commit arc] (upstream2) -- (upstream1);
        \only<4->{
          \coordinate (master) at (upstream2.east);
        }
      }

      \uncover<6->{
        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream3) {};
        \draw[commit arc] (upstream3) -- (upstream2);

        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream4) {};
        \draw[commit arc] (upstream4) -- (upstream3);
        \only<6->{
          \coordinate (master) at (upstream4.east);
        }
      }

      \uncover<3-9,11>{
        \draw (upstream1) ++(1.5,-1) node[gitcommit](topic1) {} node[commit text,above=0.1cm] {\large A};
        \draw[commit arc] (topic1) -- (upstream1);
        \only<3->{
          \coordinate (topic) at (topic1.east);
        }
      }

      \uncover<5-9,11>{
        \draw (upstream2) ++(1.5,-1) node[gitcommit](topic2) {} node[commit text,above=0.1cm] {\large A'};
        \draw[commit arc] (topic2) -- (upstream2);
        \only<5->{
          \coordinate (topic) at (topic2.east);
        }
      }

      \uncover<7-9,11>{
        \draw (topic2) ++(1.5,-1) node[gitcommit](topic3) {} node[commit text,above=0.1cm] {\large B};
        \draw[commit arc] (topic3) -- (topic2);
        \draw (topic3) ++(1.5,-1) node[gitcommit](topic4) {} node[commit text,above=0.1cm] {\large C};
        \draw[commit arc] (topic4) -- (topic3);
        \only<7->{
          \coordinate (topic) at (topic4.east);
        }
      }

      \uncover<8->{
        \draw (upstream4) ++(1.5,-1) node[gitcommit](topic2) {} node[commit text,above=0.1cm] {\large A''};
        \draw[commit arc] (topic2) -- (upstream4);
        \draw (topic2) ++(1.5,-1) node[gitcommit](topic3) {} node[commit text,above=0.1cm] {\large B'};
        \draw[commit arc] (topic3) -- (topic2);
        \draw (topic3) ++(1.5,-1) node[gitcommit](topic4) {} node[commit text,above=0.1cm] {\large C'};
        \draw[commit arc] (topic4) -- (topic3);
        \only<8->{
          \coordinate (topic) at (topic4.east);
        }
      }

      \uncover<9->{
        \draw (current) ++(5, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream5) {} node[commit text,above=0.1cm] {\large Merge2};
        \draw[commit arc] (upstream5) -- (upstream4);
        \draw[commit arc] (upstream5) -- (topic4);
        \only<9->{
          \coordinate (master) at (upstream5.east);
        }
      }

      \draw (master) ++(2.5,0) node[gitbackref](masterref) {master};
      \draw[commit arc] (masterref.west) -- (master);

      \only<3->{
        \draw (topic) ++(2.5,0) node[gitbackref](topicref) {topic};
        \draw[commit arc] (topicref.west) -- (topic);
      }
    \end{tikzpicture}
    }
    \uncover<11>{
    \item Drawbacks: rewriting history implies:
      \begin{itemize}
      \item A', A'', B', C' probably haven't been tested (never existed on disk)
      \item What if someone branched from A, A', B or C?
      \item Basic rule: don't rewrite published history
      \end{itemize}
    }
  \end{itemize}
\end{frame}

\subsection{Rewriting history with \texttt{rebase -i}}

\begin{frame}[fragile]
  \frametitle{Rewriting history with \texttt{rebase -i}}

  \begin{itemize}
  \item \mintinline{sh}{git rebase}: take all your commits, and re-apply them onto upstream
  \item \mintinline{sh}{git rebase -i}: show all your commits, and asks you
    what to do when applying them onto upstream.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Rewriting history with \texttt{rebase -i}}
{\footnotesize
    \begin{semiverbatim}
pick ca6ed7a Start feature A
pick e345d54 Bugfix found when implementing A
pick c03fffc Continue feature A
pick 5bdb132 Oops, previous commit was totally buggy

{\tiny{}# Rebase 9f58864..5bdb132 onto 9f58864
#
# Commands:
#  p, pick = use commit
#  r, reword = use commit, but edit the commit message
#  e, edit = use commit, but stop for amending
#  s, squash = use commit, but meld into previous commit
#  f, fixup = like "squash", but discard this commit's log message
#  x, exec = run command (the rest of the line) using shell
#
# These lines can be re-ordered; they are executed from top to bottom.
#
# If you remove a line here THAT COMMIT WILL BE LOST.
#
# However, if you remove everything, the rebase will be aborted.
#
# Note that empty commits are commented out}
    \end{semiverbatim}
}
\end{frame}

\begin{frame}
  \frametitle{\texttt{git rebase -i} commands (1/2)}

  \begin{description}
  \item[p, pick] use commit (by default)
  \item[r, reword] use commit, but edit the commit message\\
    Fix a typo in a commit message
  \item[e, edit] use commit, but stop for amending\\
    $\leadsto$ Once stopped, use \mintinline{sh}{git add -p}, \mintinline{sh}{git commit
      --amend},...
  \item[s, squash] use commit, but meld into previous commit
  \item[f, fixup] like "squash", but discard this commit's log message\\
    $\leadsto$ Very useful when polishing a set of commits (before or after
    review): make a bunch of short fixup patches, and squash them
    into the real commits. No one will know you did this
    mistake ;-).
  \end{description}
\end{frame}


\begin{frame}
  \frametitle{\texttt{git rebase -i} commands (2/2)}

  \begin{description}
  \item[x, exec] run command (the rest of the line) using shell
    \begin{itemize}
    \item Example: \mintinline{sh}{exec make check}. Run tests for this
      commit, stop if test fail.
    \item Use \mintinline{sh}{git rebase -i --exec 'make
        check'}\footnote{Implemented by Ensimag students!} to run
      \mintinline{sh}{make check} for each rebased commit.
    \end{itemize}
%  \item More to come (GSoC 2014)
  \end{description}
\end{frame}

\section{Repairing mistakes: the reflog}

\begin{frame}
  \frametitle{Git's reference journal: the reflog}

  \begin{itemize}
  \item Remember the history of local refs.
  \item $\neq$ ancestry relation.
  \end{itemize}

  \scalebox{.5}{
    \begin{tikzpicture}[scale=1]
      \draw node[gitcommit](root) {};
      \coordinate (current) at (root);
      \coordinate (master) at (root.east);

%      \uncover<2->{
        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream1) {};
        \draw[commit arc] (upstream1) -- (root);
%        \only<2->{
          \coordinate (master) at (upstream1.east);
%          }
%      }

%      \uncover<4->{
        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream2) {};
        \draw[commit arc] (upstream2) -- (upstream1);
%        \only<4->{
          \coordinate (master) at (upstream2.east);
%        }
%      }

%      \uncover<6->{
        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream3) {};
        \draw[commit arc] (upstream3) -- (upstream2);

        \draw (current) ++(3, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream4) {};
        \draw[commit arc] (upstream4) -- (upstream3);
%        \only<6->{
          \coordinate (master) at (upstream4.east);
%        }
%      }

%      \uncover<3-9,11>{
        \draw (upstream1) ++(1.5,-1) node[gitcommit](topic1) {} node[commit text,above=0.1cm] {\large A};
        \coordinate (A) at (topic1);
        \draw[commit arc] (topic1) -- (upstream1);
%        \only<3->{
          \coordinate (topic) at (topic1.east);
%        }
%      }

%      \uncover<5-9,11>{
        \draw (upstream2) ++(1.5,-1) node[gitcommit](topic2) {} node[commit text,above=0.1cm] {\large A'};
        \coordinate (A') at (topic2);
        \draw[commit arc] (topic2) -- (upstream2);
%        \only<5->{
          \coordinate (topic) at (topic2.east);
%        }
%      }

%      \uncover<7-9,11>{
        \draw (topic2) ++(1.5,-1) node[gitcommit](topic3) {} node[commit text,above=0.1cm] {\large B};
        \coordinate (B) at (topic3);
        \draw[commit arc] (topic3) -- (topic2);
        \draw (topic3) ++(1.5,-1) node[gitcommit](topic4) {} node[commit text,above=0.1cm] {\large C};
        \coordinate (C) at (topic4);
        \draw[commit arc] (topic4) -- (topic3);
%        \only<7->{
          \coordinate (topic) at (topic4.east);
%        }
%      }

%      \uncover<8->{
        \draw (upstream4) ++(1.5,-1) node[gitcommit](topic2) {} node[commit text,above=0.1cm] {\large A''};
        \coordinate (A'') at (topic2);
        \draw[commit arc] (topic2) -- (upstream4);
        \draw (topic2) ++(1.5,-1) node[gitcommit](topic3) {} node[commit text,above=0.1cm] {\large B'};
        \coordinate (B') at (topic3);
        \draw[commit arc] (topic3) -- (topic2);
        \draw (topic3) ++(1.5,-1) node[gitcommit](topic4) {} node[commit text,above=0.1cm] {\large C'};
        \coordinate (C') at (topic4);
        \draw[commit arc] (topic4) -- (topic3);
%        \only<8->{
          \coordinate (topic) at (topic4.east);
%        }
%      }

%      \uncover<9->{
        \draw (current) ++(5, 0) coordinate (current);
        \draw (current) node[gitcommit](upstream5) {} node[commit text,above=0.1cm] {\large Merge2};
        \draw[commit arc] (upstream5) -- (upstream4);
        \draw[commit arc] (upstream5) -- (topic4);
%        \only<9->{
          \coordinate (master) at (upstream5.east);
%        }
%      }

        \draw (master) ++(2.5,0) node[gitbackref](masterref) {master};
        \draw[commit arc] (masterref.west) -- (master);
        
        \draw (topic) ++(2.5,0) node[gitbackref](topicref) {topic};
        \draw[commit arc] (topicref.west) -- (topic);
        
        \uncover<2->{
          \draw [line width=.5ex,red!50!black,opacity=.5] plot 
          [smooth, tension=.4] coordinates {(upstream1) (A) (A') (B) (C)
            % Rebase happens on detached HEAD.
            % (A'') (B') 
            (C')};
        }
        \uncover<3->{
          \draw (C')  ++(0,-1) node[gitref](r){topic@\{0\}}; \draw[commit arc] (r.east) -- (C'.south);
          \draw (C)   ++(0,-1) node[gitref](r){topic@\{1\}}; \draw[commit arc] (r.east) -- (C.south);
          \draw (A)   ++(0,-1) node[gitref](r){topic@\{2\}}; \draw[commit arc] (r.east) -- (A.south);
        }
        \uncover<4->{
          \draw (B')  ++(0,4) node[gitref](r){HEAD@\{1\}}; \draw[commit arc] (r.east) -- (B'.south);
          \draw (A'') ++(0,2) node[gitref](r){HEAD@\{2\}}; \draw[commit arc] (r.east) -- (A''.south);
        }
      \end{tikzpicture}
    }
    \uncover<4->{
      \begin{itemize}
      \item \texttt{\textit{ref}@\{\textit{n}\}}: where \textit{ref}
        was before the \textit{n} last ref update.
      \item \texttt{\textit{ref}\textasciitilde\textit{n}}: the
        \textit{n}-th generation ancestor of \textit{ref}
      \item \texttt{\textit{ref}\^{}}: first parent of \textit{ref}
      \item \mintinline{sh}{git help revisions} for more
      \end{itemize}
    }
\end{frame}


\section{More Documentation}

\begin{frame}
\frametitle{More Documentation}
  \begin{itemize}
  \item \url{http://ensiwiki.ensimag.fr/index.php/Maintenir_un_historique_propre_avec_Git}
  \item \url{http://ensiwiki.ensimag.fr/index.php/Ecrire_de_bons_messages_de_commit_avec_Git}
  \end{itemize}
\end{frame}

% \section{Exercises}

% \begin{frame}[fragile]
%   \frametitle{Exercises}

%   \begin{itemize}
%   \item Visit \url{https://github.com/moy/dumb-project.git}
%   \item Fork it from the web interface (or just \texttt{git clone})
%   \item Clone it on your machine
%   \item Repair the dirty history!
%   \end{itemize}
% \end{frame}

\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "american"
%%% End:
