%\documentclass{beamer}

\usepackage{moy-bouveret-slides}
\usepackage{comment}
\usepackage{minted}
\usemintedstyle{tango}
% \excludecomment{detailed}
\newenvironment{detailed}{}{}
\usepackage{moy-bouveret-tikz}

\title{Using Git}

%\subtitle{...}

\author{Sylvain Bouveret, Grégory Mounié, Matthieu Moy}

\institute[first.last@imag.fr]{[first].[last]@imag.fr\\
\url{https://git.pages.ensimag.fr/formation-git/slides/\jobname.pdf}}

\date{2021}

\begin{document}

\begin{frame}[plain]
  \titlepage

  \begin{center}
    \includegraphics[width=2cm]{images/CC-BY-SA}
  \end{center}
\end{frame}

\begin{frame}{First, safety instructions...}
  \begin{center}
    \includegraphics[width=0.8\textwidth]{fig/in-case-of-fire}
  \end{center}
\end{frame}

\section{Revision Control System}

\begin{frame}[fragile] \frametitle{Backups: The Good Old Time}
  \begin{itemize}[<.->]
  \item<+-> \alert<.>{Basic problems}:
    \begin{itemize}
    \item ``Oh, my disk crashed.'' / ``Someone has stolen my laptop!''
    \item ``@\#\%!!, I've just deleted this important file!''
    \item ``Oops, I introduced a bug a long time ago in my code, how
      can I see how it was before?''
    \end{itemize}
  \item<+-> \alert<.>{Historical solutions}:
    \begin{itemize}
    \item \alert<.>{Replicate}:\\
      \verb|$ cp -r ~/project/ ~/backup/|\\
      (or better, copy to a remote machine)
    \item \alert<.>{Keep history}:\\
      \verb|$ cp -r ~/project/ ~/backup/project-2021-04-26|
    \item \dots
    % \item \alert<.>{Keep a description of history}:\\
    %   \verb|$ echo "Description of current state" > \|
    %   \verb|    ~/backup/project-2012-02-02/README.txt|
%$
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame} \frametitle{Collaborative Development: The Good Old Time}
  \begin{itemize}[<.->]
  \item<+-> \alert<.>{Basic problems}: Several persons working on the
    same set of files
    \begin{enumerate}
    \item ``Hey, you've modified the same file as me, how do we
      merge?'',
    \item ``Your modifications are broken, your code doesn't even
      compile. Fix your changes before sending it to me!'',
    \item ``Your bug fix here seems interesting, but I don't want your
      other changes''.
    \end{enumerate}
  \item<+-> \alert<.>{Historical solutions}:
    \begin{itemize}
    \item Never two person work at the same time.
      $\Rightarrow$ Doesn't scale up! Unsafe.
    \item People work on the same directory (Dropbox, same machine, NFS, ACLs\dots)\\
      $\Rightarrow$ Painful because of (2) above.
    % \item People lock the file when working on it.\\
    %   $\Rightarrow$ Hardly scales up!
    \item People work trying to avoid conflicts, and \alert<.>{merge}
      later.
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}[fragile] \frametitle{Merging: Problem and Solution}
  \footnotesize
  \begin{columns}[t]
    \begin{column}{.3\textwidth}
      My version
{\scriptsize
\begin{minted}[escapeinside=||]{C}
#include <stdio.h>

int main () {
  printf("Hello");

  return |\alert{EXIT\_SUCCESS}|;
}
\end{minted}
}
    \end{column}
    \begin{column}{.3\textwidth}
      Your version
      {\scriptsize
% l'option minted escapeinside ne marche pas dans une string        
\begin{minted}{c}
#include <stdio.h>

int main () {
  printf("Hello\n");

  return 0;
}
\end{minted}
}
    \end{column}
    \pause
    \begin{column}{.3\textwidth}
    Common ancestor
    {\scriptsize
\begin{minted}{c}
#include <stdio.h>

int main () {
  printf("Hello");

  return 0;
}
\end{minted}
}
    \end{column}
  \end{columns}
  \pause
  \normalsize
  \begin{center}
    This merge can be done for you by an automatic tool
  \end{center}
  \begin{center}
    \alert{Merging relies on history!}
  \end{center}
  \pause
  \begin{center}
    \alert{Collaborative development linked to backups}
  \end{center}
\end{frame}

\begin{frame} \frametitle{Merging}
  \begin{center}
    \input tikz/merge.tex
  \end{center}
\end{frame}

\begin{frame} \frametitle{Revision Control System: Basic Idea}
  \begin{itemize}[<.->]
  \item<+-> Keep track of \alert<.>{history}:
    \begin{itemize}
    \item {\tt commit} = snapshot of the current state,
    \item Meta-data (user's name, date, descriptive message,\dots{})
      recorded in commit.
    \end{itemize}
  \item Use it for \alert<.>{merging}/collaborative development.
    \begin{itemize}
    \item Each user works on its own copy,
    \item User explicitly ``takes'' modifications from others when
      (s)he wants.
    \end{itemize}
  \item<+-> Efficient storage/compression (``delta-compression
    $\approx$ incremental backup'')
  \end{itemize}
\end{frame}

\section{Git: Basic Principles}

\begin{frame}[label=basic-concepts] \frametitle{Git: Basic concepts}
  \begin{itemize}
  \item Each working directory contains:
    \begin{itemize}
    \item The files you work on (as usual)
    \item The history, or ``repository'' (in the directory {\tt .git/})
    \end{itemize}
  \item Basic operations:
    \begin{itemize}
    \item \alert{git clone}: get a copy of an existing repository
      (files + history)
    \item \alert{git commit}: create a new revision in a repository
    \item \alert{git pull}: get revisions from a repository
    \item \alert{git push}: send revisions to a repository
    \item \alert{git add}, \alert{git rm} and \alert{git mv}: tell Git
      which files should be tracked
    \item \alert{git status}: know what's going on
    \end{itemize}
  % \item For us:
  %   \begin{itemize}
  %   \item Each team creates a shared repository, in addition to work trees
  %   \end{itemize}
  \end{itemize}
\end{frame}

\section{Git Vs Others}

\subsection{History}

\begin{frame} \frametitle{A bit of history}
  \begin{description}
  \item[1986:] Birth of CVS, centralized version system
  \item[2000:] Birth of Subversion (SVN), a replacement for CVS with
    the same concepts
  \item[2005:] First version of Git
  \end{description}
\end{frame}

\begin{frame} \frametitle{Git and the Linux Kernel}
  \begin{description}
  \item[1991:] Linus Torvalds starts writing Linux, using mostly tar+patch,
  \item[2002:] Linux adopts BitKeeper, a proprietary decentralized
    version control system (available free of cost for Linux),
  \item[2002-2005:] Flamewars against BitKeeper, some Free Software
    alternatives appear (GNU Arch, Darcs, Monotone). None are good
    enough technically.
    \pause
  \item[2005:] BitKeeper's free of cost license revoked. Linux has to
    migrate.
  \item[2005:] Unsatisfied with the alternatives, Linus decides to start
    his own project, \alert{Git}.
  \end{description}
\end{frame}

\begin{frame} \frametitle{Git and the Linux Kernel}
  \begin{description}
  \item[2007:] Many young, but good projects for decentralized
    revision control: Git, Mercurial, Bazaar, Monotone, Darcs, \dots{}
  \item[2014:] Git is the most widely used according to Eclipse user's
    survey.
  \end{description}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Who Makes Git?}
  \footnotesize
  \def\employer#1{\(\leftarrow \text{\textsf{\alert{#1}}}\)}
  \begin{semiverbatim}
\$ git shortlog -s --no-merges | sort -nr | head -30
  7280  Junio C Hamano  \employer{Google (full-time on Git)}
  3566  Jeff King \employer{GitHub (\(\approx\) full-time on Git)}
  1873  Johannes Schindelin \employer{Microsoft (full-time on Git)}
  1824  Nguyên Thái Ngoc Duy
  1290  Shawn O. Pearce \employer{Google}
  1104  Linus Torvalds \textsf{(No longer very active contributor)}
  1002  René Scharfe
   953  Michael Haggerty  \employer{GitHub}
   861  Ævar Arnfjörð Bjarmason
   804  Jonathan Nieder  \employer{Google}
   781  brian m. carlson
   743  Elijah Newren
   638  Stefan Beller
   [...]
   287  Matthieu Moy  \textsf{(rank 30 / 1868)}
 \end{semiverbatim}
\end{frame}

\subsection{Popularity}

% To get the popcon graph image with several packages in 2021
% interactive interface:
% https://qa.debian.org/popcon-graph.php?packages=git
%
% plot generated without git (for history, or comparison without git)
% https://qa.debian.org/cgi-bin/popcon-png?packages=subversion+cvs+mercurial+rcs+bzr+brz+fossil+darcs+monotone+codeville+tla&show_installed=0&show_vote=1&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y-%25m
%
% plot generated with git
% https://qa.debian.org/cgi-bin/popcon-png?packages=subversion+cvs+mercurial+rcs+bzr+brz+fossil+darcs+monotone+codeville+tla+git&show_installed=0&show_vote=1&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y-%25m

\begin{frame} \frametitle{Git Adoption (Debian popularity contest in 2021)}
  \begin{center}
    \includegraphics[width=.8\textwidth]{fig/popcon-png}
  \end{center}
\end{frame}

\begin{frame}\frametitle{Git Adoption (Job offers at the turmoil around 2015)}
  \begin{center}
    \only<1>{\includegraphics[width=.8\textwidth]{fig/jobgraph}}
    \only<2>{\includegraphics[width=.8\textwidth]{fig/jobgraph-proprietary}}
  \end{center}  
\end{frame}

\begin{frame}
  \frametitle{Git Adoption (Hosting)}

  \begin{itemize}
  \item 2015: ``There are 11.6M people collaborating right now across 29.1M
    repositories on
    GitHub'' \beamerbutton{\url{https://github.com/about/press}}
  \item 2017: 25M people and 75M repositories
  \item 2020: 40M people and 190M repositories
  \item How about Mercurial?\\[0.1cm]
    \includegraphics[width=.7\textwidth]{fig/mercurial-hosting}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Summary of Available Libre Options}
  \begin{itemize}
  \item Centralized
    \begin{itemize}
    \item \textbf{RCS, CVS}: Outdated
    \item \textbf{SVN}: Did the job
    \end{itemize}
  \item Decentralized
    \begin{itemize}
    \item \textbf{Git}: Fast, powerful, popular
    \item \textbf{Mercurial (hg)}: Very similar to Git but designed to be
      simpler. Less popular but was very active too.
    \item \textbf{Bazaar (bzr)} then \textbf{Breezy (brz)}: former official Ubuntu devel tool. Now, brz is able to use the Git format too.
    \item \textbf{Monotone (mtn)}: Invented the core concepts behind
      Git, slow, never took up, dead
    \item \textbf{Darcs}: Novel design around patches (merge by
      rebasing without changing commit), slow (exponential
      worst-case), never took up
    \item \textbf{Fossil}: all-in-one (bug tracker, wiki, forums, chat, technotes, web), never took up
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Centralized Vs Decentralized}

\begin{detailed}
  \begin{frame}
 \frametitle{CVS and SVN: Commit/Update Approach}
  \begin{center}
    \input{tikz/commit-update.tex}
  \end{center}
\end{frame}

\begin{frame} \frametitle{Commit/Update Approach : limitations}
  \begin{itemize}
  \item A change is either ``uncommited'' or ``cast in stone''
  \item Update before commit: what if the merge fails?
  \item No easy way to contribute to a repo without write permission
  \end{itemize}
\end{frame}
\end{detailed}

\begin{frame} \frametitle{Decentralized Version Control}
  \begin{center}
    \huge Decentralized:\\
    Each developer has its own repository
  \end{center}

  \pause
  
  \begin{itemize}
  \item Works offline, fast (I\fup{$1$} use \texttt{git} more than \texttt{ls}
    and \texttt{cd} !)
  \item Replicate data ($\Rightarrow$ safer)
  \item No need for a server, creating a repo is cheap (I\fup{$1$} have 200
    repos on my account)
  \item Private space (draft, not cast in stone)
  \item Different workflows, including the usage of centralized hub of
    repositories like GitHub and GitLab.
  \end{itemize}

  \vfill
  \hrule
  {\scriptsize \fup{$1$} Matthieu Moy speaking (2015)}
\end{frame}

\begin{detailed}
  \input{example.tex}
\end{detailed}

\section{Advices Using Git}

\begin{frame} \frametitle{Advices Using Git (\underline{for beginners})}
  \begin{itemize}
  \item \alert{Never} exchange files outside Git's control (email,
    scp, usb key), except if you \emph{really} know what you're
    doing;
    \pause
  \item Publish every change, every time (SVN behaviour):
    \begin{itemize}
    \item {\tt git commit} with \alert{\tt -a};
    \item Make a  \alert{\tt git push} after each {\tt git commit -a}
      (use {\tt git pull} if needed);
  \end{itemize}
  \item Do \alert{\tt git pull} regularly, to remain synchronized with
    your teammates. You need to make a {\tt git commit -a} before you
    can make a {\tt git pull} (this is to avoid mixing manual changes
    with merges).
  \item Do not make useless changes to your code. Do not let your
    editor/IDE reformat code that is not yours.
  \end{itemize}
\end{frame}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "american"
%%% End:
