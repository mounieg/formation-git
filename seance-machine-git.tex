\documentclass[a4paper,10pt]{article}

\usepackage[frenchb]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\DeclareUnicodeCharacter{00A0}{~}
\usepackage[left=2.5cm,right=2.5cm,top=2.5cm,bottom=2.5cm]{geometry}
\usepackage{microtype}

\usepackage{url}
\usepackage{hyperref}
\usepackage{tikz}
\usetikzlibrary{shapes}

\date{Novembre 2015}
\author{Matthieu Moy}
\title{Utilisation de Git\\Ensimag 1A}

\sloppy

\begin{document}
\maketitle

Ce document peut être téléchargé depuis l'adresse suivante :\\
\url{http://www-verimag.imag.fr/~moy/cours/formation-git/seance-machine-git.pdf}

\section{Introduction}

\subsection{Git et la gestion de versions}

Git est un gestionnaire de versions, c'est à dire un logiciel qui
permet de conserver l'historique des fichiers sources d'un projet, et
d'utiliser cet historique pour fusionner automatiquement plusieurs
révisions (ou « versions »). Chaque membre de l'équipe travaille sur
sa version du projet, et peut envoyer les versions suffisamment
stables à ses coéquipiers via un dépôt partagé (commande {\tt git
  push}) qui pourront les récupérer et les intégrer aux leurs quand
ils le souhaitent (commande {\tt git pull}).

Il existe beaucoup d'autres gestionnaires de versions. La page
\url{http://ensiwiki.ensimag.fr/index.php/Gestionnaire_de_Versions}
vous donne un aperçu de l'existant.

\subsection{Organisation pendant la séance machine}

Pour la séance machine, choisissez deux PC adjacents par équipe
(on peut utiliser son ordinateur portable à la place d'un PC de l'école). Chaque
étudiant travaille sur son compte.

On choisit le compte de l'étudiant qui hébergera le dépôt partagé
(sur {\tt depots.ensimag.fr}: ce dépôt doit être accessible en permanence donc
hébergé sur un serveur). ce dépôt sera simplement un répertoire qui
contiendra l'ensemble de l'historique du projet. On ne travaillera
jamais dans ce répertoire directement, mais on utilisera Git pour
envoyer et récupérer des révisions. Tous les membres de l'équipe
auront accès à ce dépôt en lecture et en écriture, donc le choix
du compte hébergeant le dépôt n'a pas beaucoup d'importance.

Dans la suite des explications, on suppose que l'utilisateur {\tt
  alice} héberge le dépôt sur la machine {\tt depots.ensimag.fr}. L'équipe est
constituée d'{\it Alice} (qui travaille plutôt sur son portable, {\tt
  laptop1}) et {\it Bob}, qui travaille également sur son portable {\tt
  laptop2}). Si {\it Alice} ou {\it Bob} travaille sur
un PC de l'école, en remplaçant {\tt laptop1} ou {\tt laptop2} par le
nom de la machine (e.g. {\tt ensipc42}). Les explications sont écrites pour 2
utilisateurs pour simplifier, mais il peut y avoir un nombre
quelconque de coéquipiers.

\tikzstyle{depot}=[draw=black,shape=ellipse]
\begin{center}
\begin{tikzpicture}
  \draw node[depot] (partagee) {
    \begin{tabular}{c}
Dépôt Partagé\\
{\tt depots.ensimag.fr}
\end{tabular}
};
  \draw (partagee) ++(-3, -2) node[depot] (alice) {
    \begin{tabular}{c}
Répertoire de\\
travail d'{\it Alice}\\
{\tt alice@laptop1}
\end{tabular}
};
  \draw (partagee) ++(+3, -2) node[depot] (bob) {
    \begin{tabular}{c}
Répertoire de\\
travail de {\it Bob}\\
{\tt bob@laptop2}
\end{tabular}
};

\draw[<->] (partagee) -- (alice);
\draw[<->] (partagee) -- (bob);
\end{tikzpicture}
\end{center}

\section{Configuration de Git}

Si vous travaillez sur votre machine personnelle, vérifiez que Git est
installé (la commande {\tt git}, sans argument, doit vous donner un
message d'aide). Si ce n'est pas le cas, installez-le (sous Ubuntu,
« {\tt apt-get install git gitk} » ou « {\tt apt-get install git-core gitk} »
devrait faire l'affaire, ou bien rendez-vous sur \url{http://git-scm.com/}).

On commence par configurer l'outil Git. Sur la machine sur laquelle on
souhaite travailler (donc sur vos portables dans notre exemple) :
\begin{verbatim}
git config --edit --global
\end{verbatim}
Ou bien :
\begin{verbatim}
emacs ~/.gitconfig          # ou son éditeur préféré à la place d'Emacs !
\end{verbatim}
Le contenu du fichier {\tt .gitconfig} (à créer s'il n'existe pas)
doit ressembler à ceci :

\begin{verbatim}
[core]
        editor = votre_editeur_prefere
[user]
        name = Prénom Nom
        email = Prenom.Nom@ensimag.grenoble-inp.fr
[diff]
        renames = true
[push]
        default = simple
# Section ci-dessous pas nécessaires avec un Git récent
[color]
        ui = auto
\end{verbatim}

La section {\tt [user]} est obligatoire, elle donne les informations
qui seront enregistrées par Git lors d'un {\tt commit}. Il est
conseillé d'utiliser votre vrai nom (pas juste votre login) et votre
adresse officielle Ensimag ici, et d'utiliser la même configuration
sur toutes les machines sur lesquelles vous travaillez.

La ligne {\tt editor} de la section {\tt [core]} définit votre éditeur
de texte préféré (par exemple, {\tt emacs}, {\tt vim}, {\tt gvim
  -f},\dots{} mais évitez \texttt{gedit} qui vous posera problème
ici)\footnote{Si un {\tt gedit} est déjà lancé, la commande {\tt git
    commit} va se connecter au {\tt gedit} déjà lancé pour lui
  demander d'ouvrir le fichier, et le processus lancé par {\tt git} va
  terminer immédiatement. Git va croire que le message de commit est
  vide, et abandonner le commit. Il semblerait que {\tt gedit -s -w}
  règle le problème, mais cette commande est
  disponible seulement avec les versions $\geq$ 3.1.2 de {\tt gedit},
  donc pas sur CentOS 6, mais peut-être sur vos portables}. Cette
dernière ligne n'est pas obligatoires ; si elle n'est pas présente, la
variable d'environnement {\tt VISUAL} sera utilisée ; si cette
dernière n'existe pas, ce sera la variable d'environnement {\tt
  EDITOR}.

la section {\tt [diff]} et la section {\tt [color]} sont là pour
rendre l'interface de Git plus jolie. La section {\tt [push]} permet
d'avoir le même comportement avec Git 2.x et Git 1.x (utiliser {\tt
  current} au lieu de {\tt simple} avec les très vieilles versions de
Git).

\section{Mise en place}

Le contenu de cette section est réalisé une bonne fois pour toute, au
début du projet. Si certains membres de l'équipe ne comprennent pas
les détails, ce n'est pas très grave, nous verrons ce que tout le
monde doit savoir dans la section~\ref{sec=utilisation}.

\subsection{Création du dépôt partagé}
\label{sec=creation-depot-partagee}

On va maintenant créer le dépôt partagé. Seule {\it Alice} fait cette
manipulation, sur son compte {\tt depots.ensimag.fr} (le dépôt partagé
doit être créé sur un serveur pour être accessible en permanence ;
{\tt depots.ensimag.fr} est celui sur lesquels sont hébergés les dépôts Git à
l'Ensimag). Il faut dans un premier temps ouvrir un shell sur cette
machine avec ssh :

\begin{verbatim}
ssh depots.ensimag.fr
\end{verbatim}

Si votre mot de passe n'est pas reconnu, rendez-vous sur la page
\url{https://intranet.ensimag.fr/passwords/} et modifiez ou re-validez
votre mot de passe (vous devriez avoir une case « Serveur
'depots.ensimag.fr' », gardez-la cochée), puis ré-essayez.

On commence par créer un répertoire, et on donne les droits aux
autres coéquipiers via les ACLs (Access Control Lists), en utilisant
le script {\tt autoriser-equipe} spécifique à l'Ensimag :

\begin{verbatim}
cd /depots/$annee/
mkdir alice-et-bob/
chmod 700 alice-et-bob/
autoriser-equipe alice-et-bob/ bob
\end{verbatim}

Il faut ici préciser les logins de tous les coéquipiers, donc si l'équipe est
constituée des utilisateurs unix {\tt alice}, {\tt bob}, {\tt charlie}
et {\tt dave}, on entrera la commande
\begin{verbatim}
autoriser-equipe alice-et-bob/ bob charlie dave
\end{verbatim}
Les noms d'utilisateurs (login) sont ceux sur {\tt depots.ensimag.fr}, même si les
utilisateurs travaillent avec un autre nom sur leur machine
personnelle.

On peut maintenant créer le dépôt Git partagé à l'intérieur de ce
répertoire :

\begin{verbatim}
cd alice-et-bob/
git init --shared --bare projetc.git
\end{verbatim}

Si on est curieux, on peut regarder le contenu du répertoire
\texttt{projetc.git} : c'est un ensemble de fichiers que Git utilise
pour représenter l'état et l'historique de notre projet (les fichiers
sur lesquels on travaille n'y sont pas).

Nous avons terminé la création du dépôt sur {\tt depots.ensimag.fr},
et c'est la seule chose que nous faisons sur cette machine. Vous
pouvez maintenant revenir sur votre machine de travail habituelle (PC
de l'Ensimag ou votre machine personnelle).

\subsection{Création des répertoires de travail}

On va maintenant créer le premier répertoire de travail. Pour
l'instant, il n'y a aucun fichier dans notre dépôt, donc la première
chose à faire sera d'y ajouter les fichiers sur lesquels on veut
travailler. Dans notre exemple, c'est {\it Alice} qui va s'en occuper.

Pour créer un répertoire de travail dans le répertoire
\verb|~/projetc| (qui n'existe pas encore), {\it Alice} entre donc les
commandes :

\begin{verbatim}
cd
git clone ssh://alice@depots.ensimag.fr/depots/$annee/alice-et-bob/projetc.git projetc
\end{verbatim}

Pour l'instant, ce répertoire est vide, ou presque : il contient un
répertoire caché \texttt{.git/} qui contient les méta-données utiles à
Git (c'est là que sera stocké l'historique du projet).

Pour cette séance machine, un répertoire \texttt{sandbox/} a été prévu
pour vous, pour pouvoir vous entraîner sans casser un vrai projet.
{\it Alice} télécharge le dépôt depuis
\url{https://ensiwiki.ensimag.fr/index.php/Fichier:Sandbox.tar.gz}
puis importe ce répertoire :

\begin{verbatim}
cd ~/projetc/
tar xzvf ~/chemin/vers/le/repertoire/Sandbox.tar.gz
git add sandbox/
git commit -a -m "import du repertoire sandbox/"
\end{verbatim}

La commande « \texttt{git add sandbox/} » dit à Git de « traquer »
tous les fichiers du répertoire \texttt{sandbox/}, c'est à dire qu'il
va enregistrer le contenu de ces fichiers, et suivre leur historique
ensuite. La commande \texttt{git commit} enregistre effectivement le
contenu de ces fichiers.

{\it Alice} peut maintenant envoyer le squelette qui vient d'être importé
vers le dépôt partagé :

\begin{verbatim}
git push
\end{verbatim}

Tout est prêt pour commencer à travailler. {\it Bob} peut à son tour
récupérer sa copie de travail :

\begin{verbatim}
cd
git clone ssh://bob@depots.ensimag.fr/depots/$annee/alice-et-bob/projetc.git projetc
cd projetc
ls
\end{verbatim}

Attention, {\it Bob} utilise bien son nom d'utilisateur
\texttt{depots.ensimag.fr} dans la
première partie de l'URL (i.e. dans \texttt{bob@depots.ensimag.fr}).
Vu que {\it Bob} ne connaît pas le mot de passe d'{\it Alice},
son login \texttt{bob} sur \texttt{depots.ensimag.fr} est le seul moyen pour lui
de se connecter à cette machine.

Si tout s'est bien passé, la commande \texttt{ls} ci-dessus devrait
faire apparaître le répertoire \texttt{sandbox/}.

\section{Utilisation de Git pour le développement}
\label{sec=utilisation}

Pour commencer, on va travailler dans le répertoire {\tt sandbox}, qui
contient deux fichiers pour s'entraîner :

\begin{verbatim}
cd sandbox
emacs hello.c
\end{verbatim}

Il y a deux problèmes avec
{\tt hello.c} (identifiés par des commentaires). {\it Alice} résout l'un des
problème, et {\it Bob} choisit l'autre. Par ailleurs, chacun ajoute son nom
en haut du fichier, et enregistre le résultat.

\subsection{Création de nouvelles révision}

\begin{verbatim}
git status                  # comparaison du répertoire de
                            # travail et du dépôt.
\end{verbatim}
On voit apparaître :
\begin{verbatim}
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

      modified:   hello.c

\end{verbatim}
Ce qui nous intéresse ici est la ligne « modified: hello.c » (la
distinction entre « Changes not staged for commit » et « Changes to be
committed » n'est pas importante pour l'instant), qui signifie que
vous avez modifié {\tt hello.c}, et que ces modifications n'ont
pas été enregistrées dans le dépôt. On peut vérifier plus précisément
ce qu'on vient de faire :
\begin{verbatim}
git diff HEAD
\end{verbatim}
Comme {\em Alice} et {\em Bob} ont fait des modifications différentes, le diff
affiché sera différent, mais ressemblera dans les deux cas à :
\begin{verbatim}
diff --git a/sandbox/hello.c b/sandbox/hello.c
index a47665a..7f67d33 100644
--- a/sandbox/hello.c
+++ b/sandbox/hello.c
@@ -1,5 +1,5 @@
 /* Chacun ajoute son nom ici */
-/* Auteurs : ... et ... */
+/* Auteurs : Alice et ... */
 
 #include <stdio.h>
 
\end{verbatim}
Les lignes commençant par '-' correspondent à ce qui a été enlevé, et
les lignes commençant par '+' à ce qui a été ajouté par rapport au
précédent commit. Si vous avez suivi les consignes ci-dessus à propos
du fichier {\tt .gitconfig}, vous devriez avoir les lignes supprimées
en rouge et les ajoutées en vert.

Maintenant, {\em Alice} et {\em Bob} font :
\begin{verbatim}
git commit -a      # Enregistrement de l'état courant de
                   # l'arbre de travail dans le dépôt local.
\end{verbatim}
L'éditeur est lancé et demande d'entrer un message de 'log'. Ajouter
des lignes et d'autres renseignements sur les modifications apportées à
hello.c (on voit en bas la liste des fichiers modifiés). Un bon
message de log commence par une ligne décrivant rapidement le
changement, suivi d'une ligne vide, suivi d'un court texte expliquant
pourquoi la modification est bonne.

On voit ensuite apparaître :
\begin{verbatim}
[master 2483c22] Ajout de mon nom
 1 files changed, 2 insertions(+), 12 deletions(-)
\end{verbatim}
Ceci signifie qu'un nouveau « commit » (qu'on appelle aussi parfois
« revision » ou « version ») du projet a été enregistrée dans le dépôt.
Ce commit est identifié par une chaîne hexadécimale (« 2483c22 » dans
notre cas).

On peut visualiser ce qui s'est passé avec les commandes
\begin{verbatim}
gitk            # Visualiser l'historique graphiquement
\end{verbatim}
et
\begin{verbatim}
git gui blame hello.c     # voir l'historique de chaque 
                          # ligne du fichier hello.c
\end{verbatim}

On va maintenant mettre ce « commit » à disposition des
autres utilisateurs.

\subsection{Fusion de révisions (merge)}

SEULEMENT {\em Bob} fait :
\begin{verbatim}
git push           # Envoyer les commits locaux dans
                   # le dépôt partagé
\end{verbatim}
Pour voir où on en est, les deux équipes peuvent lancer la commande :
\begin{verbatim}
gitk               # afficher l'historique sous forme graphique
\end{verbatim}
ou bien
\begin{verbatim}
git log            # afficher l'historique sous forme textuelle.
\end{verbatim}
À PRESENT, {\em Alice} peut tenter d'envoyer ses modifications :
\begin{verbatim}
git push
\end{verbatim}
On voit apparaître :
\begin{verbatim}
To ssh://alice@depots.ensimag.fr/depots/$annee/alice-et-bob/projetc.git/
 ! [rejected]        master -> master (non-fast forward)
error: failed to push some refs to
'ssh://alice@depots.ensimag.fr/depots/$annee/alice-et-bob/projetc.git/'
To prevent you from losing history, non-fast-forward updates were rejected
Merge the remote changes (e.g. 'git pull') before pushing again.  See the
'Note about fast-forwards' section of 'git push --help' for details.
\end{verbatim}

L'expression « non-fast-forward » (qu'on pourrait traduire par « absence
d'avance rapide ») veut dire qu'il y a des modifications dans le dépôt
vers laquelle on veut envoyer nos modifications et que nous n'avons
pas encore récupérées. Il faut donc fusionner les modifications avant
de continuer.

L'utilisateur {\em Alice} fait donc :
\begin{verbatim}
git pull
\end{verbatim}
Après quelques messages sur l'avancement de l'opération, on voit
apparaître :
\begin{verbatim}
Auto-merging sandbox/hello.c
CONFLICT (content): Merge conflict in sandbox/hello.c
Automatic merge failed; fix conflicts and then commit the result.
\end{verbatim}
Ce qui vient de se passer est que {\em Bob} et {\em Alice} ont fait
des modifications au même endroit du même fichier dans les commits
qu'ils ont fait chacun de leur côté (en ajoutant leurs noms sur la
même ligne), et Git ne sait pas quelle version choisir pendant la
fusion : c'est un conflit, et nous allons devoir le résoudre
manuellement. Allez voir {\tt hello.c}.

La bonne nouvelle, c'est que les modifications faites par {\it Alice} et Bob
sur des endroits différents du fichier ont été fusionnés. Quand une
équipe est bien organisée et évite de modifier les mêmes endroits en
même temps, ce cas est le plus courant : les développeurs font les
modifications, et le gestionnaire de versions fait les fusions
automatiquement.

En haut du fichier, on trouve :
\begin{verbatim}
<<<<<<< HEAD
/* Auteurs : Alice et ... */
=======
/* Auteurs : ... et Bob */
>>>>>>> 2483c228b1108e74c8ca4f7ca52575902526d42a
\end{verbatim}

Les lignes entre \verb|<<<<<<<| et \verb|=======| contiennent la
version de votre commit (qui s'appelle HEAD). les lignes entre
\verb|=======| et \verb|>>>>>>>| contiennent la version que nous
venons de récupérer par « pull » (nous avions dit qu'il était identifié
par la chaîne 2483c22, en fait, l'identifiant complet est plus long,
nous le voyons ici).

Il faut alors « choisir » dans {\tt hello.c} la version qui convient
(ou même la modifier). Ici, on va fusionner à la main (i.e. avec un
éditeur de texte) et remplacer l'ensemble par ceci :

\begin{verbatim}
/* Auteurs : Alice et Bob */
\end{verbatim}

Si {\em Alice} fait à nouveau
\begin{verbatim}
git status
\end{verbatim}
On voit apparaître :
\begin{verbatim}
On branch master
Your branch and 'origin/master' have diverged,
and have 1 and 1 different commit(s) each, respectively.

Unmerged paths:
  (use "git add/rm <file>..." as appropriate to mark resolution)

      both modified:      hello.c

no changes added to commit (use "git add" and/or "git commit -a")
\end{verbatim}
Si on n'est pas sur de soi après la résolution des conflits, on peut
lancer la commande :
\begin{verbatim}
git diff    # git diff sans argument, alors qu'on avait
            # l'habitude d'appeler 'git diff HEAD'
\end{verbatim}
Après un conflit, Git affichera quelque chose comme :
\begin{verbatim}
diff --cc hello.c
index 5513e89,614e4b9..0000000
--- a/hello.c
+++ b/hello.c
@@@ -1,5 -1,5 +1,5 @@@
  /* Chacun ajoute son nom ici */
- /* Auteurs : Alice et ... */
 -/* Auteurs : ... et Bob */
++/* Auteurs : Alice et Bob */
  
  #include <stdio.h>

\end{verbatim}
(les '+' et les '-' sont répartis sur deux colonnes, ce qui correspond
aux changements par rapport aux deux « commits » qu'on est en train de
fusionner. Si vous ne comprenez pas ceci, ce n'est pas très grave !)

Après avoir résolu manuellement les conflits à l'intérieur du fichier,
on marque ces conflits comme résolus, explicitement, avec {\tt git
  add} :

\begin{verbatim}
$ git add hello.c
$ git status
On branch master
Your branch and 'origin/master' have diverged,
and have 1 and 1 different commit(s) each, respectively.

Changes to be committed:

      modified:   hello.c

\end{verbatim}

On note que {\tt hello.c} n'est plus considéré « both modified »
(i.e. contient des conflits non-résolus) par Git, mais simplement
comme « modified ».

Quand il n'y a plus de fichier en conflit, il faut faire un commit
(comme « git pull » nous l'avait demandé) :
\begin{verbatim}
git commit
\end{verbatim}
(Dans ce cas, il est conseillé, même pour un débutant, de ne pas
utiliser l'option \verb|-a|, mais c'est un détail)

Un éditeur s'ouvre, et propose un message de commit du type « {\tt Merge
branch 'master' of ...} », on peut le laisser tel quel, sauver et
quitter l'éditeur.

Nb : si il n'y avait pas eu de conflit, ce qui est le cas le plus
courant, « git pull » aurait fait tout cela : télécharger le nouveau
commit, faire la fusion automatique, et créer si besoin un nouveau
commit correspondant à la fusion.

On peut maintenant regarder plus en détails ce qu'il s'est passé :
\begin{verbatim}
gitk
\end{verbatim}
Pour {\it Alice}, on voit apparaître les deux « commit » fait par {\em Bob} et
{\em Alice} en parallèle, puis le « merge commit » que nous venons de créer
avec « git pull ». Pour {\em Bob}, rien n'a changé.

La fusion étant faite, {\em Alice} peut mettre à disposition son travail
(le premier commit, manuel, et le commit de fusion) avec :
\begin{verbatim}
git push
\end{verbatim}
et {\em Bob} peut récupérer le tout avec :
\begin{verbatim}
git pull
\end{verbatim}
(cette fois-ci, aucun conflit, tout se passe très rapidement et en une
commande)

Les deux utilisateurs peuvent comparer ce qu'ils ont avec :
\begin{verbatim}
gitk
\end{verbatim}
ils ont complètement synchronisé leur répertoires. On peut également
faire :
\begin{verbatim}
git pull
git push
\end{verbatim}
Mais ces commandes se contenterons de répondre {\tt Already up-to-date.}
et {\tt Everything up-to-date}.

\subsection{Ajout de fichiers}

À present, {\em Alice} crée un nouveau fichier, {\tt toto.c},
avec un contenu quelconque.

{\em Alice} fait
\begin{verbatim}
git status
\end{verbatim}
On voit apparaître :
\begin{verbatim}
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)

      toto.c
nothing added to commit but untracked files present (use "git add" to track)
\end{verbatim}
Notre fichier {\tt toto.c} est considéré comme « Untracked » (non suivi
par Git). Si on veut que {\tt toto.c} soit ajouté au dépôt, il faut
l'enregistrer ({\tt git commit} ne suffit pas) : {\tt git add toto.c}

{\em Alice} fait à present :
\begin{verbatim}
git status
\end{verbatim}
On voit apparaître :
\begin{verbatim}
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

      new file:   toto.c

\end{verbatim}
{\em Alice} fait à présent (-m permet de donner directement le message
de log) :
\begin{verbatim}
git commit -m "ajout de toto.c"
\end{verbatim}
On voit apparaître :
\begin{verbatim}
[master b1d56e6] Ajout de toto.c
 1 files changed, 4 insertions(+), 0 deletions(-)
 create mode 100644 toto.c
\end{verbatim}
{\tt toto.c} a été enregistré dans le dépôt. On peut publier ce
changement :
\begin{verbatim}
git push
\end{verbatim}
{\em Bob} fait à présent :
\begin{verbatim}
git pull
\end{verbatim}
Après quelques messages informatifs, on voit apparaître :
\begin{verbatim}
Fast forward
 toto.c |    4 ++++
 1 files changed, 4 insertions(+), 0 deletions(-)
 create mode 100644 toto.c
\end{verbatim}
Le fichier {\tt toto.c} est maintenant présent chez {\em Bob}.

\subsection{Fichiers ignorés par Git}

{\em Bob} crée à présent un nouveau fichier {\tt temp-file.txt}, puis
fait :
\begin{verbatim}
git status
\end{verbatim}
On voit maintenant apparaître :
\begin{verbatim}
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)

      temp-file.txt

nothing added to commit but untracked files present (use "git add" to track)
\end{verbatim}
Si {\em Bob} souhaite que le fichier {\tt temp-file.txt} ne soit pas enregistré
dans le dépôt (soit « ignoré » par Git), il doit placer son nom dans un
fichier {\tt .gitignore} dans le répertoire contenant {\tt temp-file.txt}.
Concretement, {\em Bob} tappe la commande
\begin{verbatim}
emacs .gitignore
\end{verbatim}
et ajoute une ligne
\begin{verbatim}
temp-file.txt
\end{verbatim}
puis sauve et quitte.

Dans le répertoire \texttt{sandbox/} qui vous est fourni, il existe
déjà un fichier \texttt{.gitignore} qui peut vous servir de base pour
vos projets.

Si {\em Bob} souhaite créer un nouveau \texttt{.gitignore} (par
exemple, à la racine du projet pour que les règles s'appliquent sur
tout le projet), pour que tous les utilisateurs du dépôt
bénéficient du même fichier {\tt .gitignore}, {\em Bob} fait :
\begin{verbatim}
git add .gitignore
\end{verbatim}
{\em Bob} fait a nouveau
\begin{verbatim}
git status
\end{verbatim}
On voit apparaître :
\begin{verbatim}
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

      new file:   .gitignore

\end{verbatim}
Quelques remarques :

\begin{itemize}
\item Le fichier {\tt temp-file.txt} n'apparaît plus. C'était le but
  de la manoeuvre. Une bonne pratique est de faire en sorte que « git
  status » ne montre jamais de « Untracked files » : soit un fichier
  doit être ajouté dans le dépôt, soit il doit être explicitement
  ignoré. Cela évite d'oublier de faire un « git add ».
\item En général, on met dans les {\tt .gitignore} les fichiers
  générés (*.o, fichiers exécutables, ...), ce qui est en partie fait
  pour vous dans le \texttt{.gitignore} du répertoire
  \texttt{sandbox/} (qu'il faudra adapter pour faire le
  \texttt{.gitignore} de votre projet). Les « wildcards » usuels
  (\texttt{*.o}, \texttt{*.ad?}, ...) sont acceptés pour ignorer
  plusieurs fichiers.
\item Le fichier {\tt .gitignore} vient d'être ajouté (ou bien il est
  modifié si il était déjà présent). Il faut à nouveau faire un commit
  et un push pour que cette modification soit disponible pour tout le
  monde.
\end{itemize}

\section{Pour conclure\dots{}}

Bien sûr, Git est bien plus que ce que nous venons
de voir, et nous encourageons les plus curieux à se plonger dans le
manuel utilisateur et les pages de man de Git pour en apprendre plus.
Au niveau débutant, voici ce qu'on peut retenir :

\section*{Les commandes}
\begin{description}
 \item[git commit -a] enregistre l'état courant du répertoire de travail,
 \item[git push] publie les commits,
 \item[git pull] récupère les commits publiés,
 \item[git add, git rm et git mv] permettent de dire à Git quels
   fichiers il doit surveiller (``traquer'' ou ``versionner'' dans le
   jargon),
 \item[git status, git diff HEAD] pour voir où on en est.
\end{description}

\section*{Conseils pratiques}
\begin{itemize}
\item Ne \emph{jamais} s'échanger des fichiers sans passer par Git
  (email, scp, clé USB), sauf si vous savez \emph{vraiment} ce que
  vous faites.
\item Toujours utiliser {\tt git commit} avec l'option {\tt -a}.
\item Faire un {\tt git push} après chaque {\tt git commit -a}, sauf si on
  veut garder ses modifications privées. Il peut être nécessaire de
  faire un {\tt git pull} avant un {\tt git push} si des nouvelles
  révisions sont disponibles dans le dépôt partagé.
\item Faire des {\tt git pull} régulièrement pour rester synchronisés
  avec vos collègues. Il faut faire un {\tt git commit -a} avant de
  pouvoir faire un {\tt git pull} (ce qui permet de ne pas mélanger
  modifications manuelles et fusions automatiques).
\item Ne faites jamais un « {\tt git add} » sur un fichier binaire
  généré : si vous les faites, attendez-vous à des conflits à chaque
  modification des sources ! Git est fait pour gérer des fichiers
  sources, pas des binaires.
\end{itemize}
(quand vous ne serez plus débutants\footnote{cf. par exemple
  \url{http://ensiwiki.ensimag.fr/index.php/Maintenir_un_historique_propre_avec_Git}},
vous verrez que la vie n'est pas
si simple, et que la puissance de Git vient de {\tt git commit} sans
{\tt -a}, des {\tt git commit} sans {\tt git push}, ... mais chaque
chose en son temps !)

\section*{Quand rien ne va plus ...}

En cas de problème avec l'utilisation de Git

\begin{itemize}
\item Consulter la page
  \url{http://ensiwiki.ensimag.fr/index.php/FAQ_Git} sur EnsiWiki.
  cette page a été écrite pour le projet GL, mais la plupart des
  explications s'appliquent directement pour vous,
\item Demander de l'aide aux enseignants,
\item Demander de l'aide sur la mailing-list de Git,
% \item En cas de problème non-résolu et bloquant, poser la question
%   par email à un enseignant (par exemple Matthieu Moy).
\end{itemize}

Dans tous les cas, lire la documentation est également une bonne
idée : \url{http://git-scm.com/documentation} ! Par exemple, le livre
numérique de Scott Chacon « Pro Git », simple d'accès et traduit en
français : \url{http://git-scm.com/book/fr/v2}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "francais"
%%% End:
