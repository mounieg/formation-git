all: seance-machine-git.pdf sandbox.tar.gz

sandbox.tar.gz: sandbox/.gitignore sandbox/hello.c sandbox/Makefile
	tar czvf $@ $^

include LaTeX.mk

Slides/%.pdf: force
	cd Slides && $(MAKE) $*.pdf

install: seance-machine-git.pdf Slides/git-slides.pdf Slides/git-handout.pdf \
	 Slides/configuring-git-slides.pdf Slides/configuring-git-handout.pdf \
	 Slides/advanced-git-slides.pdf Slides/advanced-git-handout.pdf
	mkdir -p ~/WWW/cours/formation-git/
	cp $^ ~/WWW/cours/formation-git/
	cd ~/WWW/cours/formation-git/ && htmllist.sh

.PHONY: force
force:
