/* Chacun ajoute son nom ici */
/* Auteurs : ... et ... */

#include <stdio.h>

int main (void) {
	/* Oups, on a oublié le \n final. */
	printf("Hello, world");

	printf("Je calcule la réponse ... ");
	
	/* Oups, on s'est trompé, pour afficher un nombre, c'est
	   plutôt %d */
	printf("La réponse est %s\n", 42);
}
