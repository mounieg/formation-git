\documentclass[a4paper,12pt]{scrartcl}

\usepackage[frenchb]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\DeclareUnicodeCharacter{00A0}{~}
\usepackage[left=2.5cm,right=2.5cm,top=2.5cm,bottom=2.5cm]{geometry}
\usepackage{microtype}
\usepackage{menukeys}


\usepackage{url}
\usepackage{hyperref}
\hypersetup{
  colorlinks,
  linkcolor={black},%{red!50!black},
  citecolor={blue!50!black},                        
  urlcolor={blue!80!black},
  linktoc=all
}
\usepackage{tikz}
\usetikzlibrary{shapes}
\usetikzlibrary{calc,fadings,shapes.arrows,shadows,backgrounds, positioning}
% il exite mainteannt gitdags pour générer du tikz !
% \usepackage{gitdags}

\usepackage{fancyheadings}
\usepackage{minted}
\usepackage{todonotes}

\date{2020-2021}
\author{Matthieu Moy, Grégory Mounié}
\title{\vspace{1cm}Git basics\\---\\MOSIG M1}

\sloppy

\usemintedstyle{tango}
\setminted{breaklines}
\setminted{linenos=true}
\definecolor{verylightgray}{rgb}{0.95,0.95,0.95}
\setminted{bgcolor=verylightgray}

\begin{document}
\maketitle
\thispagestyle{fancy}
\fancyhead[HL]{\includegraphics[height=2cm]{logo_UGA_couleur_cmjn.jpg}}
\fancyhead[HC]{\includegraphics[height=2cm]{ensimag.jpg}}
\fancyhead[HR]{\includegraphics[width=4.5cm]{GINP_BlocLogo.png}}

This document is available here~:\\
\begin{center}
  \url{http://systemes.pages.ensimag.fr/www-git/git-computer-lab.pdf}
\end{center}

An extended self-content french version of this document is available here~:\\
\url{http://systemes.pages.ensimag.fr/www-unix/avance/seance1-git/seance-machine-git.pdf}.

\section{Git and Version Control System}

Git is a Version Control System, that is a software designed to manage
the changes to textual documents like the source code of programs.

In the English version of this document, we often refer to
some chapters of the web version of {\em Pro Git Book}
at \url{https://git-scm.com/book/en/v2}. This book is freely available in
electronic form (Creative Common NC SA 3.0).

For a good introduction to VCS then to Git, read now
\href{https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control}{Chapter 1.1}
and 
\href{https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F}{Chapter 1.3}


Numerous VCS, \href{https://en.wikipedia.org/wiki/Comparison_of_version-control_software}{listed in Wikipedia}, exist,
but Git is the most commonly used, now.


For the following of sections of this document, we use a {\em
  Gitlab server}. Gitlab has similar functionalities to GitHub. But, it is
a free software, and thus many institutions deploy it.  If you have a
computer account at Ensimag, you use
\url{https://gitlab.ensimag.fr}. If you have a computer account at
UGA, you may also use
\url{https://gricad-gitlab.univ-grenoble-alpes.fr}. Finally, you may
open an account, and use \url{https://gitlab.com}.


\subsection{Tracks}
\begin{description}
\item[Git newbies] group yourself in small teams of at least two
  people, with two computers close to each other.
\item[Git experts] group yourself in small teams and do some
  others practical exercises of the web page of this lecture.
\end{description}
\subsection{Team organization}

A team will use two Linux PC close to each other (using your own
laptop is perfectly fine). The gitlab server is freely accessible at
any time from any places. The git repository of the team is assumed
to be in \texttt{gitlab.ensimag.fr}.


Let us assume that the team is composed of two people, \textit{Alice}
on \texttt{laptop1} and \textit{Bob} on \texttt{laptop2}. Explanations
may be extended to any number of team members.


\tikzstyle{depot}=[draw=black,shape=ellipse]
\begin{center}
\begin{tikzpicture}
  \draw node[depot] (shared) {
    \begin{tabular}{c}
Shared repository\\
\texttt{gitlab.ensimag.fr}
\end{tabular}
};
  \draw (shared) ++(-3, -2.5) node[depot] (alice) {
    \begin{tabular}{c}
Working repository of\\
\textit{Alice}\\
\texttt{alice@laptop1}
\end{tabular}
};
  \draw (shared) ++(+3, -2.5) node[depot] (bob) {
    \begin{tabular}{c}
Working repository\\
of \textit{Bob}\\
\texttt{bob@laptop2}
\end{tabular}
};

\draw[<->,>=latex,thick] (shared) -- (alice);
\draw[<->,>=latex,thick] (shared) -- (bob);
\end{tikzpicture}
\end{center}

\section{ssh configuration}

Gitlab may used the \texttt{https} protocol, or the \texttt{ssh}
protocol.

Using the \texttt{ssh} protocol, you avoid typing your password
at every Git transfer.

You need first to create your own public-private pair of ssh keys.
Read now \href{https://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key}{the Chapter 4.3}

\begin{itemize}
\item If you do not have one yet, create a public-private key pair with
  \texttt{ssh-keygen}. Choose a good passphrase. A sentence, or few
  words, that you will never forget, are perfectly fine. The good
  practice is to create a different key pair per computer, or computer
  account.

  Thus, now, you should have two files \texttt{id\_rsa.pub} (public
  part) and \texttt{id\_rsa} (private part) in your directory
  \texttt{\textasciitilde/.ssh}.

\item Check if you have an already running ssh-agent. Any modern Linux
  graphical session should have it by default.
\begin{minted}{bash}
petrot@chene[:08b09e6b]% env | grep SSH_AGENT_PID
SSH_AGENT_PID=27487
\end{minted}

Otherwise, start a fresh new ssh-agent in your terminal.
\begin{minted}{bash}
petrot@chene[:08b09e6b]% eval $(ssh-agent)
Agent pid 27487
\end{minted}

\item Give your passphrase to the agent~:
\begin{minted}{bash}
petrot@chene[:08b09e6b]% ssh-add
Enter passphrase for /home/petrot/.ssh/id_rsa:
\end{minted}

\item Copy the public key, the content of \texttt{id\_rsa.pub}, in your
  account on \url{https://gitlab.ensimag.fr}. In your \emph{User
    Settings} (Menu top right, third entry) you find a section
  \emph{SSH Keys} (Menu on the left). Copy-paste the public key in the
  form. Choose a name and click on \texttt{Add key}.
\end{itemize}

\section{Git local configuration}

On your own computer, to use Git, you will have to install it
first. Under Ubuntu or Debian « \texttt{apt-get install git gitk} » ou
« \texttt{apt-get install git-core gitk} ».

Details on the Git configuration are presented in
\href{https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration}{Chapter 8.1}.

Create or edit the \texttt{\textasciitilde/.gitconfig} file: 
\begin{minted}{bash}
emacs ~/.gitconfig   # or use your favorite editor
\end{minted}
Add, in \texttt{.gitconfig}, at least, the following content:

\begin{minted}{ini}
[core]
# choose a single editor line :
        editor = atom --wait
        editor = gedit -s
        editor = emacs
        editor = emacsclient -c
        editor = vim
        editor = gvim -f
[user]
        name = First_Name Last_Name
        email = First_Name.Last_name@univ-grenoble-alpes.fr
[push]
        default = simple
[color]
        diff = auto
        status = auto
        branch = auto
        interactive = auto
        ui = true
        pager = true
\end{minted}

Beware of "modern" editor already running when committing. If the editor command finishes early, Git will display 
\begin{verbatim}
Aborting commit due to empty commit message.
\end{verbatim}
and nothing will be registered.

The section \texttt{[push]} is only there to homogenize the behavior of old Git version.

\section{Starting a new project}

This section is done only once per project, thus only one team member
should do it.

\subsection{Shared repository creation in Gitlab}
\label{sec=creation-depot-partagee}

\textit{Alice} connects to her account on \texttt{gitlab.ensimag.fr}:

\begin{minted}{console}
alice@laptop1$ firefox gitlab.ensimag.fr
\end{minted}

\subsubsection{Group creation (team members)}

This step is not mandatory but it helps to manage access right on the
projects.

Using the menu/button « + », \textit{Alice} creates a group (\emph{New
  group}) with the logins of the team members as name, like
\verb!alice_bob!. This group should have a \emph{Private} visibility,
the default in Gitlab.

Using the « Groups » button, \textit{Alice} add \textit{Bob} as team
member: left menu \emph{Members}, then add Bob in \emph{Add new
  member}. \textit{Bob} has the same role as \textit{Alice}:
\emph{Owner}.

Note: \textit{Bob} may have to connect at least once to Gitlab before
being added to the team.
\begin{minted}{console}
bob@laptop2$ firefox gitlab.ensimag.fr
\end{minted}

\subsubsection{Repository creation}

Using the menu/button « + », Alice creates an empty project (\emph{New project}). The project name is \texttt{project1}, under the group \verb!alice_bob!.

Now, Alice and Bob work directly in their HOME. 

The figure \ref{fig:git} presents the typical exchanges and states of the files.
\footnote{\url{https://git-scm.com/book/en/v2/Getting-Started-Git-Basics}\\
Git has three main states that your files can reside in: committed, modified, and staged:
\begin{itemize}
	\item Committed means that the data is safely stored in your local database.
	\item Modified means that you have changed the file but have not committed it to your database yet.
	\item Staged means that you have marked a modified file in its current version to go into your next commit snapshot.
\end{itemize}
}

\begin{figure}[H]
  \centering
  \input{git-sequence.tikz}
  \caption{Working directory, staging area (index) and repository}
  \label{fig:git}
\end{figure}



\subsection{Working repository creation}

As the project is empty, there is no file in it. \textit{Alice} adds
the first files as the first step.

\textit{Alice} clones the remote repository to create her working
directory \verb|~/project1|:

{\small
\begin{minted}{console}
alice@laptop1$ git clone ssh://git@gitlab.ensimag.fr/alice_bob/project1.git project1
\end{minted}
}

The new directory is almost empty. It contains just a hidden
subdirectory \texttt{.git/} with the Git meta-data and file history.

Alice downloads the directory \texttt{sandbox/} (at
\url{https://systemes.gricad-pages.univ-grenoble-alpes.fr/www-unix/avance/seance3-git/sandbox.tar.gz}),
extracts it and adds it to the Git history.

\begin{minted}{console}
alice@laptop1$ wget https://systemes.gricad-pages.univ-grenoble-alpes.fr/www-unix/avance/seance3-git/sandbox.tar.gz
alice@laptop1$ cd ~/project1/
alice@laptop1$ tar xzvf ../sandbox.tar.gz
alice@laptop1$ git status
alice@laptop1$ git add sandbox/
alice@laptop1$ git status
alice@laptop1$ git commit -m "add sandbox/ directory"
alice@laptop1$ git status
\end{minted}

\textit{Alice} has done her first commit !  Explanations can be found
in
\href{https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository}{Chapter 2.2}

Now \textit{Alice} sends the commit to the shared repository:

\begin{minted}{console}
alice@laptop1$ git push
\end{minted}

Now \textit{Bob} gets his own working repository:

{\small
\begin{minted}{console}
bob@laptop2$ cd
bob@laptop2$ git clone ssh://git@gitlab.ensimag.fr/alice_bob/project1.git project1
bob@laptop2$ cd project1
bob@laptop2$ git status
bob@laptop2$ ls
\end{minted}
}

\textit{Bob} repository should have a \texttt{sandbox/} directory.

\section{Git first usage}
\label{sec=utilisation}

The \texttt{sandbox/} directory contains two files:

\begin{minted}{bash}
cd sandbox
emacs hello.c
\end{minted}

Two problems are present in the file \texttt{hello.c}. \textit{Alice}
solve one of the problem, et \textit{Bob} solve the other one. Both
add their name at the top and save the file.

\subsection{Commit creation}

The command:
\begin{minted}{bash}
git status                  # comparaison of working directory and history
\end{minted}
should display:
\begin{verbatim}
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

      modified:   hello.c

\end{verbatim}

\texttt{hello.c} is modified and the modifications are not yet
registered in the local history.

{\em Alice} and {\em Bob} look at the details of the differences
between the current state of the file and its last state in history:
\begin{minted}{bash}
git diff HEAD
\end{minted}


In both cases, the {\em diff} should look like this:

\begin{verbatim}
diff --git a/sandbox/hello.c b/sandbox/hello.c
index a47665a..7f67d33 100644
--- a/sandbox/hello.c
+++ b/sandbox/hello.c
@@ -1,5 +1,5 @@
 /* Chacun ajoute son nom ici */
-/* Auteurs : ... et ... */
+/* Auteurs : Alice et ... */
 
 #include <stdio.h>
 
\end{verbatim}
Lines starting with '-' are removed from previous commit.
Lines starting with '+' are added from previous commit.

Now, both, {\em Alice} and {\em Bob} create a new commit~:
\begin{minted}{bash}
git commit -a      # register all modifications of the local repository
\end{minted}

The editor starts. They have to write a log message. A good log message
start with a title line, then a blank line, then a short text. The log
message should explain the reason of the modification (Why).

After saving the message and quitting the editor, they read in their terminal:
\begin{verbatim}
[master 2483c22] Add my name
 1 files changed, 2 insertions(+), 12 deletions(-)
\end{verbatim}

A new « commit » is registered. The hexadecimal number "2483c22" is
the identification number of the commit.

Some commands are dedicated to the history visualization:
\begin{minted}{bash}
gitk            # Graphical history
\end{minted}

\begin{minted}{bash}
git log --oneline --graph   # textual history
\end{minted}
and
\begin{minted}{bash}
git gui blame hello.c     # history of each line
\end{minted}

The "commit" will be published to any user in the next section.

More on commit in \href{https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository}{Chapter 2.2}

\subsection{Merge}

First, \textbf{ONLY} {\em Bob} publishes its commit~:
\begin{minted}{console}
bob@laptop2$ git push           # send the Bob commit to shared repository
\end{minted}

{\em Alice} and {\em Bob} should follow changes in the local history with:
%Pour voir où on en est, les deux équipes peuvent lancer la commande~:
\begin{minted}{bash}
gitk               # graphical history
\end{minted}
or with:
\begin{minted}{bash}
git log            # textual history.
\end{minted}

Second, {\em Alice} attempts to publish her modification:
\begin{minted}{console}
alice@laptop1$ git push
\end{minted}
The publication should fail:
\begin{verbatim}
To ssh://git@gitlab.ensimag.fr/alice_bob/project1.git
 ! [rejected]        master -> master (non-fast forward)
error: failed to push some refs to
'ssh://git@gitlab.ensimag.fr/alice_bob/project1.git'
To prevent you from losing history, non-fast-forward updates were rejected
Merge the remote changes (e.g. 'git pull') before pushing again.  See the
'Note about fast-forwards' section of 'git push --help' for details.
\end{verbatim}


{\em Alice} has first to merge {\em Bob} modifications already published.

{\em Alice} does the merging of the version:
\begin{minted}{console}
alice@laptop1$ git pull
\end{minted}

{\em Alice} and {\em Bob} have changed the same lines, thus the
automatic merge should fail.

\begin{verbatim}
Auto-merging sandbox/hello.c
CONFLICT (content): Merge conflict in sandbox/hello.c
Automatic merge failed; fix conflicts and then commit the result.
\end{verbatim}

The good news: the modifications at different lines are already merged.

% La bonne nouvelle, c'est que les modifications faites par \textit{Alice} et Bob
% sur des endroits différents du fichier ont été fusionnées. Quand une
% équipe est bien organisée et évite de modifier les mêmes endroits en
% même temps, ce cas est le plus courant~: les développeurs font les
% modifications, et le gestionnaire de versions fait les fusions
% automatiquement.
The bad news: {\em Alice} has to set \texttt{hello.c} to the correct version by
hand. Hopefully, problematic lines are clearly indicated in the file.
\begin{minted}{diff}
<<<<<<< HEAD
/* Auteurs : Alice et ... */
=======
/* Auteurs : ... et Bob */
>>>>>>> 2483c228b1108e74c8ca4f7ca52575902526d42a
\end{minted}

{\em Alice} has to remove indicators to put the file in the desired state:
\begin{verbatim}
/* Auteurs : Alice et Bob */
\end{verbatim}

Then
\begin{minted}{console}
alice@laptop1$ git status
\end{minted}
should display:
\begin{verbatim}
On branch master
Your branch and 'origin/master' have diverged,
and have 1 and 1 different commit(s) each, respectively.

Unmerged paths:
  (use "git add/rm <file>..." as appropriate to mark resolution)

      both modified:      hello.c

no changes added to commit (use "git add" and/or "git commit -a")
\end{verbatim}

To be sure of the results, {\em Alice} checks the differences:
\begin{minted}{bash}
git diff  # and not 'git diff HEAD'
\end{minted}
The result looks like:
\begin{verbatim}
diff --cc hello.c
index 5513e89,614e4b9..0000000
--- a/hello.c
+++ b/hello.c
@@@ -1,5 -1,5 +1,5 @@@
  /* Chacun ajoute son nom ici */
- /* Auteurs : Alice et ... */
 -/* Auteurs : ... et Bob */
++/* Auteurs : Alice et Bob */
  
  #include <stdio.h>

\end{verbatim}

{\em Alice} mark the conflicts as solved, using \texttt{git add}:
\begin{verbatim}
$ git add hello.c
$ git status
On branch master
Your branch and 'origin/master' have diverged,
and have 1 and 1 different commit(s) each, respectively.

Changes to be committed:

      modified:   hello.c

\end{verbatim}


Then, {\em Alice} finishes building the commit of the merging:  
\begin{minted}{bash}
git commit
\end{minted}
The default message is sufficient. {\em Alice} \/saves it and quits the editor.

Looking at the details:
\begin{minted}{bash}
gitk
\end{minted}

{\em Alice}'s logs show 3 commits: the two modifications and the merge. 

{\em Alice} publishes her repository state:
\begin{minted}{console}
alice@laptop1$ git push
\end{minted}
and {\em Bob} get it without a glitch: 
\begin{minted}{console}
bob@laptop2$ git pull
\end{minted}

All repositories should be in the same state now:
\begin{minted}{bash}
gitk
\end{minted}

\begin{minted}{bash}
git pull # do nothing: already up-to-date
git push # do nothing: already up-to-date
\end{minted}

\subsection{Add new files}

{\em Alice} create a new file \texttt{toto.c} and do:
\begin{minted}{console}
alice@laptop1$ git status
\end{minted}
that should display:
\begin{verbatim}
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)

      toto.c
nothing added to commit but untracked files present (use "git add" to track)
\end{verbatim}

To put it in the next commit, {\em Alice} has to mark it for tracking
first with \texttt{git add toto.c}:

\begin{minted}{console}
alice@laptop1$ git add toto.c
alice@laptop1$ git status
\end{minted}
should display:
\begin{verbatim}
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

      new file:   toto.c

\end{verbatim}

{\em Alice} creates the commit in one line, providing directly the message:
\begin{minted}{bash}
alice@laptop1$ git commit -m "ajout de toto.c"
\end{minted}
should display:
\begin{verbatim}
[master b1d56e6] Ajout de toto.c
 1 files changed, 4 insertions(+), 0 deletions(-)
 create mode 100644 toto.c
\end{verbatim}

{\em Alice} publishes the commit:
\begin{minted}{console}
alice@laptop1$ git push
\end{minted}
{\em Bob} gets the file with the following command:
\begin{minted}{console}
bob@laptop2$ git pull
\end{minted}
that should display:
\begin{verbatim}
Fast forward
 toto.c |    4 ++++
 1 files changed, 4 insertions(+), 0 deletions(-)
 create mode 100644 toto.c
\end{verbatim}



\subsection{Ignoring some files in Git}

{\em Bob} creates a new file \texttt{temp-file.txt}.

The command:
\begin{minted}{console}
bob@laptop2$ git status
\end{minted}
should display:
\begin{verbatim}
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)

      temp-file.txt

nothing added to commit but untracked files present (use "git add" to track)
\end{verbatim}

If {\em Bob} doesn't want to track the file in the repository, he
should register in the repository that this file should be ignored.
He adds the file name into a file \texttt{.gitignore} in the same
directory.

{\em Bob} opens the file: 
\begin{minted}{console}
bob@laptop2$ emacs .gitignore
\end{minted}
adds the line:
\begin{verbatim}
temp-file.txt
\end{verbatim}
saves, and quits.



{\em Bob} does again the command:
\begin{minted}{bash}
bob@laptop2$ git status
\end{minted}
that should display:
\begin{verbatim}
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

      new file:   .gitignore

\end{verbatim}

The file \texttt{temp-file.txt} is now ignored. It is common to add all generated file in
the \texttt{.ignore} file like
\texttt{*.o *\textasciitilde\ *.bak}.

{\em Bob} adds and pushes the \texttt{.ignore} file.


\section{Conclusion}

Git has many more way to help you in your daily life of computer
scientist. Most are explained in \href{https//git-scm.com/book}{Pro
  Git book}.

\section*{Summary of the basic commands}
\begin{description}
\item[git commit] register, in a local commit, the state of file previously added with \texttt{git add}
\item[git commit -a] register, in a local commit, the state of all tracked files
 \item[git push] publishes the commits
 \item[git pull] get and merge published commits,
 \item[git add, git rm et git mv] mark, or unmark, file for tracking.
 \item[git status, git diff HEAD] display the current state of the local repository and tips
   for the next step
\end{description}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "francais"
%%% End:
