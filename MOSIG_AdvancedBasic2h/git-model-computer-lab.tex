\documentclass[a4paper,10pt]{scrartcl}

\usepackage[frenchb]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\DeclareUnicodeCharacter{00A0}{~}
\usepackage[left=2.5cm,right=2.5cm,top=2.5cm,bottom=2.5cm]{geometry}
\usepackage{microtype}
\usepackage{menukeys}


\usepackage{url}
\usepackage{hyperref}
\hypersetup{
  colorlinks,
  linkcolor={black},%{red!50!black},
  citecolor={blue!50!black},                        
  urlcolor={blue!80!black},
  linktoc=all
}
\usepackage{tikz}
\usetikzlibrary{shapes}
\usetikzlibrary{calc,fadings,shapes.arrows,shadows,backgrounds, positioning}
% il exite mainteannt gitdags pour générer du tikz !
% \usepackage{gitdags}

\usepackage{fancyheadings}
\usepackage{minted}
\usepackage{todonotes}

\newcommand{\annee}{2020-2021}

\date{\annee}
\author{Sylvain Bouveret, Grégory Mounié}
\title{Git\\Fundamentals of the data model}

\sloppy

\usemintedstyle{tango}
\setminted{breaklines}
\setminted{linenos=true}
\definecolor{verylightgray}{rgb}{0.95,0.95,0.95}
\setminted{bgcolor=verylightgray}

\begin{document}
\maketitle
\thispagestyle{fancy}
\fancyhead[HL]{\includegraphics[height=2cm]{logo_UGA_couleur_cmjn.jpg}}
\fancyhead[HC]{\includegraphics[height=2cm]{ensimag.jpg}}
\fancyhead[HR]{\includegraphics[width=4.5cm]{GINP_BlocLogo.png}}

This document is available here:\\
\begin{center}
  \url{http://systemes.pages.ensimag.fr/www-git/git-model-computer-lab.pdf}
\end{center}

\section{Introduction}

\subsection{Git: plumbing}

At first sight, Git is a complicated beast. It uses roughly 100
commands, some with tenths of options. But Git has a simple data
model. Indeed, it has a classical file hierarchy storing multiple
version of the content and the commands to manipulate these
files. Understanding the basics of the model greatly ease the
understanding of the majority of the commands and the efficient
management of the repositories.

The exercises will look at the commits, the branches, the tags and
most of the basic command effect.
  
\subsection{Team organization}

These exercises can be done alone. We advise to do it in team of
two. Each member of the team will do all the commands. Thus, each
member should explain to the other its understanding. Exchanging with
your teammate will force you to slow your pace and consider all the
details more carefully.

Let us look now at the content of the \texttt{.git} directory.

\section{First steps}

You will create first a small python program to uncompress ZLIB file,
then a minimalist repository.

\subsection{ZLIB cat in python}

The goal of the following program is to show you that Git use plain
Zlib compression format. Thus, the file contents are easily readable. Zlib
is part of the standard library of libpng, python or Java and a
ubiquitous compression format (cf. https://en.wikipedia.org/wiki/Zlib)

The code can be directly downloaded from
\url{http://recherche.noiraudes.net/resources/git/TP/zlibcat.py3})

\inputminted{python}{../FormaContinue_MeteoAvril2021/TPs/zlibcat.py3}

Do not forget to set its execution right after the edition.

\begin{minted}{console}
$ emacs zlibcat.py3 # or your favorite editor
$ chmod u+x zlibcat.py3
\end{minted}

\subsection{Minimalist repository}

Create a minimalist repository \mintinline{console}{MiniRepo} with a
single file and a first commit.

\begin{minted}{console}
$ mkdir MiniRepo
$ cd MiniRepo
$ git init .
$ emacs file.txt # add few lines with your editor
$ git add file.txt
$ git commit -m "message 1"
\end{minted}

\section{Exploring the .git directory}
\label{sec=gitrepo}

Check the files into the repository:
\begin{minted}{console}
$ ls -F .git
\end{minted}

You should find a file \texttt{HEAD}. Read its content.

The current state of the working directory (HEAD) is written in the
\texttt{HEAD} file. Following the value in the \texttt{refs/}
directory, find the file content with the SHA-1 variable of the head
and note its value.

First check that the value of the SHA-1 is the same as the commit. 
\begin{minted}{console}
$ git log
\end{minted}

\subsection{From SHA-1 to SHA-256}

Git is moving, very slowly and carefully, from SHA-1 hash to SHA-256
hash to evade from the vulnerabilities of SHA-1.

\section{Content-addressable storage: the \texttt{objects/} directory}

\subsection{First, by hand}

In the \texttt{objects/} directory, you should find a directory with a
name of two characters, the first two characters of the SHA-1.

In this directory, you should find a file, with a name composed of the
remaining character of the SHA-1.

Display the content of the file using \texttt{zlibcat.py3} with a
command similar to:
\begin{minted}{console}
$ ./zlibcat.py3 < .git/objects/2d/447e8255ace8f0d36527aa62ab7669f121f540
\end{minted}

Note the SHA-1 of the \texttt{tree}. Using the same procedure, display
the content of the \texttt{tree} with \texttt{zlibcat.py3}.

The tree format is slightly more complicated. Yet, only one unread
file remain in \texttt{objects/}. Check that the last file content is
the content of \texttt{file.txt}.

\subsection{Second, using Git}

Git provides directly a tool to read the content of file given the SHA-1 value.
\begin{minted}{console}
$ git cat-file -p THE_SHA-1_FILE_NAME_TO_READ
\end{minted}

Redo the full chain from \texttt{HEAD} to \texttt{file.txt} using
\texttt{cat-file}.

\subsection{Trees}

Write a subdirectory, new file in it, with the same name, but
different content, and register the file in a new commit.

\begin{minted}{console}
$ mkdir subDir
$ emacs subDir/file.txt # insert few lines
$ git add subDir/file.txt
$ git commit -m "message 2"
\end{minted}

Following again the path of \mintinline{sh}{git cat-file -p}, read the
\texttt{tree}. It contents now a reference to another tree (the
subdirectory). The subdirectory tree reference the new file. Check the
content of the new file.

\subsection{Duplicates}

Copy the first file and register the copy.
\begin{minted}{console}
$ cp file.txt file_copy.txt 
$ git add file_copy.txt
$ git commit -m "message 3"
\end{minted}

Following again the path of \mintinline{sh}{git cat-file -p}, read the
\texttt{tree}. Check that the blob are the same for both files (same
SHA-1, thus same file).

\section{Delta storage}

Modify slightly the content of \texttt{file.txt}. Commit the modification.

Check that a new blob appear. Both blob should have very similar data
in them.

Ask Git to optimize the storage:
\begin{minted}{console}
$ git gc
\end{minted}

A new \texttt{pack} file (and \texttt{.idx}) is now present in the
\texttt{objects/pack/} directory. This pack file includes the file
contents with delta. The following command allow to display the table
of content of the pack:
\begin{minted}{console}
$ git verify-pack -v .git/objects/pack/pack-0618531a948d3537443496da7765fb4d0b4fb74f
\end{minted}

The pack contains the last value of the file and the delta to come
back in the file history.

\section{The tags in the \texttt{refs/} directory}\label{sec:tags}

The tags are given to a particular commit in order to find them again
later. They are quite similar to branches but never move their head
after their creation.

Create a tag on the current commit:
\begin{minted}{console}
$ git tag v0.1
\end{minted}

A new file appears in the \texttt{.git/refs/tags}. Its content should
be the same as the current HEAD:
\begin{minted}{console}
$ cat .git/refs/tags/v0.1
\end{minted}

Now, move forward the \texttt{master} branch:

\begin{minted}{console}
$ emacs file2.txt # insert few lines
$ git add file2.txt
$ git commit -m "message 4"
\end{minted}

Check that the tag do not move.

\section{Branches}

Create a new branch \texttt{develop}, copy a file and commit.

\begin{minted}{console}
$ git checkout -b develop
$ cp file.txt file_copy2.txt 
$ git add file_copy2.txt
$ git commit -m "message dev1"
\end{minted}

Check that HEAD points on the right commit of the \texttt{develop} branch.

Then, come back on the \texttt{master} branch and do exactly the same
modification.

\begin{minted}{console}
$ git checkout master
$ cp file.txt file_copy2.txt
$ git add file_copy2.txt
$ git commit -m "message 5"
\end{minted}

Check that HEAD points on the right commit of the \texttt{master} branch.

And now merge the \texttt{develop} branch in master.
\begin{minted}{console}
$ git merge develop
\end{minted}

Check that the commit has two parents and the all the three trees
point to the same file.

\section{Headless Git}

This section discusses about ``detached head'': why do you pass in this
state, and what to do to avoid trouble.

\subsection{Get the work directory state of a previous commit}
\label{sec:headless}

Get the SHA-1 value of the last commit of \texttt{develop} using the
following command:
\begin{minted}{console}
$ git show develop
\end{minted}

Get the working directory related to this commit (\texttt{checkout}):
\begin{minted}{console}
$ git checkout <SHA-1 commit>
\end{minted}

Git should warn you about a ``detached head'' state.

Create a new file and register it with a new commit:
\begin{minted}{console}
$ emacs file3.txt # insert few lines
$ git add file3.txt
$ git commit -m "detached head message 1"
\end{minted}

Check that the commit is fully known by Git:
\begin{minted}{console}
$ git log --graph --oneline
\end{minted}

Note well this commit number and then move to the \texttt{master}
branch:
\begin{minted}{console}
$ git checkout master
$ git log --graph --oneline
\end{minted}

But now, where is the commit with \texttt{file3.txt}? No branch
references it. Unless you remember the SHA-1, you may have to dig into
the logs:
\begin{itemize}
\item look into HEAD history (\texttt{logs/refs/HEAD}) for the title
  of the commit
\item if you know some ``exclusive'' content of the commit, look into
  the whole set of file content by using the full list of revision and
  \mintinline{console}{git-grep}:
\begin{minted}{console}
$ git rev-list --all | xargs git grep <PATTERN>
\end{minted}
\item do a full search in the blob list, excluding, first, those
  already in some branches.
\end{itemize}

In a Git repository, the only easily visible files are in the ancestor
commits of an entry point:
\begin{itemize}
\item branches;
\item tags;
\item \texttt{HEAD}.
\end{itemize}

Hopefully, you remember fully the SHA-1 and thus it is much easier to
manage. Two non-exclusive example methods follows.

First, merge the commit in the \texttt{develop} branch:
\begin{minted}{console}
$ git checkout develop
$ git merge <commit SHA-1> # commit with file3.txt
\end{minted}

Second, create a new branch with the commit number.
\mintinline{console}{$ git branch <BRANCH NAME> <commit SHA-1>}.

\subsection{Working with another branch}

Come back to the \texttt{master}:
\begin{minted}{console}
$ git checkout master
\end{minted}

Redo the same manipulation as in section~\ref{sec:headless}, but
using the branch name \texttt{develop} instead of the commit number of
the branch.

\begin{minted}{console}
$ git checkout develop
$ emacs file3.txt # insert few lines
$ git add file3.txt
$ git commit -m "message dev2"
$ git checkout master
\end{minted}

Come back to \texttt{develop} and check that the commit is there:
\begin{minted}{console}
git checkout develop
git log --graph --oneline
\end{minted}

The pointer of the branch follows HEAD and advances with it. It is
much easier to change HEAD for another branch without loosing any
commit.

\subsection{And with the tags?}

If you come back to the tag \texttt{v0.1} from section~\ref{sec:tags},
are you in a detached head state, or not ? Why ?

Try to come back to the tag, and check !

\section{Conclusion\dots{}}

Git data storage is addressable by content. The link between files compose
a direct acyclic graph, with several sources (entry points). Most Git
commands create new nodes, new sources or navigate in the
graph. Understanding this graph management is the main challenge of
Git, and almost the only challenge.

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "francais"
%%% End:
