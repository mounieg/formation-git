%https://tex.stackexchange.com/questions/70320/workflow-diagram

\begin{tikzpicture}[head/.style={line width=2pt,draw=gray,rounded corners=8pt,text 
width=2cm,align=center}]
	
\node[head,fill=green!60!black!50, minimum height = 1.5cm] (wd) {working \\ directory};
\node[head,fill=yellow!40,right=of wd, minimum height = 1.5cm] (sa) {staging \\ area};
\node[head,fill=blue!40,right=of sa, minimum height = 1.5cm] (lr) {local \\ repo};
\node[head,fill=red!40,right=2cm of lr, minimum height = 1.5cm] (rr) {remote \\ repo};

\node[above= 1 cm of rr,font={\bfseries}]{Remote repository};
\node[above= 0.5 cm of rr]{(gitlab.ensimag.fr)};

\node[above= 1 cm of sa,font={\bfseries}]{Local repository};
\node[above= 0.5 cm of sa]{(laptop1 and laptop2)};

\begin{scope}[line width=2pt,gray]
\foreach \x in {wd,sa,lr,rr}
  \draw ([yshift=-5pt]\x.south) -- +(0,-7.3cm);
\end{scope}
\node[draw, fill=white] at ([yshift=-1cm]rr.south) {git init};
\draw[<-, very thick] ([yshift=-1.5cm]wd.south) -- ([yshift=-1.5cm]rr.south) node[midway,above] {git clone};
\draw[->, very thick] ([yshift=-2.3cm]wd.south) -- ([yshift=-2.3cm]sa.south) node[midway,above] {git add};
\draw[->, very thick] ([yshift=-2.3cm]sa.south) -- ([yshift=-2.3cm]lr.south) node[midway,above] {git commit};
\draw[->, very thick] ([yshift=-3.1cm]wd.south) -- ([yshift=-3.1cm]lr.south) node[midway,above] {git commit -a};
\draw[->, very thick] ([yshift=-3.9cm]lr.south) -- ([yshift=-3.9cm]rr.south) node[midway,above] {git push};
\draw[<->, very thick] ([yshift=-4.7cm]wd.south) -- ([yshift=-4.7cm]sa.south) node[midway,above] {git status};
\draw[<-, very thick] ([yshift=-5.5cm]wd.south) -- ([yshift=-5.5cm]rr.south) node[midway,above] {git pull};
\draw[<->, very thick] ([yshift=-6.3cm]wd.south) -- ([yshift=-6.3cm]sa.south) node[midway,above] {git diff};
\draw[<->, very thick] ([yshift=-7.1cm]wd.south) -- ([yshift=-7.1cm]lr.south) node[midway,above] {git diff HEAD};

% Background
\begin{pgfonlayer}{background}
\fill[gray!10]($(lr.north)!0.5!(rr.north)+(0,0.5)$)rectangle($(lr.south)!0.5!(rr.south)+(4,-7.3)$) ;
\draw[dashed, shorten <=-1.5cm] ($(lr.south)!0.5!(rr.south)$)--($(lr.south)!0.5!(rr.south)-(0,7.3)$);
\end{pgfonlayer}

\node[draw, fill=white, font=\scriptsize] at ([yshift=-9cm]lr.west) {inspiration : http://ndpsoftware.com/git-cheatsheet.html};


\end{tikzpicture}