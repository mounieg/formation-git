%\documentclass{beamer}

\usepackage{moy-slides}
\usepackage{comment}
%\excludecomment{detailed}
\newenvironment{detailed}{}{}

\title[Git]{Using Git}

%\subtitle{...}

\author{Matthieu Moy}

\institute[Matthieu.Moy@imag.fr]{Matthieu.Moy@imag.fr\\
\url{http://www-verimag.imag.fr/~moy/cours/formation-git/git-slides.pdf}}

\date{2015}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[width=\textwidth]{fig/in-case-of-fire}
  \end{center}
\end{frame}

\section[Intro]{Revision Control System}

\begin{frame}[fragile] \frametitle{Backups: The Old Good Time}
  \begin{itemize}[<.->]
  \item<+-> \alert<.>{Basic problems}:
    \begin{itemize}
    \item ``Oh, my disk crashed.'' / ``Someone has stolen my laptop!''
    \item ``@\#\%!!, I've just deleted this important file!''
    \item ``Oops, I introduced a bug a long time ago in my code, how
      can I see how it was before?''
    \end{itemize}
  \item<+-> \alert<.>{Historical solutions}:
    \begin{itemize}
    \item \alert<.>{Replicate}:\\
      \verb|$ cp -r ~/project/ ~/backup/|\\
      (or better, copy to a remote machine like telesun)
    \item \alert<.>{Keep history}:\\
      \verb|$ cp -r ~/project/ ~/backup/project-2015-02-02|
    \item \dots
    % \item \alert<.>{Keep a description of history}:\\
    %   \verb|$ echo "Description of current state" > \|
    %   \verb|    ~/backup/project-2012-02-02/README.txt|
%$
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame} \frametitle{Collaborative Development: The Old Good
    Time}
  \begin{itemize}[<.->]
  \item<+-> \alert<.>{Basic problems}: Several persons working on the
    same set of files
    \begin{enumerate}
    \item ``Hey, you've modified the same file as me, how do we
      merge?'',
    \item ``Your modifications are broken, your code doesn't even
      compile. Fix your changes before sending it to me!'',
    \item ``Your bug fix here seems interesting, but I don't want your
      other changes''.
    \end{enumerate}
  \item<+-> \alert<.>{Historical solutions}:
    \begin{itemize}
    \item Never two person work at the same time.
      $\Rightarrow$ Doesn't scale up! Unsafe.
    \item People work on the same directory (same machine, NFS, ACLs
      \dots)\\
      $\Rightarrow$ Painful because of (2) above.
    % \item People lock the file when working on it.\\
    %   $\Rightarrow$ Hardly scales up!
    \item People work trying to avoid conflicts, and \alert<.>{merge}
      later.
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}[fragile] \frametitle{Merging: Problem and Solution}
  \footnotesize
  \begin{columns}[t]
    \begin{column}{.3\hsize}
      \begin{itemize}
      \item My version
{\scriptsize
\begin{semiverbatim}
#include <stdio.h>

int main () \{
  printf("Hello");

  return \alert{EXIT_SUCCESS};
\}
\end{semiverbatim}
}
    \end{itemize}
    \end{column}
    \begin{column}{.3\hsize}
      \begin{itemize}
      \item Your version
{\scriptsize
\begin{semiverbatim}
#include <stdio.h>

int main () \{
  printf("Hello\alert{!\\n}");

  return 0;
\}
\end{semiverbatim}
}
    \end{itemize}
    \end{column}
    \pause
    \begin{column}{.3\hsize}
      \begin{itemize}
      \item Common ancestor
{\scriptsize
\begin{semiverbatim}
#include <stdio.h>

int main () \{
  printf("Hello");

  return 0;
\}
\end{semiverbatim}
}
      \end{itemize}
    \end{column}
  \end{columns}
  \pause
  \normalsize
  \begin{center}
    This merge can be done for you by an automatic tool
  \end{center}
  \begin{center}
    \alert{Merging relies on history!}
  \end{center}
  \pause
  \begin{center}
    \alert{Collaborative development linked to backups}
  \end{center}
\end{frame}

\begin{frame} \frametitle{Merging}
  \begin{center}
    \scalebox{.8}{
      \input fig/merge.tex
    }
  \end{center}
\end{frame}

\begin{frame} \frametitle{Revision Control System: Basic Idea}
  \begin{itemize}[<.->]
  \item<+-> Keep track of \alert<.>{history}:
    \begin{itemize}
    \item {\tt commit} = snapshot of the current state,
    \item Meta-data (user's name, date, descriptive message,\dots{})
      recorded in commit.
    \end{itemize}
  \item Use it for \alert<.>{merging}/collaborative development.
    \begin{itemize}
    \item Each user works on its own copy,
    \item User explicitly ``takes'' modifications from others when
      (s)he wants.
    \end{itemize}
  \item<+-> Efficient storage/compression (``delta-compression
    $\approx$ incremental backup'')
  \end{itemize}
\end{frame}

\section[Git]{Git: Basic Principles}

\begin{frame}[label=basic-concepts] \frametitle{Git: Basic concepts}
  \begin{itemize}
  \item Each working directory contains:
    \begin{itemize}
    \item The files you work on (as usual)
    \item The history, or ``repository'' (in the directory {\tt .git/})
    \end{itemize}
  \item Basic operations:
    \begin{itemize}
    \item \alert{git clone}: get a copy of an existing repository
      (files + history)
    \item \alert{git commit}: create a new revision in a repository
    \item \alert{git pull}: get revisions from a repository
    \item \alert{git push}: send revisions to a repository
    \item \alert{git add}, \alert{git rm} and \alert{git mv}: tell Git
      which files should be tracked
    \item \alert{git status}: know what's going on
    \end{itemize}
  \item For us:
    \begin{itemize}
    \item Each team creates a shared repository, in addition to work trees
    \end{itemize}
  \end{itemize}
\end{frame}

\section[Others]{Git Vs Others}

\subsection{History}

\begin{frame} \frametitle{A bit of history}
  \begin{description}
  \item[1986:] Birth of CVS, centralized version system
  \item[2000:] Birth of Subversion (SVN), a replacement for CVS with
    the same concepts
  \item[2005:] First version of Git
  \end{description}
\end{frame}

\begin{frame} \frametitle{Git and the Linux Kernel}
  \begin{description}
  \item[1991:] Linus Torvalds starts writing Linux, using mostly tar+patch,
  \item[2002:] Linux adopts BitKeeper, a proprietary decentralized
    version control system (available free of cost for Linux),
  \item[2002-2005:] Flamewars against BitKeeper, some Free Software
    alternatives appear (GNU Arch, Darcs, Monotone). None are good
    enough technically.
    \pause
  \item[2005:] BitKeeper's free of cost license revoked. Linux has to
    migrate.
  \item[2005:] Unsatisfied with the alternatives, Linus decides to start
    his own project, \alert{Git}.
    \pause
  \item[2007:] Many young, but good projects for decentralized
    revision control: Git, Mercurial, Bazaar, Monotone, Darcs, \dots{}
  \item[2014:] Git is the most widely used according to Eclipse user's
    survey.
  \end{description}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Who Makes Git?}
  \def\employer#1{\(\leftarrow \mbox{\textsf{#1}}\)}
  \begin{semiverbatim}
\$ git shortlog -s --no-merges | sort -nr | head -30
  6136  Junio C Hamano \employer{Google (full-time on Git)}
  1680  Jeff King \employer{GitHub (\(\approx\) full-time on Git)}
  1289  Shawn O. Pearce \employer{Google}
  1096  Linus Torvalds \textsf{(No longer contributor)}
   751  Nguyen Tha Ngoc Duy 
   748  Johannes Schindelin \employer{Microsoft (full-time on Git)}
   720  Jonathan Nieder \employer{Google}
   520  Michael Haggerty \employer{GitHub (recent)}
   514  René Scharfe
   511  Jakub Narebski
   487  Eric Wong
   414  Felipe Contreras
   401  Johannes Sixt
   348  Christian Couder \employer{Booking.com (50\% on Git)}
   345  Nicolas Pitre
  \end{semiverbatim}
\end{frame}

\subsection{Popularity}

\begin{frame} \frametitle{Git Adoption (Debian popularity contest)}
  \begin{center}
    \includegraphics[width=.8\textwidth]{fig/popcon-png}
  \end{center}
\end{frame}

\begin{frame}\frametitle{Git Adoption (Job offers)}
  \begin{center}
    \only<1>{\includegraphics[width=.8\textwidth]{fig/jobgraph}}
    \only<2>{\includegraphics[width=.8\textwidth]{fig/jobgraph-proprietary}}
  \end{center}  
\end{frame}

\begin{frame}
  \frametitle{Git Adoption (Hosting)}

  \begin{itemize}
  \item ``There are 11.6M people collaborating right now across 29.1M
    repositories on
    GitHub''\footnote{\url{https://github.com/about/press}}
  \item How about Mercurial?
    \includegraphics[width=.8\textwidth]{fig/mercurial-hosting}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Summary of Available Options}

  \begin{itemize}
  \item Centralized
    \begin{description}
    \item[RCS, CVS] Outdated
    \item[SVN] Does the job
    \end{description}
  \item Decentralized
    \begin{description}
    \item[Git] Fast, powerful, popular
    \item[Mercurial (hg)] Very similar to Git but designed to be
      simpler. Less popular but very active too.
    \item[Bazaar (bzr)] Development stopped in 2013
    \item[Monotone (mtn)] Invented the core concepts behind Git, slow,
      never took up
    \item[Darcs] Novel design, slow (exponential worst-case), never
      took up
    \end{description}
  \end{itemize}
\end{frame}

\subsection{Centralized Vs Decentralized}

\begin{detailed}
  \begin{frame}
 \frametitle{CVS and SVN: Commit/Update Approach}
  \begin{center}
    \scalebox{.8}{
\mode<beamer>{%
\only<1>{\input fig/commit-update-1.pdf_t}%
\only<2>{\input fig/commit-update-2.pdf_t}%
\only<3>{\input fig/commit-update-3.pdf_t}%
\only<4>{\input fig/commit-update-4.pdf_t}%
\only<5>{\input fig/commit-update-5.pdf_t}%
\only<6>{\input fig/commit-update-6.pdf_t}%
\only<7>{\input fig/commit-update-7.pdf_t}%
\only<8>{\input fig/commit-update-8.pdf_t}%
\only<9>{\input fig/commit-update-9.pdf_t}%
\only<10>{\input fig/commit-update-10.pdf_t}%
\only<11->{\input fig/commit-update-11.pdf_t}%
}\mode<handout>{%
\only<all:1>{\input fig/commit-update-7.pdf_t}%
\only<all:2>{\input fig/commit-update-8.pdf_t}%
\only<all:3>{\input fig/commit-update-11.pdf_t}%
}
    }
  \end{center}
\end{frame}

\begin{frame} \frametitle{Commit/Update Approach : limitations}
  \begin{itemize}
  \item A change is either ``uncommited'' or ``cast in stone''
  \item Update before commit: what if the merge fails?
  \item No easy way to contribute to a repo without write permission
  \end{itemize}
\end{frame}
\end{detailed}

\begin{frame} \frametitle{Decentralized Version Control}
  \begin{center}
    \huge Each developer has its own repository
  \end{center}
  \begin{itemize}
  \item Works offline, fast (I use \texttt{git} more than \texttt{ls}
    and \texttt{cd} !)
  \item Replicate data ($\Rightarrow$ safer)
  \item No need for a server, creating a repo is cheap (I have 200
    repos on my account)
  \item Private space (draft, not cast in stone)
  \item Different workflows
  \end{itemize}
\end{frame}

\begin{detailed}
  \input{example.tex}
\end{detailed}

\section[Advices]{Advices Using Git}

\begin{frame} \frametitle{Advices Using Git (\alert{for beginners})}
  \begin{itemize}
  \item \alert{Never} exchange files outside Git's control (email,
    scp, usb key), except if you \emph{really} know what you're
    doing;
    \pause
  \item Always use {\tt git commit} with \alert{\tt -a};
  \item Make a  \alert{\tt git push} after each {\tt git commit -a}
    (use {\tt git pull} if needed);
  \item Do \alert{\tt git pull} regularly, to remain synchronized with
    your teammates. You need to make a {\tt git commit -a} before you
    can make a {\tt git pull} (this is to avoid mixing manual changes
    with merges).
  \item Do not make useless changes to your code. Do not let your
    editor/IDE reformat code that is not yours.
  \end{itemize}
\end{frame}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "american"
%%% End:
