\usepackage{moy-slides}
\usepackage{tikz}
\usetikzlibrary{shapes}
\usepackage{moy-tikz}
\usepackage{comment}

\title[Configuring Git]{Configuring Git}

%\subtitle{...}

\author{Matthieu Moy}

\institute[Matthieu.Moy@imag.fr]{Matthieu.Moy@imag.fr\\
\url{http://www-verimag.imag.fr/~moy/cours/formation-git/configuring-git-slides.pdf}}

\date{2015}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Configuration of Git}

  \begin{itemize}
  \item What you already did:
    \begin{itemize}
    \item Introduce yourself (\texttt{user.name} = ... ,
      \texttt{user.email} = ...)
    \item Tell Git about your favorite editor (\texttt{core.editor})
    \item Tell Git to ignore some files (\texttt{.gitignore})
    \end{itemize}
  \item What we're about to do:
    \begin{itemize}
    \item Learn where the config files are
    \item Learn how to read the docs
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Configuration files}

\begin{frame}[fragile]
  \frametitle{Git Configuration: Which Files}
  
  \begin{itemize}
  \item 3 places
    \begin{itemize}
    \item System-wide: \verb|/etc/gitconfig|
    \item User-wide (``global''): \verb|~/.gitconfig| or
      \verb|~/.config/git/config|
    \item Per-repository: \verb|$project/.git/config|
    \end{itemize}
  \item Precedence: per-repo overrides user-wide overrides
    system-wide.
  \item Not versionned by default, not propagated by \texttt{git
      clone}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Git Configuration: Syntax}

  \begin{itemize}
  \item Simple syntax, key/value:
\begin{verbatim}
[section1]
	# This is a comment
        key1 = value1 # comment as well
        key2 = value2
[section2 "subsection"]
        key3 = value3
\end{verbatim}
  \item Semantics:
    \begin{itemize}
    \item ``\texttt{section1.key1} takes value \texttt{value1}''
    \item ``\texttt{section1.key2} takes value \texttt{value2}''
    \item ``\texttt{section2.subsection.key3} takes value \texttt{value3}''
    \end{itemize}
  \item ``section'' and ``key'' are case-insensitive.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Querying/Modifying Config Files}

\begin{lstlisting}[language=sh]
# Modify per-repo .git/config:
$ git config user.name 'Matthieu Moy'
$ cat .git/config
...
[user]
        name = Matthieu Moy
# Modify user-wide ~/.gitconfig:
$ git config --global user.name 'Matthieu Moy'
# Get the value of a variable:
$ git config user.name
Matthieu Moy
\end{lstlisting}
\end{frame}

\begin{frame}
  \frametitle{Some Useful Config Variables}
  \begin{itemize}
  \item User-wide:
    \begin{description}
    \item[user.name, user.email] Who you are (used in \texttt{git
        commit})
    \item[core.editor] Text editor to use for \texttt{commit},
      \texttt{rebase -i}, ...
    \end{description}
  \item Per-repo:
    \begin{description}
    \item[remote.origin.url] Where to fetch/push
    \end{description}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Aliases}
\begin{lstlisting}[language=sh]
# Definition
$ cat .git/config
...
[alias]
        lg = log --graph --oneline

# Use
$ git lg
*   a5da80c Merge branch 'master' into HEAD
|\  
| * 048e8c1 bar
* | 5034527 boz
|/  
* 1e0e4a5 foo
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Documentation about Configuration}
  \begin{itemize}
  \item \texttt{man git-config} : documents all configuration
    variables (> 350)
  \item Example:
\begin{verbatim}
user.email
    Your email address to be recorded in any
    newly created commits. Can be overridden
    by the GIT_AUTHOR_EMAIL, GIT_COMMITTER_EMAIL,
    and EMAIL environment variables.
    See git-commit-tree(1).
\end{verbatim}
  \end{itemize}
\end{frame}

\section{(Git)Ignore files}

\begin{frame}
  \frametitle{Ignore Files: Why?}
  \begin{itemize}
  \item Git needs to know which files to track (\texttt{git add},
    \texttt{git rm})
  \item You don't want to forget a \texttt{git add}
  \item $\Rightarrow$ \texttt{git status} shows \texttt{Untracked
      files} as a reminder. Two options:
    \begin{itemize}
    \item \texttt{git add} them
    \item ask Git to ignore: add a rule to \texttt{.gitignore}
    \end{itemize}
  \item Only impacts \texttt{git status} and \texttt{git add}.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Ignore Files: How?}
  
  \begin{itemize}
  \item \texttt{.gitignore} file contain one rule per line:
\begin{verbatim}
# This is a comment

# Ignore all files ending with ~:
*~
# Ignore all files named 'core':
core
# Ignore file named foo.pdf in this directory:
/foo.pdf
\end{verbatim}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Ignore Files: Where?}
  \begin{itemize}
  \item User-wide: \verb|~/.config/git/ignore|:
    \begin{itemize}
    \item Example: your editor's file like \verb|*~| or \verb|.*.swp|
    \item Don't disturb your co-workers with your personal preferences
    \item Set once and for all
    \end{itemize}
  \item Per-repo, not versionned: \verb|.git/info/exclude|
    \begin{itemize}
    \item Not very useful ;-)
    \end{itemize}
  \item Tracked within the project (\texttt{git add} it):
    \verb|.gitignore| in any directory, applies to this directory and
    subdirectories.
    \begin{itemize}
    \item Generated files (especially binary)
    \item Example: \verb|*.o| and \verb|*.so| for a C project
    \item Share with people working on the same project
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{About Generated Files}
  \begin{itemize}
  \item Versionning (\texttt{git add}-ing) generated files is bad
    \pause
  \item Versionning generated \alert{binary} files is \alert{very} bad
    \pause
  \item Why?
    \begin{itemize}
    \item breaks \texttt{make} (timestamp = \texttt{git checkout}
      time)
    \item breaks merge
    \item eats disk space (inefficient delta-compression)
    \end{itemize}
  \end{itemize}
\end{frame}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "american"
%%% End:
